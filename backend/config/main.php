<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'language' => 'ru-RU',
    'bootstrap' => [
        'log',
        'languagepicker',
    ],
    'controllerNamespace' => 'backend\controllers',
    'modules' => [
        'admin' => [
            'class' => 'backend\modules\admin\Module',
        ],
        'translatemanager' => [
            'class' => 'backend\modules\translatemanager\Module',
            'allowedIPs' => ['*',],
            'ignoredCategories' => [
                'yii',
                'language',
                'rbac-admin',
                'kvbase',
                'kvselect',
            ],
//            'tables' => [
//                [
//                    'connection' => 'db',
//                    'table' => '{{%menus}}',
//                    'columns' => ['name', 'description'],
//                    'category' => 'database-table-name',
//                ],
//                [
//                    'connection' => 'db',
//                    'table' => '{{%menu_items}}',
//                    'columns' => ['label', 'description'],
//                    'category' => 'database-table-name',
//                ],
//            ],
        ],
    ],
    'homeUrl' => '/apanel',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
            'baseUrl' => '/apanel',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => '_session-backend',
            'cookieParams' => [
                'lifetime' => 30 * 86400
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        'assetManager' => [
            'bundles' => [
                'backend\extensions\adminlte\assets\AdminLteAsset' => [
                    'skin' => 'skin-purple',
                ],
            ],
        ],
        'languagepicker' => [
            'class' => 'common\components\languagepicker\Component',
            'languages' => function () {
                return \backend\modules\translatemanager\models\Language::getLanguageNames(true);
            }
        ],
    ],
    'as access' => [
        'class' => 'backend\modules\admin\components\AccessControl',
        'allowActions' => [
//            '*',
            'site/logout',
        ]
    ],
    'params' => $params,
    'controllerMap' => [
        'elfinder' => [
            'class' => 'mihaildev\elfinder\PathController',
            'access' => ['@'],
            'root' => [
                'baseUrl' => '@uploadsUrl',
                'basePath' => '@uploadsPath',
                'path' => '/',
                'name' => 'Files'
            ],
            'watermark' => [
                'source' => __DIR__ . '/logo.png', // Path to Water mark image
                'marginRight' => 5,          // Margin right pixel
                'marginBottom' => 5,          // Margin bottom pixel
                'quality' => 95,         // JPEG image save quality
                'transparency' => 70,         // Water mark image transparency ( other than PNG )
                'targetType' => IMG_GIF | IMG_JPG | IMG_PNG | IMG_WBMP, // Target image formats ( bit-field )
                'targetMinPixel' => 200         // Target image minimum pixel size
            ]
        ]
    ],
];

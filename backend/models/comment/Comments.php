<?php

namespace backend\models\comment;

use Yii;
use common\models\User;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "comments".
 *
 * @property integer $id
 * @property string $model
 * @property integer $model_id
 * @property integer $author_id
 * @property integer $parent_id
 * @property double $rating
 * @property string $text
 * @property integer $likes
 * @property integer $unlikes
 * @property string $sessions
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property User $author
 */
class Comments extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comments';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['model', 'model_id', 'author_id', 'text'], 'required'],
            [['model_id', 'author_id', 'parent_id', 'likes', 'unlikes', 'status', 'created_at', 'updated_at'], 'integer'],
            [['rating'], 'number'],
            [['text', 'sessions'], 'string'],
            [['model'], 'string', 'max' => 255],
            [['status'], 'default', 'value' => self::STATUS_NEW],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'model' => Yii::t('model', 'Model'),
            'model_id' => Yii::t('model', 'Model ID'),
            'author_id' => Yii::t('model', 'Author'),
            'parent_id' => Yii::t('model', 'Parent'),
            'rating' => Yii::t('model', 'Rating'),
            'text' => Yii::t('model', 'Text'),
            'likes' => Yii::t('model', 'Likes'),
            'unlikes' => Yii::t('model', 'Unlikes'),
            'sessions' => Yii::t('model', 'Sessions'),
            'status' => Yii::t('model', 'Status'),
            'created_at' => Yii::t('model', 'Created At'),
            'updated_at' => Yii::t('model', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @return bool
     */
    public function getBtnDisabled()
    {
        if (empty($this->sessions))
            return false;

        $session = Yii::$app->session;
        if (!$session->isActive) $session->open();
        $sessionId = Yii::$app->user->isGuest ? $session->id : Yii::$app->user->id;
        return in_array($sessionId, explode(' ', $this->sessions));
    }

    /**
     * Specialists to array
     * @return array
     */
    public static function getAuthorsList()
    {
        return ArrayHelper::map(User::find()->where(['!=', 'status', User::STATUS_DELETED])->asArray()->all(), 'id', 'email');
    }

    /**
     * Status
     */
    const STATUS_NEW = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2;

    /**
     * Status Array
     * @param integer|null $status
     * @return array|string
     */
    public static function getStatusArray($status = null)
    {
        $array = [
            self::STATUS_NEW => Yii::t('model', 'New'),
            self::STATUS_ACTIVE => Yii::t('model', 'Active'),
            self::STATUS_INACTIVE => Yii::t('model', 'Inactive'),
        ];

        return $status === null ? $array : $array[$status];
    }

    /**
     * Status Name
     * @return string
     */
    public function getStatusClass()
    {
        $array = [
            self::STATUS_NEW => 'new',
            self::STATUS_ACTIVE => 'active',
            self::STATUS_INACTIVE => 'inactive',
        ];

        return isset($array[$this->status]) ? $array[$this->status] : '';
    }
}

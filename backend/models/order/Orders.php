<?php

namespace backend\models\order;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use yii\behaviors\TimestampBehavior;
use common\models\User;
use backend\models\location\Locations;

/**
 * This is the model class for table "orders".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $session_id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $phone
 * @property integer $location_id
 * @property string $address
 * @property string $comment
 * @property integer $payment
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property OrderItems[] $orderItems
 * @property User $customer
 * @property Locations $location
 */
class Orders extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orders';
    }

    public $sendEmail;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'location_id', 'payment', 'status', 'created_at', 'updated_at', 'sendEmail'], 'integer'],
            [['session_id', 'first_name', 'last_name', 'email', 'phone', 'location_id', 'address', 'payment'], 'required'],
            [['comment'], 'string'],
            [['session_id', 'first_name', 'last_name', 'email', 'phone', 'address'], 'string', 'max' => 255],
            [['status'], 'default', 'value' => self::STATUS_NEW],
            ['status', 'in', 'range' => [self::STATUS_DELETED, self::STATUS_NEW, self::STATUS_PENDING, self::STATUS_DELIVERY, self::STATUS_COMPLETED, self::STATUS_CANCELED, self::STATUS_FALSE]],
            ['payment', 'in', 'range' => [self::PAYMENT_CASH, self::PAYMENT_TERMINAL, self::PAYMENT_COMBINED]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'user_id' => Yii::t('model', 'Клиент'),
            'session_id' => Yii::t('model', 'Session ID'),
            'first_name' => Yii::t('model', 'Имя'),
            'last_name' => Yii::t('model', 'Фамилия'),
            'email' => Yii::t('model', 'Эл. адрес'),
            'phone' => Yii::t('model', 'Телефон'),
            'location_id' => Yii::t('model', 'Район'),
            'address' => Yii::t('model', 'Адрес'),
            'comment' => Yii::t('model', 'Комментарий'),
            'payment' => Yii::t('model', 'Оплата'),
            'status' => Yii::t('model', 'Статус'),
            'created_at' => Yii::t('model', 'Дата создания'),
            'updated_at' => Yii::t('model', 'Обновленная дата'),
            'total_price' => Yii::t('model', 'Итоговая цена'),
            'total_quantity' => Yii::t('model', 'Количество товаров'),
            'sendEmail' => Yii::t('model', 'Send Email'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderItems()
    {
        return $this->hasMany(OrderItems::className(), ['order_id' => 'id']);
    }

    /**
     * @return ActiveDataProvider
     */
    public function getOrderItemsDataProvider()
    {
        return new ActiveDataProvider([
            'query' => OrderItems::find()->where([
                'order_id' => $this->id
            ]),
            'sort' => false
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(Locations::className(), ['id' => 'location_id']);
    }

    /**
     * @return array
     */
    public static function getLocationsList()
    {
        return Locations::getLocationsList();
    }

    /**
     * @return array
     */
    public static function getUsersList()
    {
        return ArrayHelper::merge([0 => 'Гость'],
            ArrayHelper::map(User::find()->where(['!=', 'status', User::STATUS_DELETED])->asArray()->all(), 'id', 'username'));
    }

    /**
     * Set Status Deleted
     * @return mixed
     */
    public function setStatusDeleted()
    {
        $this->status = self::STATUS_DELETED;
        return $this->save();
    }

    /**
     * Status
     */
    const STATUS_DELETED = -1;
    const STATUS_NEW = 0;
    const STATUS_PENDING = 1;
    const STATUS_DELIVERY = 2;
    const STATUS_COMPLETED = 3;
    const STATUS_CANCELED = 4;
    const STATUS_FALSE = 5;

    /**
     * Status Array
     * @param integer|null $status
     * @return array|string
     */
    public static function getStatusArray($status = null)
    {
        $array = [
            self::STATUS_NEW => Yii::t('model', 'На рассмотрении'),
            self::STATUS_PENDING => Yii::t('model', 'Подтверждён'),
            self::STATUS_DELIVERY => Yii::t('model', 'Доставка'),
            self::STATUS_COMPLETED => Yii::t('model', 'Завершен'),
            self::STATUS_CANCELED => Yii::t('model', 'Отменён'),
            self::STATUS_FALSE => Yii::t('model', 'Ложный'),
        ];

        return $status === null ? $array : $array[$status];
    }

    /**
     * Status Name
     * @return string
     */
    public function getStatusName()
    {
        $array = [
            self::STATUS_NEW => '<span class="text-bold text-red">' . self::getStatusArray(self::STATUS_NEW) . '</span>',
            self::STATUS_PENDING => '<span class="text-bold text-yellow">' . self::getStatusArray(self::STATUS_PENDING) . '</span>',
            self::STATUS_DELIVERY => '<span class="text-bold text-aqua">' . self::getStatusArray(self::STATUS_DELIVERY) . '</span>',
            self::STATUS_COMPLETED => '<span class="text-bold text-green">' . self::getStatusArray(self::STATUS_COMPLETED) . '</span>',
            self::STATUS_CANCELED => '<span class="text-bold text-light-blue">' . self::getStatusArray(self::STATUS_CANCELED) . '</span>',
            self::STATUS_FALSE => '<span class="text-bold text-muted">' . self::getStatusArray(self::STATUS_FALSE) . '</span>',
        ];

        return isset($array[$this->status]) ? $array[$this->status] : '';
    }

    /**
     * Payment
     */
    const PAYMENT_CASH = 0;
    const PAYMENT_TERMINAL = 1;
    const PAYMENT_COMBINED = 2;

    /**
     * Payment List
     * @param integer|null $payment
     * @return array
     */
    public static function getPaymentArray($payment = null)
    {
        $array = [
            self::PAYMENT_CASH => 'Наличные',
            self::PAYMENT_TERMINAL => 'Терминал',
            self::PAYMENT_COMBINED => 'Комбинированный',
        ];

        return $payment === null ? $array : $array[$payment];
    }

    /**
     * Payment Name
     * @return string
     */
    public function getPaymentName()
    {
        $array = [
            self::PAYMENT_CASH => '<span class="text-bold text-blue">' . self::getPaymentArray(self::PAYMENT_CASH) . '</span>',
            self::PAYMENT_TERMINAL => '<span class="text-bold text-blue">' . self::getPaymentArray(self::PAYMENT_TERMINAL) . '</span>',
            self::PAYMENT_COMBINED => '<span class="text-bold text-blue">' . self::getPaymentArray(self::PAYMENT_COMBINED) . '</span>',
        ];

        return isset($array[$this->payment]) ? $array[$this->payment] : '';
    }
}

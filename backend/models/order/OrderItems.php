<?php

namespace backend\models\order;

use Yii;
use yii\db\ActiveRecord;
use backend\models\product\Products;

/**
 * This is the model class for table "order_items".
 *
 * @property integer $order_id
 * @property integer $product_id
 * @property integer $quantity
 * @property integer $price
 * @property integer $total_price
 *
 * @property Orders $order
 * @property Products $product
 */
class OrderItems extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_items';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'product_id', 'quantity', 'price'], 'required'],
            [['order_id', 'product_id', 'quantity', 'price', 'total_price'], 'integer'],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Orders::className(), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'order_id' => Yii::t('model', 'Заказ'),
            'product_id' => Yii::t('model', 'Товар'),
            'quantity' => Yii::t('model', 'Количество'),
            'price' => Yii::t('model', 'Цена'),
            'total_price' => Yii::t('model', 'Итоговая цена'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Orders::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_id']);
    }
}

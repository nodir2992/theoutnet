<?php

namespace backend\models\location;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "locations".
 *
 * @property integer $id
 * @property integer $location_id
 * @property string $type
 * @property string $name
 * @property integer $weight
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Locations $location
 * @property Locations[] $locations
 * @property Districts[] $districts
 */
class Locations extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'locations';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['location_id', 'weight', 'status', 'created_at', 'updated_at'], 'integer'],
            [['type'], 'string', 'max' => 16],
            [['name'], 'string', 'max' => 255],
            [['weight'], 'default', 'value' => 0],
            [['status'], 'default', 'value' => self::STATUS_INACTIVE],
            [['location_id'], 'exist', 'skipOnError' => true, 'targetClass' => self::className(), 'targetAttribute' => ['location_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'location_id' => Yii::t('model', 'Location ID'),
            'type' => Yii::t('model', 'Type'),
            'name' => Yii::t('model', 'Name'),
            'weight' => Yii::t('model', 'Weight'),
            'status' => Yii::t('model', 'Status'),
            'created_at' => Yii::t('model', 'Created At'),
            'updated_at' => Yii::t('model', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(self::className(), ['id' => 'location_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocations()
    {
        return $this->hasMany(self::className(), ['location_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistricts()
    {
        return $this->hasMany(Districts::className(), ['location_id' => 'id'])
            ->andOnCondition(['type' => self::TYPE_DISTRICT]);
    }

    /**
     * Type
     */
    const TYPE_REGION = 'region';
    const TYPE_CITY = 'city';
    const TYPE_DISTRICT = 'district';

    /**
     * Type Array
     * @return array
     */
    public static function getTypeArray()
    {
        return [
            self::TYPE_REGION => Yii::t('model', 'Region'),
            self::TYPE_CITY => Yii::t('model', 'City'),
        ];
    }

    /**
     * Status
     */
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    const STATUS_DELETED = -1;

    /**
     * Status Array
     * @param integer|null $status
     * @return array|string
     */
    public static function getStatusArray($status = null)
    {
        $array = [
            self::STATUS_INACTIVE => Yii::t('model', 'Inactive'),
            self::STATUS_ACTIVE => Yii::t('model', 'Active'),
        ];

        return $status === null ? $array : $array[$status];
    }

    /**
     * Status Name
     * @return string
     */
    public function getStatusName()
    {
        $array = [
            self::STATUS_ACTIVE => '<span class="text-bold text-green">' . self::getStatusArray(self::STATUS_ACTIVE) . '</span>',
            self::STATUS_INACTIVE => '<span class="text-bold text-red">' . self::getStatusArray(self::STATUS_INACTIVE) . '</span>',
        ];

        return isset($array[$this->status]) ? $array[$this->status] : '';
    }

    /**
     * Set Status Deleted
     * @return mixed
     */
    public function setStatusDeleted()
    {
        $this->status = self::STATUS_DELETED;
        return $this->save();
    }

    /**
     * self model to array
     * @return array
     */
    public static function getLocationsList()
    {
        return ArrayHelper::map(self::find()
            ->where([
                'status' => self::STATUS_ACTIVE,
                'location_id' => 1
            ])
            ->orderBy(['name' => SORT_ASC])
            ->asArray()
            ->all(), 'id', 'name');
    }
}

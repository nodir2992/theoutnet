<?php

namespace backend\models\post;

use Yii;
use yii\helpers\Url;
use yii\db\ActiveRecord;
use backend\behaviors\SlugBehavior;
use yii\behaviors\TimestampBehavior;
use backend\models\product\Products;
use backend\behaviors\MetaTitleBehavior;
use himiklab\sitemap\behaviors\SitemapBehavior;

/**
 * This is the model class for table "posts".
 *
 * @property integer $id
 * @property integer $category_id
 * @property string $slug
 * @property string $title
 * @property string $image
 * @property string $summary
 * @property string $body
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property integer $views
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property PostCategories $category
 * @property ProductToPost[] $productToPosts
 * @property Products[] $products
 */
class Posts extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'posts';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            [
                'class' => SlugBehavior::className(),
                'attribute' => 'title'
            ],
            [
                'class' => MetaTitleBehavior::className(),
                'attribute' => 'title'
            ],
            'sitemap' => [
                'class' => SitemapBehavior::className(),
                'scope' => function ($model) {
                    /** @var \yii\db\ActiveQuery $model */
                    $model->select(['slug', 'status', 'updated_at']);
                    $model->andWhere(['status' => self::STATUS_ACTIVE]);
                },
                'dataClosure' => function ($model) {
                    /** @var self $model */
                    return [
                        'loc' => Url::to(['/post/view', 'slug' => $model->slug], true),
                        'lastmod' => $model->updated_at,
                        'changefreq' => SitemapBehavior::CHANGEFREQ_DAILY,
                        'priority' => 0.8
                    ];
                }
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'slug', 'title', 'image'], 'required'],
            [['category_id', 'views', 'status', 'created_at', 'updated_at'], 'integer'],
            [['summary', 'body', 'meta_description'], 'string'],
            [['slug', 'title', 'image', 'meta_title', 'meta_keywords'], 'string', 'max' => 255],
            [['slug'], 'unique'],
            [['views'], 'default', 'value' => 0],
            [['status'], 'default', 'value' => self::STATUS_INACTIVE],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => PostCategories::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'category_id' => Yii::t('model', 'Category'),
            'slug' => Yii::t('model', 'Slug'),
            'title' => Yii::t('model', 'Title'),
            'image' => Yii::t('model', 'Image'),
            'summary' => Yii::t('model', 'Summary'),
            'body' => Yii::t('model', 'Body'),
            'meta_title' => Yii::t('model', 'Meta Title'),
            'meta_keywords' => Yii::t('model', 'Meta Keywords'),
            'meta_description' => Yii::t('model', 'Meta Description'),
            'views' => Yii::t('model', 'Views'),
            'status' => Yii::t('model', 'Status'),
            'created_at' => Yii::t('model', 'Created At'),
            'updated_at' => Yii::t('model', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(PostCategories::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductToPosts()
    {
        return $this->hasMany(ProductToPost::className(), ['post_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Products::className(), ['id' => 'product_id'])
            ->viaTable('product_to_post', ['post_id' => 'id'])
            ->andOnCondition(['status' => [Products::STATUS_AVAILABLE, Products::STATUS_NOT_AVAILABLE]]);
    }

    /**
     * Status
     */
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    /**
     * Status Array
     * @param integer|null $status
     * @return array|string
     */
    public static function getStatusArray($status = null)
    {
        $array = [
            self::STATUS_INACTIVE => Yii::t('model', 'Inactive'),
            self::STATUS_ACTIVE => Yii::t('model', 'Active'),
        ];

        return $status === null ? $array : $array[$status];
    }

    /**
     * Status Name
     * @return string
     */
    public function getStatusName()
    {
        $array = [
            self::STATUS_ACTIVE => '<span class="text-bold text-green">' . self::getStatusArray(self::STATUS_ACTIVE) . '</span>',
            self::STATUS_INACTIVE => '<span class="text-bold text-red">' . self::getStatusArray(self::STATUS_INACTIVE) . '</span>',
        ];

        return isset($array[$this->status]) ? $array[$this->status] : '';
    }
}

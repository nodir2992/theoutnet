<?php

namespace backend\models\product;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "product_images".
 *
 * @property integer $product_id
 * @property string $generate_name
 * @property string $name
 * @property string $path
 * @property integer $position
 *
 * @property Products $product
 */
class ProductImages extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_images';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'generate_name', 'name', 'path'], 'required'],
            [['product_id', 'position'], 'integer'],
            [['generate_name'], 'string', 'max' => 64],
            [['name', 'path'], 'string', 'max' => 255],
            [['generate_name'], 'unique'],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_id' => Yii::t('model', 'Product ID'),
            'generate_name' => Yii::t('model', 'Generate Name'),
            'name' => Yii::t('model', 'Name'),
            'path' => Yii::t('model', 'Path'),
            'position' => Yii::t('model', 'Position'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_id']);
    }

    /**
     * @inheritdoc
     * @return \yii\db\ActiveQuery
     */
    public static function find()
    {
        return parent::find()->orderBy(['position' => SORT_ASC]);
    }
}

<?php

namespace backend\models\product;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "product_to_sticker".
 *
 * @property integer $product_id
 * @property integer $sticker_id
 *
 * @property Products $product
 * @property Stickers $sticker
 */
class ProductToSticker extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_to_sticker';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'sticker_id'], 'required'],
            [['product_id', 'sticker_id'], 'integer'],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['sticker_id'], 'exist', 'skipOnError' => true, 'targetClass' => Stickers::className(), 'targetAttribute' => ['sticker_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_id' => Yii::t('model', 'Product ID'),
            'sticker_id' => Yii::t('model', 'Sticker ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSticker()
    {
        return $this->hasOne(Stickers::className(), ['id' => 'sticker_id']);
    }
}

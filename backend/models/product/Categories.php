<?php

namespace backend\models\product;

use Yii;
use yii\helpers\Url;
use yii\db\ActiveRecord;
use backend\behaviors\SlugBehavior;
use yii\behaviors\TimestampBehavior;
use backend\behaviors\MetaTitleBehavior;
use himiklab\sitemap\behaviors\SitemapBehavior;

/**
 * This is the model class for table "product_categories".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $slug
 * @property string $name
 * @property string $image
 * @property string $description
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property integer $weight
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Categories $parent
 * @property Categories[] $categories
 * @property Categories[] $activeChilds
 * @property ProductToCategory[] $productToCategories
 * @property Products[] $products
 */
class Categories extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_categories';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            MetaTitleBehavior::className(),
            SlugBehavior::className(),
            'sitemap' => [
                'class' => SitemapBehavior::className(),
                'scope' => function ($model) {
                    /** @var \yii\db\ActiveQuery $model */
                    $model->select(['slug', 'status', 'updated_at']);
                    $model->andWhere(['status' => self::STATUS_ACTIVE]);
                },
                'dataClosure' => function ($model) {
                    /** @var self $model */
                    return [
                        'loc' => Url::to(['/product/category', 'slug' => $model->slug], true),
                        'lastmod' => $model->updated_at,
                        'changefreq' => SitemapBehavior::CHANGEFREQ_DAILY,
                        'priority' => 0.8
                    ];
                }
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'image'], 'required'],
            [['parent_id', 'weight', 'status', 'created_at', 'updated_at'], 'integer'],
            [['description', 'meta_keywords', 'meta_description'], 'string'],
            [['slug', 'name', 'image', 'meta_title'], 'string', 'max' => 255],
            [['slug'], 'unique'],
            [['status'], 'default', 'value' => self::STATUS_INACTIVE],
            [['weight'], 'default', 'value' => 0],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Categories::className(), 'targetAttribute' => ['parent_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'parent_id' => Yii::t('model', 'Parent'),
            'slug' => Yii::t('model', 'Slug'),
            'name' => Yii::t('model', 'Name'),
            'image' => Yii::t('model', 'Image'),
            'description' => Yii::t('model', 'Description'),
            'meta_title' => Yii::t('model', 'Meta Title'),
            'meta_keywords' => Yii::t('model', 'Meta Keywords'),
            'meta_description' => Yii::t('model', 'Meta Description'),
            'weight' => Yii::t('model', 'Weight'),
            'status' => Yii::t('model', 'Status'),
            'created_at' => Yii::t('model', 'Created At'),
            'updated_at' => Yii::t('model', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Categories::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Categories::className(), ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActiveChilds()
    {
        return $this->hasMany(Categories::className(), ['parent_id' => 'id'])
            ->andOnCondition(['status' => static::STATUS_ACTIVE]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductToCategories()
    {
        return $this->hasMany(ProductToCategory::className(), ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Products::className(), ['id' => 'product_id'])->viaTable('product_to_category', ['category_id' => 'id']);
    }

    /**
     * Status
     */
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    /**
     * Status Array
     * @param integer|null $status
     * @return array|string
     */
    public static function getStatusArray($status = null)
    {
        $array = [
            self::STATUS_INACTIVE => Yii::t('model', 'Inactive'),
            self::STATUS_ACTIVE => Yii::t('model', 'Active'),
        ];

        return $status === null ? $array : $array[$status];
    }

    /**
     * Status Name
     * @return string
     */
    public function getStatusName()
    {
        $array = [
            self::STATUS_ACTIVE => '<span class="text-bold text-green">' . self::getStatusArray(self::STATUS_ACTIVE) . '</span>',
            self::STATUS_INACTIVE => '<span class="text-bold text-red">' . self::getStatusArray(self::STATUS_INACTIVE) . '</span>',
        ];

        return isset($array[$this->status]) ? $array[$this->status] : '';
    }

    /**
     * Categories Array
     * @param integer $id
     * @return array
     */
    public static function getCategoriesArray($id = 0)
    {
        $items = [];
        $index = '';
        $models = ($id == 0) ? self::findAll(['parent_id' => NULL]) :
            self::find()->where(['parent_id' => NULL])->andWhere(['!=', 'id', $id])->all();

        if (!empty($models)) {
            foreach ($models as $model) {
                $items[$model->id] = $index . $model->name;
                $childs = ($id == 0) ? self::findAll(['parent_id' => $model->id]) :
                    self::find()->where(['parent_id' => $model->id])->andWhere(['!=', 'id', $id])->all();

                if (!empty($childs))
                    $items = self::getCategoriesArrayChilds($id, $childs, $items, $index . '—');
            }
        }

        return $items;
    }

    /**
     * @param integer $id
     * @param array $models
     * @param array $items
     * @param string $index
     * @return array
     */
    protected static function getCategoriesArrayChilds($id, $models, $items, $index)
    {
        /** @var self $model */
        foreach ($models as $model) {
            $items[$model->id] = $index . $model->name;
            $childs = ($id == 0) ? self::findAll(['parent_id' => $model->id]) :
                self::find()->where(['parent_id' => $model->id])->andWhere(['!=', 'id', $id])->all();

            if (!empty($childs))
                $items = self::getCategoriesArrayChilds($id, $childs, $items, $index . '—');
        }

        return $items;
    }

}

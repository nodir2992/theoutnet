<?php

namespace backend\models\product;

use backend\models\comment\Comments;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use backend\behaviors\SlugBehavior;
use yii\behaviors\TimestampBehavior;
use backend\behaviors\MetaTitleBehavior;
use himiklab\sitemap\behaviors\SitemapBehavior;

/**
 * This is the model class for table "products".
 *
 * @property integer $id
 * @property integer $brand_id
 * @property integer $group_id
 * @property integer $discount_id
 * @property string $barcode
 * @property string $slug
 * @property string $name
 * @property string $description
 * @property string $information
 * @property string $detail
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property integer $price
 * @property integer $old_price
 * @property integer $quantity
 * @property integer $timer_start
 * @property integer $timer_end
 * @property integer $views
 * @property integer $votes
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ProductImages[] $productImages
 * @property ProductToCategory[] $productToCategories
 * @property Categories[] $categories
 * @property ProductToFilter[] $productToFilters
 * @property FilterItems[] $filterItems
 * @property ProductToRibbon[] $productToRibbons
 * @property Ribbons[] $ribbons
 * @property ProductToSticker[] $productToStickers
 * @property Stickers[] $stickers
 * @property Stickers[] $activeStickers
 * @property Stickers[] $activeStickersBottom
 * @property Stickers[] $activeStickersTop
 * @property Brands $brand
 * @property Discounts $discount
 * @property Groups $group
 */
class Products extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'products';
    }

    public $filter_ids;
    public $ribbon_ids;
    public $ribbon_id;
    public $sticker_ids;
    public $sticker_id;
    public $category_ids;
    public $category_id;
    public $uploaded_images;
    public $deleted_images;
    public $sorted_images;
    public $dataFilters;
    public $str_timer;
    public $str_timer_start;
    public $str_timer_end;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            MetaTitleBehavior::className(),
            SlugBehavior::className(),
            'sitemap' => [
                'class' => SitemapBehavior::className(),
                'scope' => function ($model) {
                    /** @var \yii\db\ActiveQuery $model */
                    $model->select(['slug', 'status', 'updated_at']);
                    $model->andWhere(['status' => [self::STATUS_AVAILABLE, self::STATUS_NOT_AVAILABLE]]);
                },
                'dataClosure' => function ($model) {
                    /** @var self $model */
                    return [
                        'loc' => Url::to(['/product/view', 'slug' => $model->slug], true),
                        'lastmod' => $model->updated_at,
                        'changefreq' => SitemapBehavior::CHANGEFREQ_DAILY,
                        'priority' => 0.8
                    ];
                }
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['brand_id', 'group_id', 'barcode', 'name', 'price'], 'required'],
            [['brand_id', 'group_id', 'discount_id', 'quantity', 'timer_start', 'timer_end', 'views', 'votes', 'status', 'created_at', 'updated_at'], 'integer'],
            [['price', 'old_price'], 'number'],
            [['description', 'information', 'detail', 'meta_keywords', 'meta_description'], 'string'],
            [['barcode'], 'string', 'max' => 64],
            [['slug', 'name', 'meta_title'], 'string', 'max' => 255],
            [['barcode'], 'unique'],
            [['slug'], 'unique'],
            [['status'], 'default', 'value' => self::STATUS_INACTIVE],
            [['category_ids'], 'required'],
            [['filter_ids', 'ribbon_ids', 'sticker_ids', 'category_ids'], 'each', 'rule' => ['integer']],
            [['uploaded_images', 'deleted_images', 'sorted_images', 'str_timer_start', 'str_timer_end'], 'string'],
            [['str_timer'], 'boolean'],
            [['ribbon_id', 'sticker_id', 'category_id'], 'integer'],
            [['brand_id'], 'exist', 'skipOnError' => true, 'targetClass' => Brands::className(), 'targetAttribute' => ['brand_id' => 'id']],
            [['discount_id'], 'exist', 'skipOnError' => true, 'targetClass' => Discounts::className(), 'targetAttribute' => ['discount_id' => 'id']],
            [['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => Groups::className(), 'targetAttribute' => ['group_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'brand_id' => Yii::t('model', 'Brand'),
            'group_id' => Yii::t('model', 'Group'),
            'discount_id' => Yii::t('model', 'Discount'),
            'barcode' => Yii::t('model', 'Barcode'),
            'slug' => Yii::t('model', 'Slug'),
            'name' => Yii::t('model', 'Name'),
            'description' => Yii::t('model', 'Description'),
            'information' => Yii::t('model', 'Information'),
            'detail' => Yii::t('model', 'Detail'),
            'meta_title' => Yii::t('model', 'Meta Title'),
            'meta_keywords' => Yii::t('model', 'Meta Keywords'),
            'meta_description' => Yii::t('model', 'Meta Description'),
            'price' => Yii::t('model', 'Price'),
            'old_price' => Yii::t('model', 'Old Price'),
            'quantity' => Yii::t('model', 'Quantity'),
            'views' => Yii::t('model', 'Views'),
            'votes' => Yii::t('model', 'Votes'),
            'status' => Yii::t('model', 'Status'),
            'created_at' => Yii::t('model', 'Created At'),
            'updated_at' => Yii::t('model', 'Updated At'),
            'filter_ids' => Yii::t('model', 'Filters'),
            'ribbon_ids' => Yii::t('model', 'Ribbons'),
            'ribbon_id' => Yii::t('model', 'Ribbon'),
            'sticker_ids' => Yii::t('model', 'Stickers'),
            'sticker_id' => Yii::t('model', 'Sticker'),
            'category_ids' => Yii::t('model', 'Categories'),
            'category_id' => Yii::t('model', 'Category'),
            'uploaded_images' => Yii::t('model', 'Images'),
            'str_timer_start' => Yii::t('model', 'Timer Start'),
            'str_timer_end' => Yii::t('model', 'Timer End'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductImages()
    {
        return $this->hasMany(ProductImages::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductToCategories()
    {
        return $this->hasMany(ProductToCategory::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Categories::className(), ['id' => 'category_id'])
            ->viaTable('product_to_category', ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductToFilters()
    {
        return $this->hasMany(ProductToFilter::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilterItems()
    {
        return $this->hasMany(FilterItems::className(), ['id' => 'filter_item_id'])
            ->viaTable('product_to_filter', ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductToRibbons()
    {
        return $this->hasMany(ProductToRibbon::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRibbons()
    {
        return $this->hasMany(Ribbons::className(), ['id' => 'ribbon_id'])
            ->viaTable('product_to_ribbon', ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductToStickers()
    {
        return $this->hasMany(ProductToSticker::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStickers()
    {
        return $this->hasMany(Stickers::className(), ['id' => 'sticker_id'])
            ->viaTable('product_to_sticker', ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActiveStickers()
    {
        return $this->hasMany(Stickers::className(), ['id' => 'sticker_id'])
            ->viaTable('product_to_sticker', ['product_id' => 'id'])
            ->andOnCondition(['status' => Stickers::STATUS_ACTIVE]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActiveStickersBottom()
    {
        return $this->hasMany(Stickers::className(), ['id' => 'sticker_id'])
            ->viaTable('product_to_sticker', ['product_id' => 'id'])
            ->andOnCondition(['status' => Stickers::STATUS_ACTIVE, 'position' => Stickers::POSITION_BOTTOM]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActiveStickersTop()
    {
        return $this->hasMany(Stickers::className(), ['id' => 'sticker_id'])
            ->viaTable('product_to_sticker', ['product_id' => 'id'])
            ->andOnCondition(['status' => Stickers::STATUS_ACTIVE, 'position' => Stickers::POSITION_TOP]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrand()
    {
        return $this->hasOne(Brands::className(), ['id' => 'brand_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscount()
    {
        return $this->hasOne(Discounts::className(), ['id' => 'discount_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(Groups::className(), ['id' => 'group_id']);
    }

    /**
     * @return ActiveDataProvider
     */
    public function getCommentsDataProvider()
    {
        return new ActiveDataProvider([
            'query' => Comments::find()->where([
                'model' => $this::tableName(),
                'model_id' => $this->id,
                'status' => Comments::STATUS_ACTIVE,
            ]),
            'sort' => false,
            'pagination' => [
                'defaultPageSize' => 10
            ]
        ]);
    }

    /**
     * Set Status Deleted
     * @return mixed
     */
    public function setStatusDeleted()
    {
        $this->status = self::STATUS_DELETED;
        return $this->save();
    }

    /**
     * Status
     */
    const STATUS_NOT_AVAILABLE = 0;
    const STATUS_AVAILABLE = 1;
    const STATUS_INACTIVE = 10;
    const STATUS_DELETED = -10;

    /**
     * Status Array
     * @param integer|null $status
     * @return array|string
     */
    public static function getStatusArray($status = null)
    {
        $array = [
            self::STATUS_INACTIVE => Yii::t('model', 'Inactive'),
            self::STATUS_AVAILABLE => Yii::t('model', 'Available'),
            self::STATUS_NOT_AVAILABLE => Yii::t('model', 'Not Available'),
        ];

        return $status === null ? $array : $array[$status];
    }

    /**
     * Status Name
     * @return string
     */
    public function getStatusName()
    {
        $array = [
            self::STATUS_AVAILABLE => '<span class="text-bold text-green">' . self::getStatusArray(self::STATUS_AVAILABLE) . '</span>',
            self::STATUS_NOT_AVAILABLE => '<span class="text-bold text-warning">' . self::getStatusArray(self::STATUS_NOT_AVAILABLE) . '</span>',
            self::STATUS_INACTIVE => '<span class="text-bold text-red">' . self::getStatusArray(self::STATUS_INACTIVE) . '</span>',
        ];

        return isset($array[$this->status]) ? $array[$this->status] : '';
    }

    /**
     * Is Available
     * @return bool
     */
    public function isAvailable()
    {
        return $this->status === self::STATUS_AVAILABLE ? true : false;
    }

    /**
     * Dollar Rate
     * @return string
     */
    protected function getDollarRate()
    {
        /** @var $dollar DollarRate */
        return ($dollar = DollarRate::find()->orderBy(['id' => SORT_DESC])->one()) ? $dollar->value : 1;
    }

    /**
     * Price
     * @return string
     */
    public function getPriceBySum()
    {
        return $this->price * $this->getDollarRate();
    }

    /**
     * Price As Currency
     * @return string
     */
    public function getPriceAsCurrency()
    {
        return Yii::$app->formatter->asDecimal($this->getPriceBySum(), 0)
            . Yii::t('model', '&nbsp;<i>сум</i>');
    }

    /**
     * Old Price As Currency
     * @return string
     */
    public function getOldPriceAsCurrency()
    {
        return $this->old_price ? Yii::$app->formatter->asDecimal(($this->old_price * $this->getDollarRate()), 0)
            . Yii::t('model', '&nbsp;<i>сум</i>') : $this->old_price;
    }

    /**
     * Default Image Url
     * @return string
     */
    public function getDefaultImageUrl()
    {
        return isset($this->productImages[0]) ? $this->productImages[0]->path : '';
    }

    /**
     * Brands model to array
     * @return array
     */
    public static function getBrandsList()
    {
        return ArrayHelper::map(Brands::find()->where(['status' => Brands::STATUS_ACTIVE])->asArray()->all(), 'id', 'name');
    }

    /**
     * Groups model to array
     * @return array
     */
    public static function getGroupsList()
    {
        return ArrayHelper::map(Groups::find()->where(['status' => Groups::STATUS_ACTIVE])->asArray()->all(), 'id', 'name');
    }

    /**
     * Discounts model to array
     * @return array
     */
    public static function getDiscountsList()
    {
        return ArrayHelper::map(Discounts::find()->where(['status' => Discounts::STATUS_ACTIVE])->asArray()->all(), 'id', 'name');
    }

    /**
     * Ribbons model to array
     * @return array
     */
    public static function getRibbonsList()
    {
        return ArrayHelper::map(Ribbons::find()->where(['status' => Ribbons::STATUS_ACTIVE])->asArray()->all(), 'id', 'name');
    }

    /**
     * Stickers model to array
     * @return array
     */
    public static function getStickersList()
    {
        return ArrayHelper::map(Stickers::find()->where(['status' => Stickers::STATUS_ACTIVE])->asArray()->all(), 'id', 'name');
    }

    /**
     * Categories model to array
     * @return array
     */
    public static function getCategoriesList()
    {
        return Categories::getCategoriesArray();
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        parent::afterFind();
        $this->filter_ids = ArrayHelper::getColumn($this->productToFilters, 'filter_item_id');
        $this->ribbon_ids = ArrayHelper::getColumn($this->productToRibbons, 'ribbon_id');
        $this->sticker_ids = ArrayHelper::getColumn($this->productToStickers, 'sticker_id');
        $this->category_ids = ArrayHelper::getColumn($this->productToCategories, 'category_id');
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->discount_id && empty($this->old_price)) {
                $this->old_price = $this->price;
                $this->price = ((100 - $this->discount->percentage) / 100) * $this->price;
            }
            if ($this->str_timer) {
                $this->timer_start = empty($this->str_timer_start) ? null : strtotime($this->str_timer_start);
                $this->timer_end = empty($this->str_timer_end) ? null : strtotime($this->str_timer_end);
            }

            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if (!empty($filter_ids = $this->filter_ids)) {
            if (!$insert) ProductToFilter::deleteAll(['product_id' => $this->id]);
            foreach ($filter_ids as $filter_id) {
                $relation = new ProductToFilter();
                $relation->product_id = $this->id;
                $relation->filter_item_id = $filter_id;
                $relation->save();
            }
        } else {
            if (!$insert) ProductToFilter::deleteAll(['product_id' => $this->id]);
        }

        if (!empty($ribbon_ids = $this->ribbon_ids)) {
            if (!$insert) ProductToRibbon::deleteAll(['product_id' => $this->id]);
            foreach ($ribbon_ids as $ribbon_id) {
                $relation = new ProductToRibbon();
                $relation->product_id = $this->id;
                $relation->ribbon_id = $ribbon_id;
                $relation->save();
            }
        } else {
            if (!$insert) ProductToRibbon::deleteAll(['product_id' => $this->id]);
        }

        if (!empty($sticker_ids = $this->sticker_ids)) {
            if (!$insert) ProductToSticker::deleteAll(['product_id' => $this->id]);
            foreach ($sticker_ids as $sticker_id) {
                $relation = new ProductToSticker();
                $relation->product_id = $this->id;
                $relation->sticker_id = $sticker_id;
                $relation->save();
            }
        } else {
            if (!$insert) ProductToSticker::deleteAll(['product_id' => $this->id]);
        }

        if (!empty($category_ids = $this->category_ids)) {
            if (!$insert) ProductToCategory::deleteAll(['product_id' => $this->id]);
            foreach ($category_ids as $category_id) {
                $relation = new ProductToCategory();
                $relation->product_id = $this->id;
                $relation->category_id = $category_id;
                $relation->save();
            }
        }

        if (!empty($uploadedImages = json_decode($this->uploaded_images))) {
            if (empty($position = ProductImages::find()->where(['product_id' => $this->id])->max('position'))) $position = 0;
            foreach ($uploadedImages as $file) {
                $image = new ProductImages();
                $image->product_id = $this->id;
                $image->generate_name = $file->generate_name;
                $image->name = $file->name;
                $image->path = $file->path;
                $image->position = $position;
                $image->save();
                $position++;
            }
        }

        if (!empty($deletedImages = json_decode($this->deleted_images))) {
            foreach ($deletedImages as $generate_name) {
                if (!empty($image = ProductImages::findOne(['generate_name' => $generate_name]))) {
                    $basePath = str_replace("backend", "", Yii::$app->basePath);
                    if ($image->delete()) unlink($basePath . $image->path);
                }
            }
        }

        if (!empty($sortedImages = json_decode($this->sorted_images))) {
            $position = 0;
            foreach ($sortedImages as $item) {
                if (!empty($image = ProductImages::findOne(['generate_name' => $item->key]))) {
                    $image->position = $position;
                    $image->save();
                    $position++;
                }
            }
        }
    }
}

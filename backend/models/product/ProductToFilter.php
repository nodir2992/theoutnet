<?php

namespace backend\models\product;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "product_to_filter".
 *
 * @property integer $product_id
 * @property integer $filter_item_id
 *
 * @property FilterItems $filterItem
 * @property Products $product
 */
class ProductToFilter extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_to_filter';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'filter_item_id'], 'required'],
            [['product_id', 'filter_item_id'], 'integer'],
            [['filter_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => FilterItems::className(), 'targetAttribute' => ['filter_item_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_id' => Yii::t('model', 'Product ID'),
            'filter_item_id' => Yii::t('model', 'Filter Item ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilterItem()
    {
        return $this->hasOne(FilterItems::className(), ['id' => 'filter_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_id']);
    }
}

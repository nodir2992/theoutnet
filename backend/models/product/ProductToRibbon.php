<?php

namespace backend\models\product;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "product_to_ribbon".
 *
 * @property integer $product_id
 * @property integer $ribbon_id
 *
 * @property Products $product
 * @property Ribbons $ribbon
 */
class ProductToRibbon extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_to_ribbon';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'ribbon_id'], 'required'],
            [['product_id', 'ribbon_id'], 'integer'],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['ribbon_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ribbons::className(), 'targetAttribute' => ['ribbon_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_id' => Yii::t('model', 'Product ID'),
            'ribbon_id' => Yii::t('model', 'Ribbon ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRibbon()
    {
        return $this->hasOne(Ribbons::className(), ['id' => 'ribbon_id']);
    }
}

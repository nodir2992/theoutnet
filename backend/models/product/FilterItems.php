<?php

namespace backend\models\product;

use Yii;
use yii\db\ActiveRecord;
use backend\behaviors\SlugBehavior;

/**
 * This is the model class for table "product_filter_items".
 *
 * @property integer $id
 * @property integer $filter_id
 * @property string $slug
 * @property string $name
 *
 * @property Filters $filter
 * @property ProductToFilter[] $productToFilters
 * @property Products[] $products
 */
class FilterItems extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_filter_items';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            SlugBehavior::className(),
        ];
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['filter_id', 'name'], 'required'],
            [['filter_id'], 'integer'],
            [['slug', 'name'], 'string', 'max' => 255],
            [['slug'], 'unique'],
            [['filter_id'], 'exist', 'skipOnError' => true, 'targetClass' => Filters::className(), 'targetAttribute' => ['filter_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'filter_id' => Yii::t('model', 'Filter ID'),
            'slug' => Yii::t('model', 'Slug'),
            'name' => Yii::t('model', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilter()
    {
        return $this->hasOne(Filters::className(), ['id' => 'filter_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductToFilters()
    {
        return $this->hasMany(ProductToFilter::className(), ['filter_item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Products::className(), ['id' => 'product_id'])->viaTable('product_to_filter', ['filter_item_id' => 'id']);
    }
}

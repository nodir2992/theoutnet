<?php

namespace backend\models\product;

use Yii;
use yii\db\ActiveRecord;
use backend\behaviors\SlugBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "product_ribbons".
 *
 * @property integer $id
 * @property string $key
 * @property string $slug
 * @property string $name
 * @property string $icon
 * @property string $description
 * @property integer $weight
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ProductToRibbon[] $productToRibbons
 * @property Products[] $products
 */
class Ribbons extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_ribbons';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            SlugBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key', 'name', 'icon'], 'required'],
            [['description'], 'string'],
            [['weight', 'status', 'created_at', 'updated_at'], 'integer'],
            [['key', 'slug', 'name', 'icon'], 'string', 'max' => 255],
            [['key', 'slug'], 'unique'],
            [['key'], 'match', 'pattern' => '/^[A-Za-z0-9_]+$/u'],
            [['status'], 'default', 'value' => self::STATUS_INACTIVE],
            [['weight'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'key' => Yii::t('model', 'Key'),
            'slug' => Yii::t('model', 'Slug'),
            'name' => Yii::t('model', 'Name'),
            'icon' => Yii::t('model', 'Icon'),
            'description' => Yii::t('model', 'Description'),
            'weight' => Yii::t('model', 'Weight'),
            'status' => Yii::t('model', 'Status'),
            'created_at' => Yii::t('model', 'Created At'),
            'updated_at' => Yii::t('model', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductToRibbons()
    {
        return $this->hasMany(ProductToRibbon::className(), ['ribbon_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Products::className(), ['id' => 'product_id'])
            ->orderBy(['id' => SORT_DESC])
            ->viaTable('product_to_ribbon', ['ribbon_id' => 'id'])
            ->andOnCondition(['status' => [Products::STATUS_AVAILABLE, Products::STATUS_NOT_AVAILABLE]]);
    }

    /**
     * Set Status Deleted
     * @return mixed
     */
    public function setStatusDeleted()
    {
        $this->status = self::STATUS_DELETED;
        return $this->save();
    }

    /**
     * Status
     */
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    const STATUS_DELETED = -1;

    /**
     * Status Array
     * @param integer|null $status
     * @return array|string
     */
    public static function getStatusArray($status = null)
    {
        $array = [
            self::STATUS_INACTIVE => Yii::t('model', 'Inactive'),
            self::STATUS_ACTIVE => Yii::t('model', 'Active'),
        ];

        return $status === null ? $array : $array[$status];
    }

    /**
     * Status Name
     * @return string
     */
    public function getStatusName()
    {
        $array = [
            self::STATUS_ACTIVE => '<span class="text-bold text-green">' . self::getStatusArray(self::STATUS_ACTIVE) . '</span>',
            self::STATUS_INACTIVE => '<span class="text-bold text-red">' . self::getStatusArray(self::STATUS_INACTIVE) . '</span>',
        ];

        return isset($array[$this->status]) ? $array[$this->status] : '';
    }
}

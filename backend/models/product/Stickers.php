<?php

namespace backend\models\product;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use backend\behaviors\SlugBehavior;

/**
 * This is the model class for table "product_stickers".
 *
 * @property integer $id
 * @property string $slug
 * @property string $name
 * @property string $image
 * @property string $link
 * @property string $description
 * @property integer $position
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ProductToSticker[] $productToStickers
 * @property Products[] $products
 */
class Stickers extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_stickers';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            SlugBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['slug', 'name', 'image', 'link'], 'required'],
            [['description'], 'string'],
            [['position', 'status', 'created_at', 'updated_at'], 'integer'],
            [['slug', 'name', 'image', 'link'], 'string', 'max' => 255],
            [['slug'], 'unique'],
            [['status'], 'default', 'value' => self::STATUS_INACTIVE],
            [['position'], 'default', 'value' => self::POSITION_TOP],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'slug' => Yii::t('model', 'Slug'),
            'name' => Yii::t('model', 'Name'),
            'image' => Yii::t('model', 'Image'),
            'link' => Yii::t('model', 'Link'),
            'description' => Yii::t('model', 'Description'),
            'position' => Yii::t('model', 'Position'),
            'status' => Yii::t('model', 'Status'),
            'created_at' => Yii::t('model', 'Created At'),
            'updated_at' => Yii::t('model', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductToStickers()
    {
        return $this->hasMany(ProductToSticker::className(), ['sticker_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Products::className(), ['id' => 'product_id'])->viaTable('product_to_sticker', ['sticker_id' => 'id']);
    }

    /**
     * Position
     */
    const POSITION_TOP = 0;
    const POSITION_BOTTOM = 1;

    /**
     * Position List
     * @param integer|null $position
     * @return array
     */
    public static function getPositionArray($position = null)
    {
        $array = [
            self::POSITION_TOP => 'Top',
            self::POSITION_BOTTOM => 'Bottom',
        ];

        return $position === null ? $array : $array[$position];
    }

    /**
     * Position Name
     * @return string
     */
    public function getPositionName()
    {
        $array = [
            self::POSITION_TOP => '<span class="text-bold text-blue">' . self::getPositionArray(self::POSITION_TOP) . '</span>',
            self::POSITION_BOTTOM => '<span class="text-bold text-blue">' . self::getPositionArray(self::POSITION_BOTTOM) . '</span>',
        ];

        return isset($array[$this->position]) ? $array[$this->position] : '';
    }

    /**
     * Status
     */
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    /**
     * Status Array
     * @param integer|null $status
     * @return array|string
     */
    public static function getStatusArray($status = null)
    {
        $array = [
            self::STATUS_INACTIVE => Yii::t('model', 'Inactive'),
            self::STATUS_ACTIVE => Yii::t('model', 'Active'),
        ];

        return $status === null ? $array : $array[$status];
    }

    /**
     * Status Name
     * @return string
     */
    public function getStatusName()
    {
        $array = [
            self::STATUS_ACTIVE => '<span class="text-bold text-green">' . self::getStatusArray(self::STATUS_ACTIVE) . '</span>',
            self::STATUS_INACTIVE => '<span class="text-bold text-red">' . self::getStatusArray(self::STATUS_INACTIVE) . '</span>',
        ];

        return isset($array[$this->status]) ? $array[$this->status] : '';
    }
}

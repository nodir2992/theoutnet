<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\grid\GridView;
use common\models\User;
use backend\widgets\PageSize;
use backend\widgets\ActionsApply;
use backend\modules\admin\components\Helper;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\admin\models\searchs\User */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('rbac-admin', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index box box-default">
    <div class="box-body">
        <p>
            <?php if (Helper::checkRoute('create'))
                echo Html::a(Yii::t('rbac-admin', 'Create'), ['create'], ['class' => 'btn btn-success']); ?>
        </p>


        <?php ActionsApply::begin(['template' => '<div class="form-inline margin-bottom-5">{list}{button}']); ?>
        <?= PageSize::widget([
            'template' => Helper::checkRoute('apply') ? '{list}&nbsp;{label}</div>' :
                '<div class="form-inline margin-bottom-5">{list}&nbsp;{label}</div>'
        ]); ?>

        <div class="table-responsive">
            <?php Pjax::begin(); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'filterSelector' => 'select[name="per_page"]',
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    ['class' => 'yii\grid\CheckboxColumn'],

                    [
                        'attribute' => 'id',
                        'options' => ['width' => '70'],
                    ],
                    'username',
                    'email:email',
                    'first_name',
                    'last_name',
                    [
                        'attribute' => 'status',
                        'format' => 'raw',
                        'filter' => User::getStatusText(),
                        'value' => function ($model) {
                            /* @var $model common\models\User */
                            return $model->getStatusName();
                        },
                    ],
                    [
                        'attribute' => 'created_at',
                        'filter' => false,
                        'value' => function ($model) {
                            /* @var $model common\models\User */
                            return Yii::$app->formatter->asDatetime($model->created_at);
                        },
                    ],
                    [
                        'attribute' => 'updated_at',
                        'filter' => false,
                        'value' => function ($model) {
                            /* @var $model common\models\User */
                            return Yii::$app->formatter->asDatetime($model->updated_at);
                        },
                    ],

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => Helper::filterActionColumn(['activate', 'view', 'update', 'delete']),
                        'buttons' => [
                            'activate' => function ($url, $model) {
                                /* @var $model common\models\User */
                                if ($model->status == User::STATUS_INACTIVE) {
                                    $options = [
                                        'title' => Yii::t('rbac-admin', 'Activate'),
                                        'aria-label' => Yii::t('rbac-admin', 'Activate'),
                                        'data-confirm' => Yii::t('rbac-admin', 'Are you sure you want to activate this user?'),
                                        'data-method' => 'post',
                                        'data-pjax' => '0',
                                    ];
                                    return Html::a('<span class="glyphicon glyphicon-ok"></span>', $url, $options);
                                } else {
                                    return '';
                                }

                            }
                        ]
                    ],
                ],
            ]) ?>
            <?php Pjax::end(); ?>
        </div>
        <?php ActionsApply::end(); ?>

    </div>
</div>

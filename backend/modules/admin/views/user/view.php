<?php

use backend\models\location\Locations;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\widgets\DetailView;
use rmrevin\yii\fontawesome\FA;
use backend\modules\admin\components\Helper;
use backend\modules\admin\assets\AnimateAsset;

/* @var $this yii\web\View */
/* @var $modelUser common\models\User */
/* @var $modelAssignment backend\modules\admin\models\Assignment */

$this->title = $modelUser->username;
$this->params['breadcrumbs'][] = ['label' => Yii::t('rbac-admin', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

AnimateAsset::register($this);

$opts = Json::htmlEncode([
    'items' => $modelAssignment->getItems(),
]);
$this->registerJs("var _opts = {$opts};");
$this->registerJs($this->render('_script.js'));
$animateIcon = ' <i class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></i>';
?>

<div class="user-view box box-info">
    <div class="box-body">

        <p class="btn-group">
            <?php
            if (Helper::checkRoute('update'))
                echo Html::a(Yii::t('rbac-admin', 'Update'), ['update', 'id' => $modelUser->id], ['class' => 'btn btn-primary']);

            if (Helper::checkRoute('delete'))
                echo Html::a(Yii::t('rbac-admin', 'Delete'), ['delete', 'id' => $modelUser->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ]); ?>
        </p>

        <div class="row">
            <div class="col-lg-7 col-md-11 col-sm-12">
                <?= DetailView::widget([
                    'model' => $modelUser,
                    'attributes' => [
                        'username',
                        'email:email',
                        'first_name',
                        'last_name',
                        'created_at:datetime',
                        'updated_at:datetime',
                        'info:ntext',
                        'phone',
                        'location_id',
                        'address',
                        [
                            'attribute' => 'image',
                            'format' => 'raw',
                            'value' => $modelUser->image ? Html::img($modelUser->image, ['alt' => 'picture', 'width' => 100]) : $modelUser->image,
                        ],
                        [
                            'attribute' => 'status',
                            'format' => 'raw',
                            'value' => $modelUser->getStatusName(),
                        ],
                    ],
                ]) ?>

            </div>
        </div>

        <?php if (Helper::checkRoute('assign') && Helper::checkRoute('revoke')): ?>
            <div class="row">
                <div class="col-lg-3 col-sm-5">
                    <input class="form-control search" data-target="available"
                           placeholder="<?= Yii::t('rbac-admin', 'Search for available'); ?>">
                    <select multiple size="20" class="form-control list" data-target="available">
                    </select>
                </div>
                <div class="col-md-1 col-sm-2 padding-top-165" align="center">
                    <?= Html::a(FA::i('angle-double-right') . $animateIcon, ['assign', 'id' => (string)$modelAssignment->id], [
                        'class' => 'btn btn-success btn-assign',
                        'data-target' => 'available',
                        'title' => Yii::t('rbac-admin', 'Assign'),
                    ]) ?><br>
                    <?= Html::a(FA::i('angle-double-left') . $animateIcon, ['revoke', 'id' => (string)$modelAssignment->id], [
                        'class' => 'btn btn-danger btn-assign',
                        'data-target' => 'assigned',
                        'title' => Yii::t('rbac-admin', 'Remove'),
                    ]) ?>
                </div>
                <div class="col-lg-3 col-sm-5">
                    <input class="form-control search" data-target="assigned"
                           placeholder="<?= Yii::t('rbac-admin', 'Search for assigned'); ?>">
                    <select multiple size="20" class="form-control list" data-target="assigned">
                    </select>
                </div>
            </div>
        <?php endif; ?>

    </div>
</div>
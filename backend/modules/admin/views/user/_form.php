<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use rmrevin\yii\fontawesome\FA;
use mihaildev\elfinder\InputFile;
use common\models\User as UserModel;
use backend\modules\admin\models\form\User as UserForm;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model UserForm */
?>

<div class="box-body">
    <div class="row">
        <div class="col-lg-4 col-md-8">
            <?php $form = ActiveForm::begin(['id' => 'form-update']); ?>
            <div class="row">
                <div class="col-lg-12">
                    <?= $form->field($model, 'username')->textInput() ?>
                    <?= $form->field($model, 'email')->textInput() ?>
                    <?= $form->field($model, 'first_name')->textInput() ?>
                    <?= $form->field($model, 'last_name')->textInput() ?>

                    <?= $form->field($model, 'image')->widget(InputFile::className(), [
                        'controller' => 'elfinder',
                        'path' => '/users',
                        'filter' => 'image',
                        'template' => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
                        'options' => ['class' => 'form-control'],
                        'buttonOptions' => ['class' => 'btn btn-warning'],
                        'buttonName' => FA::i(FA::_PHOTO),
                    ]) ?>

                    <?= $form->field($model, 'phone')->widget(MaskedInput::className(),
                        ['mask' => '+\\9\\9899-999-9999']) ?>

                    <?= $form->field($model, 'location_id')->dropDownList($model->getLocationsList()) ?>

                    <?= $form->field($model, 'address') ?>


                    <?= $form->field($model, 'password')->passwordInput() ?>

                    <?= $form->field($model, 'info')->textarea(['rows' => 4]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <?= $form->field($model, 'status')->dropDownList(UserModel::getStatusText()) ?>
                </div>
            </div>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('rbac-admin', 'Create') : Yii::t('rbac-admin', 'Update'),
                    ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<?php

use yii\helpers\Html;
use yii\jui\AutoComplete;
use yii\widgets\ActiveForm;
use backend\modules\admin\components\RouteRule;
use backend\modules\admin\components\Configs;

/* @var $this yii\web\View */
/* @var $model backend\modules\admin\models\AuthItem */
/* @var $form yii\widgets\ActiveForm */
/* @var $context backend\modules\admin\components\ItemController */

$context = $this->context;
$labels = $context->labels();
$rules = Configs::authManager()->getRules();
unset($rules[RouteRule::RULE_NAME]);
?>

<div class="auth-item-form box-body">
    <?php $form = ActiveForm::begin(['id' => 'item-form']); ?>
    <div class="row">
        <div class="col-lg-4 col-sm-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => 64]) ?>

            <?= $form->field($model, 'description')->textarea(['rows' => 4]) ?>
        </div>
        <div class="col-lg-4 col-sm-6">
            <?= $form->field($model, 'ruleName')->widget(AutoComplete::classname(), [
                'options' => [
                    'class' => 'form-control',
                ],
                'clientOptions' => [
                    'source' => array_keys($rules),
                ],
            ]) ?>

            <?= $form->field($model, 'data')->textarea(['rows' => 4]) ?>
        </div>
    </div>
    <div class="form-group">
        <?php
        echo Html::submitButton($model->isNewRecord ? Yii::t('rbac-admin', 'Create') : Yii::t('rbac-admin', 'Update'), [
            'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary',
            'name' => 'submit-button'])
        ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>

<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\grid\GridView;
use yii\widgets\DetailView;
use backend\models\product\Filters;
use backend\modules\admin\components\Helper;

/* @var $this yii\web\View */
/* @var $model backend\models\product\Groups */
/* @var $searchModel backend\models\product\search\FiltersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('views', 'Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="groups-view box box-info">
    <div class="box-body">

        <p class="btn-group">
            <?php
            if (Helper::checkRoute('update'))
                echo Html::a(Yii::t('views', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']);

            if (Helper::checkRoute('delete'))
                echo Html::a(Yii::t('views', 'Delete'), ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('views', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ]); ?>
        </p>

        <div class="row">
            <div class="col-lg-4 col-md-8">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        'name',
                        'created_at:datetime',
                        'updated_at:datetime',
                        [
                            'attribute' => 'status',
                            'format' => 'raw',
                            'value' => $model->getStatusName(),
                        ],
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>

<div class="menus-view box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode(Yii::t('views', 'Filters')) ?></h3>
    </div>
    <div class="box-body">
        <p>
            <?php if (Helper::checkRoute('create-filter'))
                echo Html::a(Yii::t('views', 'Create'), ['create-filter', 'group_id' => $model->id], ['class' => 'btn btn-success']); ?>
        </p>
        <div class="row">
            <div class="col-lg-8 col-md-10">
                <?php Pjax::begin(); ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        [
                            'attribute' => 'id',
                            'options' => ['width' => '70'],
                        ],
                        [
                            'attribute' => 'name',
                            'format' => 'raw',
                            'value' => function (Filters $data) {
                                return (Helper::checkRoute('view-filter')) ? Html::a($data->name, ['view-filter', 'id' => $data->id], ['data-pjax' => 0]) : $data->name;
                            },
                        ],
                        [
                            'attribute' => 'status',
                            'format' => 'raw',
                            'filter' => Filters::getStatusArray(),
                            'value' => function (Filters $data) {
                                return $data->getStatusName();
                            },
                        ],
                        [
                            'attribute' => 'Items',
                            'format' => 'raw',
                            'value' => function (Filters $data) {
                                return count($data->productFilterItems);
                            },
                        ],
                        [
                            'attribute' => 'created_at',
                            'format' => 'date',
                            'filter' => false,
                        ],
                        [
                            'attribute' => 'updated_at',
                            'format' => 'date',
                            'filter' => false,
                        ],

                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => Helper::filterActionColumn(['view-filter', 'update-filter', 'delete-filter']),
                            'buttons' => [
                                'view-filter' => function ($url) {
                                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                        'title' => Yii::t('yii', 'View'),
                                        'data-pjax' => 0,
                                    ]);
                                },
                                'update-filter' => function ($url) {
                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                        'title' => Yii::t('yii', 'Update'),
                                        'data-pjax' => 0,
                                    ]);
                                },
                                'delete-filter' => function ($url) {
                                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                        'title' => Yii::t('yii', 'Delete'),
                                        'data' => [
                                            'confirm' => 'Are you sure you want to delete this item?',
                                            'method' => 'post',
                                        ],
                                    ]);
                                }
                            ]
                        ],
                    ],
                ]); ?>
                <?php Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>
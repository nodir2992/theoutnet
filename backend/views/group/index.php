<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\grid\GridView;
use backend\widgets\PageSize;
use backend\widgets\ActionsApply;
use backend\models\product\Groups;
use backend\modules\admin\components\Helper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\product\search\GroupsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('views', 'Groups');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="groups-index box box-default">
    <div class="box-body">
        <p>
            <?php if (Helper::checkRoute('create'))
                echo Html::a(Yii::t('views', 'Create'), ['create'], ['class' => 'btn btn-success']); ?>
        </p>
        <div class="row">
            <div class="col-lg-9">
                <?php ActionsApply::begin(['template' => '<div class="form-inline margin-bottom-5">{list}{button}']);
                echo PageSize::widget([
                    'template' => Helper::checkRoute('apply-items') ? '{list}&nbsp;{label}</div>' :
                        '<div class="form-inline margin-bottom-5">{list}&nbsp;{label}</div>'
                ]); ?>
                <?php Pjax::begin(); ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'filterSelector' => 'select[name="per_page"]',
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        ['class' => 'yii\grid\CheckboxColumn'],

                        [
                            'attribute' => 'id',
                            'options' => ['width' => '70'],
                        ],
                        [
                            'attribute' => 'name',
                            'format' => 'raw',
                            'value' => function (Groups $data) {
                                return (Helper::checkRoute('view')) ? Html::a($data->name, ['view', 'id' => $data->id], ['data-pjax' => 0]) : $data->name;
                            },
                        ],
                        [
                            'attribute' => 'status',
                            'format' => 'raw',
                            'filter' => Groups::getStatusArray(),
                            'value' => function (Groups $data) {
                                return $data->getStatusName();
                            },
                        ],
                        [
                            'attribute' => 'created_at',
                            'format' => 'date',
                            'filter' => false,
                        ],
                        [
                            'attribute' => 'updated_at',
                            'format' => 'date',
                            'filter' => false,
                        ],

                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => Helper::filterActionColumn(['view', 'update', 'delete'])
                        ],
                    ],
                ]); ?>
                <?php Pjax::end(); ?>
                <?php ActionsApply::end(); ?>
            </div>
        </div>
    </div>
</div>

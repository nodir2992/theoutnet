<?php

/* @var $this yii\web\View */
/* @var $model backend\models\product\Groups */

$this->title = Yii::t('views', 'Update Group: ') . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('views', 'Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('views', 'Update');
?>
<div class="groups-update box box-primary">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

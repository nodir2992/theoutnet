<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;

/* @var $this yii\web\View */
/* @var $model backend\models\product\Filters */
/* @var $form yii\widgets\ActiveForm */
/* @var $modelItems \backend\models\product\FilterItems */
/* @var $modelItem \backend\models\product\FilterItems */

\backend\modules\translatemanager\helpers\Language::registerAssets();
$this->registerJs($this->render('_form.js'));
?>

<div class="filters-form box-body">

    <?php $form = ActiveForm::begin(['options' => ['id' => 'filter_form']]); ?>

    <div class="row">
        <div class="col-lg-4 col-md-8">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?php DynamicFormWidget::begin([
                'widgetContainer' => 'dynamicform_wrapper',
                'widgetBody' => '.container-filters',
                'widgetItem' => '.filter-item',
                'limit' => 10,
                'insertButton' => '.add-item',
                'deleteButton' => '.remove-item',
                'model' => $modelItems[0],
                'formId' => 'filter_form',
                'formFields' => [
                    'name'
                ],
            ]); ?>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-cubes"></i> <strong><?= Yii::t('views', 'Filter Items') ?></strong>
                    <button type="button" class="pull-right add-item btn btn-success btn-xs">
                        <i class="fa fa-plus"></i></button>
                </div>
                <div class="panel-body container-filters">
                    <?php foreach ($modelItems as $index => $modelItem): ?>
                        <div class="filter-item panel panel-default">
                            <div class="panel-heading">
                                <strong class="panel-title-filter">Item: <?= ($index + 1) ?></strong>
                                <button type="button" class="pull-right remove-item btn btn-danger btn-xs">
                                    <i class="fa fa-minus"></i></button>
                            </div>
                            <div class="panel-body">
                                <?php if (!$modelItem->isNewRecord)
                                    echo Html::activeHiddenInput($modelItem, "[{$index}]id"); ?>
                                <?= $form->field($modelItem, "[{$index}]name")->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>

            <?php DynamicFormWidget::end(); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-2 col-sm-4  col-xs-6">
            <?= $form->field($model, 'status')->dropDownList($model->getStatusArray()) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('views', 'Create') : Yii::t('views', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use backend\models\product\Filters;
use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\modules\admin\components\Helper;

/* @var $this yii\web\View */
/* @var $model backend\models\product\Filters */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('views', 'Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->group->name, 'url' => ['view', 'id' => $model->group_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="filters-view box box-info">
    <div class="box-body">

        <p class="btn-group">
            <?php
            if (Helper::checkRoute('update-filter'))
                echo Html::a(Yii::t('views', 'Update'), ['update-filter', 'id' => $model->id], ['class' => 'btn btn-primary']);

            if (Helper::checkRoute('delete-filter'))
                echo Html::a(Yii::t('views', 'Delete'), ['delete-filter', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('views', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ]); ?>
        </p>

        <div class="row">
            <div class="col-lg-4 col-md-8">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        'name',
                        [
                            'attribute' => 'Items',
                            'format' => 'raw',
                            'value' => function (Filters $data) {
                                $return = '';
                                if ($models = $data->productFilterItems) {
                                    foreach ($models as $item) {
                                        $return = $return . '<li class="list-group-item">' . $item->name . '</li>';
                                    }
                                }
                                return '<ul class="list-group">' . $return . '</ul>';
                            },
                        ],
                        'created_at:datetime',
                        'updated_at:datetime',
                        [
                            'attribute' => 'status',
                            'format' => 'raw',
                            'value' => $model->getStatusName(),
                        ],
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>

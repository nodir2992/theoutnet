<?php

/* @var $this yii\web\View */
/* @var $model backend\models\product\Filters */
/* @var $modelItems \backend\models\product\FilterItems */

$this->title = Yii::t('views', 'Update Filter: ') . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('views', 'Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->group->name, 'url' => ['view', 'id' => $model->group_id]];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view-filter', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('views', 'Update');
?>
<div class="filters-update box box-primary">

    <?= $this->render('_form', [
        'model' => $model,
        'modelItems' => $modelItems,
    ]) ?>

</div>

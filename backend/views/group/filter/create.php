<?php

/* @var $this yii\web\View */
/* @var $model backend\models\product\Filters */
/* @var $modelItems \backend\models\product\FilterItems */

$this->title = Yii::t('views', 'Create Filter');
$this->params['breadcrumbs'][] = ['label' => Yii::t('views', 'Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->group->name, 'url' => ['view', 'id' => $model->group_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="filters-create box box-success">

    <?= $this->render('_form', [
        'model' => $model,
        'modelItems' => $modelItems,
    ]) ?>

</div>

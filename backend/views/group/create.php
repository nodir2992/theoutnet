<?php

/* @var $this yii\web\View */
/* @var $model backend\models\product\Groups */

$this->title = Yii::t('views', 'Create Group');
$this->params['breadcrumbs'][] = ['label' => Yii::t('views', 'Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="groups-create box box-success">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

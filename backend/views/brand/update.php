<?php

/* @var $this yii\web\View */
/* @var $model backend\models\product\Brands */

$this->title = Yii::t('views', 'Update Brand: ') . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('views', 'Brands'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('views', 'Update');
?>
<div class="brands-update box box-primary">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

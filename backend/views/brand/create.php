<?php

/* @var $this yii\web\View */
/* @var $model backend\models\product\Brands */

$this->title = Yii::t('views', 'Create Brand');
$this->params['breadcrumbs'][] = ['label' => Yii::t('views', 'Brands'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="brands-create box box-success">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\grid\GridView;
use kartik\sortable\Sortable;
use backend\widgets\PageSize;
use backend\widgets\ActionsApply;
use backend\models\product\Brands;
use backend\modules\admin\components\Helper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\product\search\BrandsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('views', 'Brands');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="brands-index box box-default">
    <div class="box-body">

        <p>
            <?php if (Helper::checkRoute('create'))
                echo Html::a(Yii::t('views', 'Create'), ['create'], ['class' => 'btn btn-success']); ?>
        </p>
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#tab_1" data-toggle="tab">
                        <i class="fa fa-th"></i>&nbsp;<b> <?= Yii::t('views', 'Grid') ?></b></a>
                </li>
                <li>
                    <a href="#tab_2" data-toggle="tab">
                        <i class="fa fa-sort"></i>&nbsp;<b> <?= Yii::t('views', 'Sort') ?></b></a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane grid active" id="tab_1">
                    <div class="row">
                        <div class="col-lg-9"><?php ActionsApply::begin(['template' => '<div class="form-inline margin-bottom-5">{list}{button}']);
                            echo PageSize::widget([
                                'template' => Helper::checkRoute('apply-items') ? '{list}&nbsp;{label}</div>' :
                                    '<div class="form-inline margin-bottom-5">{list}&nbsp;{label}</div>'
                            ]); ?>
                            <?php Pjax::begin(['id' => 'data_grid']); ?>
                            <?= GridView::widget([
                                'dataProvider' => $dataProvider,
                                'filterModel' => $searchModel,
                                'filterSelector' => 'select[name="per_page"]',
                                'columns' => [
                                    ['class' => 'yii\grid\SerialColumn'],
                                    ['class' => 'yii\grid\CheckboxColumn'],

                                    [
                                        'attribute' => 'id',
                                        'options' => ['width' => '70'],
                                    ],
                                    [
                                        'attribute' => 'name',
                                        'format' => 'raw',
                                        'value' => function (Brands $data) {
                                            return (Helper::checkRoute('view')) ? Html::a($data->name, ['view', 'id' => $data->id], ['data-pjax' => 0]) : $data->name;
                                        },
                                    ],
                                    [
                                        'attribute' => 'weight',
                                        'options' => ['width' => '100'],
                                    ],
                                    [
                                        'attribute' => 'status',
                                        'format' => 'raw',
                                        'filter' => Brands::getStatusArray(),
                                        'value' => function (Brands $data) {
                                            return $data->getStatusName();
                                        },
                                    ],
                                    [
                                        'attribute' => 'image',
                                        'format' => 'raw',
                                        'filter' => false,
                                        'value' => function (Brands $data) {
                                            return Html::img($data->image, ['alt' => 'brand', 'width' => 100]);
                                        },
                                    ],
                                    [
                                        'attribute' => 'created_at',
                                        'format' => 'date',
                                        'filter' => false,
                                    ],
                                    [
                                        'attribute' => 'updated_at',
                                        'format' => 'date',
                                        'filter' => false,
                                    ],

                                    [
                                        'class' => 'yii\grid\ActionColumn',
                                        'template' => Helper::filterActionColumn(['view', 'update', 'delete'])
                                    ],
                                ],
                            ]); ?>
                            <?php Pjax::end(); ?>
                            <?php ActionsApply::end(); ?>
                        </div>
                    </div>
                </div>

                <div class="tab-pane tree" id="tab_2">
                    <div class="sortable-tab col-lg-3 col-md-4 col-sm-6">
                        <?php
                        if (!empty($arrayBrands)) {
                            echo Sortable::widget([
                                'items' => $arrayBrands,
                                'pluginEvents' => [
                                    'sortupdate' => 'function(e) {
                                        var overlay = $("#overlay");
                                        overlay.show();
                                        var arr = e.target.children;
                                        var data = [];
                                        for (var i = 0; i < arr.length; i++) {
                                            data.push(arr[i].children[0].alt);
                                        }
                                        $.ajax({
                                            url: "sortable",
                                            type: "POST",
                                            data: {data},
                                            success: function (data) {
                                            $.pjax.reload({
                                                    container: "#data_grid"
                                                }).done(function () {
                                                    overlay.hide();
                                                });
                                            }                                        
                                        });
                                    }',
                                ]
                            ]);
                        }
                        ?>
                    </div>
                    <div class="clearfix"></div>

                </div>
            </div>
        </div>

    </div>
</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;

/* @var $this yii\web\View */
/* @var $model backend\models\location\Locations */
/* @var $modelItems backend\models\location\Locations */
/* @var $modelItem backend\models\location\Locations */
/* @var $form yii\widgets\ActiveForm */

\backend\modules\translatemanager\helpers\Language::registerAssets();
$this->registerJs($this->render('_form.js'));
?>

<div class="locations-form box-body">

    <?php $form = ActiveForm::begin(['options' => ['id' => 'location_form']]); ?>

    <div class="row">
        <div class="col-lg-4 col-md-8">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?php DynamicFormWidget::begin([
                'widgetContainer' => 'dynamicform_wrapper',
                'widgetBody' => '.container-districts',
                'widgetItem' => '.district-item',
//                'limit' => 10,
                'insertButton' => '.add-item',
                'deleteButton' => '.remove-item',
                'model' => $modelItems[0],
                'formId' => 'location_form',
                'formFields' => [
                    'name',
                    'status'
                ],
            ]); ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-cubes"></i> <strong><?= Yii::t('views', 'Districts') ?></strong>
                    <button type="button" class="pull-right add-item btn btn-success btn-xs">
                        <i class="fa fa-plus"></i></button>
                </div>
                <div class="panel-body container-districts">
                    <?php foreach ($modelItems as $index => $modelItem): ?>
                        <div class="district-item panel panel-default">
                            <div class="panel-heading">
                                <strong class="panel-title-district">Item: <?= ($index + 1) ?></strong>
                                <button type="button" class="pull-right remove-item btn btn-danger btn-xs">
                                    <i class="fa fa-minus"></i></button>
                            </div>
                            <div class="panel-body">
                                <?php if (!$modelItem->isNewRecord)
                                    echo Html::activeHiddenInput($modelItem, "[{$index}]id"); ?>
                                <?= $form->field($modelItem, "[{$index}]name")->textInput(['maxlength' => true]) ?>
                                <?= $form->field($modelItem, "[{$index}]status")->dropDownList($model->getStatusArray()) ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <?php DynamicFormWidget::end(); ?>

        </div>
    </div>

    <div class="row">
        <div class="col-md-2 col-sm-4  col-xs-6">
            <?= $form->field($model, 'type')->dropDownList($model->getTypeArray()) ?>

            <?= $form->field($model, 'status')->dropDownList($model->getStatusArray()) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('views', 'Create') : Yii::t('views', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

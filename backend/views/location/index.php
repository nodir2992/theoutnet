<?php

use backend\models\location\Locations;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\grid\GridView;
use backend\modules\admin\components\Helper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\location\LocationsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('views', 'Locations');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="locations-index box box-default">
    <div class="box-body">

        <p>
            <?php if (Helper::checkRoute('create'))
                echo Html::a(Yii::t('views', 'Create'), ['create'], ['class' => 'btn btn-success']); ?>
        </p>
        <div class="row">
            <div class="col-md-8">
                <?php Pjax::begin(); ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        [
                            'attribute' => 'id',
                            'options' => ['width' => '70'],
                        ],
                        [
                            'attribute' => 'name',
                            'format' => 'raw',
                            'value' => function (Locations $data) {
                                return (Helper::checkRoute('view')) ? Html::a($data->name, ['view', 'id' => $data->id], ['data-pjax' => 0]) : $data->name;
                            },
                        ],
                        [
                            'attribute' => 'type',
                            'format' => 'raw',
                            'filter' => Locations::getTypeArray(),
                            'value' => function (Locations $data) {
                                return $data->type;
                            },
                        ],
                        [
                            'attribute' => 'status',
                            'format' => 'raw',
                            'filter' => Locations::getStatusArray(),
                            'value' => function (Locations $data) {
                                return $data->getStatusName();
                            },
                        ],
                        [
                            'attribute' => 'created_at',
                            'format' => 'date',
                            'filter' => false,
                        ],
                        [
                            'attribute' => 'updated_at',
                            'format' => 'date',
                            'filter' => false,
                        ],

                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => Helper::filterActionColumn(['view', 'update', 'delete'])
                        ],
                    ],
                ]); ?>
                <?php Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>

<?php

/* @var $this yii\web\View */
/* @var $model backend\models\location\Locations */
/* @var $modelItems backend\models\location\Locations */

$this->title = Yii::t('views', 'Create Location');
$this->params['breadcrumbs'][] = ['label' => Yii::t('views', 'Locations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="locations-create box box-success">

    <?= $this->render('_form', [
        'model' => $model,
        'modelItems' => $modelItems,
    ]) ?>

</div>

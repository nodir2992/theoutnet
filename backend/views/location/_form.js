/**
 * Created by nodir on 6/28/17.
 */
jQuery(document).ready(function () {

    jQuery(".dynamicform_wrapper").on("afterInsert", function (e, item) {
        jQuery(".dynamicform_wrapper .panel-title-district").each(function (index) {
            jQuery(this).html(lajax.t("Item: ") + (index + 1));
        });
    });

    jQuery(".dynamicform_wrapper").on("afterDelete", function (e) {
        jQuery(".dynamicform_wrapper .panel-title-district").each(function (index) {
            jQuery(this).html(lajax.t("Item: ") + (index + 1));
        });
    });

});

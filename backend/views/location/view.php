<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\models\location\Locations;
use backend\modules\admin\components\Helper;

/* @var $this yii\web\View */
/* @var $model backend\models\location\Locations */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('views', 'Locations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="locations-view box box-info">
    <div class="box-body">

        <p class="btn-group">
            <?php
            if (Helper::checkRoute('update'))
                echo Html::a(Yii::t('views', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']);

            if (Helper::checkRoute('delete'))
                echo Html::a(Yii::t('views', 'Delete'), ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('views', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ]); ?>
        </p>

        <div class="row">
            <div class="col-lg-4 col-md-8">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        'type',
                        'name',
                        'created_at:datetime',
                        'updated_at:datetime',
                        [
                            'attribute' => 'status',
                            'format' => 'raw',
                            'value' => $model->getStatusName(),
                        ],
                        [
                            'attribute' => 'districts',
                            'format' => 'raw',
                            'value' => function (Locations $model) {
                                $result = '';
                                if (!empty($districts = $model->districts)) {
                                    foreach ($districts as $district) {
                                        $result = $result . Html::tag('li',
                                                $district->name . ' <span class="pull-right">' . $district->getStatusName() . '</span>',
                                                ['class' => 'list-group-item']);
                                    }
                                }
                                return '<ul class="list-group">' . $result . '</ul>';
                            },
                        ],
                    ],
                ]) ?>
            </div>
        </div>

    </div>
</div>

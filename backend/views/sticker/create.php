<?php

/* @var $this yii\web\View */
/* @var $model backend\models\product\Stickers */

$this->title = Yii::t('views', 'Create Sticker');
$this->params['breadcrumbs'][] = ['label' => Yii::t('views', 'Stickers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="stickers-create box box-success">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

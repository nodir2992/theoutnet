<?php

/* @var $this yii\web\View */
/* @var $model backend\models\product\Stickers */

$this->title = Yii::t('views', 'Update Sticker: ') . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('views', 'Stickers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('views', 'Update');
?>

<div class="stickers-update box box-primary">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

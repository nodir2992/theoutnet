<?php

use kartik\datetime\DateTimePicker;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\file\FileInput;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use mihaildev\elfinder\ElFinder;
use mihaildev\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model backend\models\product\Products */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="products-form box-body">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-lg-6 col-md-10">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-2 col-md-4 col-md-6 col-sm-6">
            <?= $form->field($model, 'barcode')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-lg-2 col-md-4 col-sm-6">
            <?= $form->field($model, 'price')->textInput() ?>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6">
            <?= $form->field($model, 'old_price')->textInput() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-3 col-md-6">
            <?= $form->field($model, 'brand_id')->widget(Select2::className(), [
                'data' => $model->getBrandsList(),
                'theme' => Select2::THEME_DEFAULT,
                'options' => ['placeholder' => ''],
            ]) ?>
        </div>
        <div class="col-lg-3 col-md-6">
            <?= $form->field($model, 'discount_id')->dropDownList(ArrayHelper::merge([null => ''], $model->getDiscountsList())) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 col-md-10">
            <?= $form->field($model, 'category_ids')->widget(Select2::className(), [
                'data' => $model->getCategoriesList(),
                'theme' => Select2::THEME_DEFAULT,
                'options' => [
                    'placeholder' => '',
                    'multiple' => true
                ],
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-4 col-md-6">
            <?= $form->field($model, 'ribbon_ids')->widget(Select2::className(), [
                'data' => $model->getRibbonsList(),
                'theme' => Select2::THEME_DEFAULT,
                'options' => [
                    'placeholder' => '',
                    'multiple' => true
                ],
            ]) ?>
        </div>
        <div class="col-lg-4 col-md-6">
            <?= $form->field($model, 'sticker_ids')->widget(Select2::className(), [
                'data' => $model->getStickersList(),
                'theme' => Select2::THEME_DEFAULT,
                'options' => [
                    'placeholder' => '',
                    'multiple' => true
                ],
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-3 col-md-4">
            <?= $form->field($model, 'group_id')->widget(Select2::className(), [
                'data' => $model->getGroupsList(),
                'theme' => Select2::THEME_DEFAULT,
                'options' => ['placeholder' => '', 'id' => 'main_group',],
            ]) ?>
        </div>
        <div class="col-lg-5 col-md-8">
            <?= $form->field($model, 'filter_ids')->widget(DepDrop::className(), [
                'data' => $model->filter_ids ? $model->dataFilters : [],
                'type' => DepDrop::TYPE_SELECT2,
                'options' => ['multiple' => true],
                'select2Options' => ['hideSearch' => true],
                'pluginOptions' => [
                    'depends' => ['main_group'],
                    'url' => Url::to(['get-filter-list']),
                    'placeholder' => '',
                    'loadingText' => '...'
                ]
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-8">
            <div class="form-group kartik-file-input">
                <?php
                $initialPreview = [];
                $initialPreviewConfig = [];
                if (!empty($images = $model->productImages)) {
                    foreach ($images as $image) {
                        array_push($initialPreview, $image->path);
                        array_push($initialPreviewConfig, [
                            'caption' => $image->name,
                            'key' => $image->generate_name,
                        ]);
                    }
                } ?>
                <?= $form->field($model, 'uploaded_images')->hiddenInput(['id' => 'uploaded_images'])->label(false) ?>
                <?= $form->field($model, 'deleted_images')->hiddenInput(['id' => 'deleted_images'])->label(false) ?>
                <?= $form->field($model, 'sorted_images')->hiddenInput(['id' => 'sorted_images'])->label(false) ?>
                <?php $this->registerJs("
                    var uploadedImages = {}, deletedImages = [],
                    uploaded = document.getElementById('uploaded_images'),
                    deleted = document.getElementById('deleted_images'),
                    sorted = document.getElementById('sorted_images');") ?>
                <label class="control-label"><?= Yii::t('model', 'Images') ?></label>
                <?= FileInput::widget([
                    'name' => 'image',
                    'options' => [
                        'accept' => 'image/*',
                        'multiple' => true
                    ],
                    'pluginOptions' => [
                        'uploadUrl' => Url::to(['upload-image']),
                        'deleteUrl' => Url::to(['delete-image']),
                        'maxFileCount' => 10,
                        'showRemove' => false,
                        'showUpload' => false,
                        'showCaption' => false,
                        'initialPreview' => $initialPreview,
                        'initialPreviewAsData' => true,
                        'initialPreviewConfig' => $initialPreviewConfig,
                        'overwriteInitial' => false,
                        'browseIcon' => '<i class="glyphicon glyphicon-camera"></i>&nbsp;&nbsp;',
                        'browseLabel' => Yii::t('views', 'Browse …'),
                    ],
                    'pluginEvents' => [
                        'fileuploaded' => new JsExpression('function(event, data, previewId) {
                            uploadedImages[previewId] = data.response;
                            uploaded.value = JSON.stringify(uploadedImages);
                        }'),
                        'filedeleted' => new JsExpression('function(event, key) {
                            deletedImages.push(key);
                            deleted.value = JSON.stringify(deletedImages);
                        }'),
                        'filesuccessremove' => new JsExpression('function(event, previewId) {
                            delete uploadedImages[previewId];
                            uploaded.value = JSON.stringify(uploadedImages);
                        }'),
                        'filesorted' => new JsExpression('function(event, params) {
                            sorted.value = JSON.stringify(params.stack);
                        }')
                    ]
                ]) ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-8">
            <?= $form->field($model, 'description')->widget(CKEditor::className(), [
                'editorOptions' => ElFinder::ckeditorOptions(
                    ['elfinder', 'path' => 'product'],
                    [
                        'allowedContent' => true,
                        'height' => 400,
                        'toolbarGroups' => [
                            'mode', 'undo', 'selection',
                            ['name' => 'clipboard', 'groups' => ['clipboard', 'doctools', 'cleanup']],
                            ['name' => 'basicstyles', 'groups' => ['basicstyles', 'colors']],
                            ['name' => 'paragraph', 'groups' => ['align', 'templates', 'list', 'indent']],
                            'styles', 'insert', 'blocks', 'links', 'find', 'tools', 'about',
                        ]
                    ]
                ),
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-8">
            <?= $form->field($model, 'information')->widget(CKEditor::className(), [
                'editorOptions' => ElFinder::ckeditorOptions(
                    ['elfinder', 'path' => 'product'],
                    [
                        'allowedContent' => true,
                        'height' => 400,
                        'toolbarGroups' => [
                            'mode', 'undo', 'selection',
                            ['name' => 'clipboard', 'groups' => ['clipboard', 'doctools', 'cleanup']],
                            ['name' => 'basicstyles', 'groups' => ['basicstyles', 'colors']],
                            ['name' => 'paragraph', 'groups' => ['align', 'templates', 'list', 'indent']],
                            'styles', 'insert', 'blocks', 'links', 'find', 'tools', 'about',
                        ]
                    ]
                ),
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-8">
            <?= $form->field($model, 'detail')->widget(CKEditor::className(), [
                'editorOptions' => ElFinder::ckeditorOptions(
                    ['elfinder', 'path' => 'product'],
                    [
                        'allowedContent' => true,
                        'height' => 400,
                        'toolbarGroups' => [
                            'mode', 'undo', 'selection',
                            ['name' => 'clipboard', 'groups' => ['clipboard', 'doctools', 'cleanup']],
                            ['name' => 'basicstyles', 'groups' => ['basicstyles', 'colors']],
                            ['name' => 'paragraph', 'groups' => ['align', 'templates', 'list', 'indent']],
                            'styles', 'insert', 'blocks', 'links', 'find', 'tools', 'about',
                        ]
                    ]
                ),
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'meta_title')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'meta_keywords')->textarea(['rows' => 3]) ?>
            <?= $form->field($model, 'meta_description')->textarea(['rows' => 6]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-2 col-sm-4">
            <?= $form->field($model, 'quantity')->textInput() ?>
        </div>
        <div class="col-md-2 col-sm-4">
            <?= $form->field($model, 'str_timer_start')->widget(
                DateTimePicker::className(), [
                    'type' => DateTimePicker::TYPE_COMPONENT_APPEND,
                    'options' => [
                        'value' => !$model->isNewRecord && $model->timer_start ? date("d-m-Y H:i", $model->timer_start) : false
                    ],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'dd-mm-yyyy hh:ii'
                    ]
                ]
            ) ?>
        </div>
        <div class="col-md-2 col-sm-4">
            <?= $form->field($model, 'str_timer_end')->widget(
                DateTimePicker::className(), [
                    'type' => DateTimePicker::TYPE_COMPONENT_APPEND,
                    'options' => [
                        'value' => !$model->isNewRecord && $model->timer_end ? date("d-m-Y H:i", $model->timer_end) : false
                    ],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'dd-mm-yyyy hh:ii'
                    ]
                ]
            ) ?>
        </div>
        <?= $form->field($model, 'str_timer')->hiddenInput(['value' => true])->label(false) ?>
    </div>

    <div class="row">
        <div class="col-md-2 col-sm-4 col-xs-6">
            <?= $form->field($model, 'status')->dropDownList($model->getStatusArray()) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('views', 'Create') : Yii::t('views', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

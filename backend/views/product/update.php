<?php

/* @var $this yii\web\View */
/* @var $model backend\models\product\Products */

$this->title = Yii::t('views', 'Update Product: ') . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('views', 'Products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('views', 'Update');
?>
<div class="products-update box box-primary">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

/* @var $this yii\web\View */
/* @var $model backend\models\product\Products */

$this->title = Yii::t('views', 'Create Product');
$this->params['breadcrumbs'][] = ['label' => Yii::t('views', 'Products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-create box box-success">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;
use backend\models\product\DollarRate;
use backend\modules\admin\components\Helper;

/* @var $this yii\web\View */
/* @var $dataProvider DollarRate */
/* @var $model DollarRate */

$this->title = Yii::t('views', 'Dollar Rate');
$this->params['breadcrumbs'][] = ['label' => Yii::t('views', 'Products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="box box-default">
    <div class="box-body">
        <?php Pjax::begin(); ?>
        <p>
            <?php
            Modal::begin([
                'header' => '<h4>' . Yii::t('views', 'Create Dollar Rate') . '</h4>',
                'toggleButton' => [
                    'label' => Yii::t('views', 'Create'),
                    'class' => 'btn btn-success'
                ],
            ]);

            $form = ActiveForm::begin();

            echo $form->field($model, 'value')->textInput()
                . Html::submitButton(Yii::t('views', 'Create'), ['class' => 'btn btn-success']);

            ActiveForm::end();
            Modal::end();
            ?>
        </p>
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        [
                            'class' => 'yii\grid\SerialColumn',
                            'options' => ['width' => '1'],
                        ],

                        'value',
                        'created_at:date',

                        [
                            'class' => 'yii\grid\ActionColumn',
                            'options' => ['width' => '1'],
                            'template' => Helper::filterActionColumn(['dollar-rate-delete']),
                            'buttons' => [
                                'dollar-rate-delete' => function ($url) {
                                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                        'title' => Yii::t('yii', 'Delete'),
                                        'data' => [
                                            'confirm' => 'Are you sure you want to delete this item?',
                                            'method' => 'post',
                                        ],
                                    ]);
                                }
                            ]
                        ],
                    ],
                ]) ?>
            </div>
        </div>
        <?php Pjax::end(); ?>
    </div>
</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use dosamigos\gallery\Gallery;
use backend\models\product\Products;
use backend\modules\admin\components\Helper;

/* @var $this yii\web\View */
/* @var $model backend\models\product\Products */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('views', 'Products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-view box box-info">
    <div class="box-body">

        <p class="btn-group">
            <?php
            if (Helper::checkRoute('update'))
                echo Html::a(Yii::t('views', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']);

            if (Helper::checkRoute('delete'))
                echo Html::a(Yii::t('views', 'Delete'), ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('views', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ]); ?>
        </p>

        <div class="row">
            <div class="col-lg-8 col-md-10">
                <?= DetailView::widget([
                    'model' => $model,
                    'template' => "<tr><th width='30%'>{label}</th><td>{value}</td></tr>",
                    'attributes' => [
                        'id',
                        'name',
                        'slug',
                        'barcode',
                        [
                            'attribute' => 'price',
                            'format' => 'raw',
                            'value' => $model->price . ' $ | ' . $model->getPriceAsCurrency(),
                        ],
                        [
                            'attribute' => 'old_price',
                            'format' => 'raw',
                            'value' => $model->old_price ?
                                $model->old_price . ' $ | ' . $model->getOldPriceAsCurrency() :
                                $model->old_price,
                        ],
                        [
                            'attribute' => 'discount_id',
                            'format' => 'raw',
                            'value' => $model->discount_id ?
                                Html::a($model->discount->name, ['discount/view', 'id' => $model->discount_id], ['target' => '_blank']) :
                                $model->discount_id,
                        ],
                        [
                            'attribute' => 'brand_id',
                            'format' => 'raw',
                            'value' => Html::a($model->brand->name, ['brand/view', 'id' => $model->brand_id], ['target' => '_blank']),
                        ],
                        [
                            'attribute' => 'group_id',
                            'format' => 'raw',
                            'value' => Html::a($model->group->name, ['group/view', 'id' => $model->group_id], ['target' => '_blank']),
                        ],
                        [
                            'attribute' => 'filter_ids',
                            'format' => 'raw',
                            'value' => function (Products $model) {
                                $result = '';
                                if (!empty($filters = $model->filterItems)) {
                                    foreach ($filters as $filter) {
                                        $result .= Html::a($filter->name, ['group/view-filter', 'id' => $filter->filter_id],
                                            ['class' => 'list-group-item', 'target' => '_blank']);
                                    }
                                }

                                return '<div class="list-group col-lg-8 col-md-10">' . $result . '</div>';
                            },
                        ],
                        [
                            'attribute' => 'category_ids',
                            'format' => 'raw',
                            'value' => function (Products $model) {
                                $result = '';
                                if (!empty($categories = $model->categories)) {
                                    foreach ($categories as $category) {
                                        $result .= Html::a($category->name, ['category/view', 'id' => $category->id],
                                            ['class' => 'list-group-item', 'target' => '_blank']);
                                    }
                                }

                                return '<div class="list-group col-lg-8 col-md-10">' . $result . '</div>';
                            },
                        ],
                        [
                            'attribute' => 'ribbon_ids',
                            'format' => 'raw',
                            'value' => function (Products $model) {
                                $result = '';
                                if (!empty($ribbons = $model->ribbons)) {
                                    foreach ($ribbons as $ribbon) {
                                        $result .= Html::a($ribbon->name, ['ribbon/view', 'id' => $ribbon->id],
                                            ['class' => 'list-group-item', 'target' => '_blank']);
                                    }
                                }

                                return '<div class="list-group col-lg-8 col-md-10">' . $result . '</div>';
                            },
                        ],
                        [
                            'attribute' => 'sticker_ids',
                            'format' => 'raw',
                            'value' => function (Products $model) {
                                $result = '';
                                if (!empty($stickers = $model->stickers)) {
                                    foreach ($stickers as $sticker) {
                                        $result .= Html::a($sticker->name, ['sticker/view', 'id' => $sticker->id],
                                            ['class' => 'list-group-item', 'target' => '_blank']);
                                    }
                                }

                                return '<div class="list-group col-lg-8 col-md-10">' . $result . '</div>';
                            },
                        ],
                        [
                            'attribute' => 'uploaded_images',
                            'format' => 'raw',
                            'value' => function (Products $model) {
                                $items = [];
                                if (!empty($images = $model->productImages)) {
                                    foreach ($images as $image) {
                                        array_push($items, [
                                            'src' => $image->path,
                                            'options' => ['title' => $image->name, 'class' => 'amigos-gallery']
                                        ]);
                                    }
                                }
                                return Gallery::widget(['items' => $items]);
                            },
                        ],
                        [
                            'attribute' => 'description',
                            'format' => 'raw',
                        ],
                        [
                            'attribute' => 'information',
                            'format' => 'raw',
                        ],
                        [
                            'attribute' => 'detail',
                            'format' => 'raw',
                        ],
                        'meta_title',
                        'meta_keywords:ntext',
                        'meta_description:ntext',
                        'quantity',
                        'timer_start:datetime',
                        'timer_end:datetime',
                        'views',
                        'votes',
                        'created_at:datetime',
                        'updated_at:datetime',
                        [
                            'attribute' => 'status',
                            'format' => 'raw',
                            'value' => $model->getStatusName(),
                        ],
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>

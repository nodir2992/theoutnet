<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;
use kartik\select2\Select2;
use backend\models\order\OrderItems;
use backend\modules\admin\components\Helper;

/* @var $this yii\web\View */
/* @var $model backend\models\order\Orders */

$this->title = Yii::t('views', 'Order № ') . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('views', 'Orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-view box box-info">
    <div class="box-body">

        <p class="btn-group">
            <?php
            if (Helper::checkRoute('export-pdf'))
                echo Html::a(Yii::t('views', 'Export to PDF'),
                    ['export-pdf', 'id' => $model->id], ['class' => 'btn btn-warning', 'target' => '_blank']);

            if (Helper::checkRoute('update'))
                echo Html::a(Yii::t('views', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']);

            if (Helper::checkRoute('delete'))
                echo Html::a(Yii::t('views', 'Delete'), ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('views', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ]); ?>
        </p>

        <div class="row">
            <div class="col-md-6">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        [
                            'attribute' => 'user_id',
                            'value' => $model->user_id !== 0 ? $model->customer->username : 'Гость'
                        ],
                        'first_name',
                        'last_name',
                        'email:email',
                        'phone',
                        [
                            'attribute' => 'location_id',
                            'value' => $model->location->name
                        ],
                        'address',
                        'comment:ntext',
                        [
                            'attribute' => 'payment',
                            'format' => 'raw',
                            'value' => $model->getPaymentName(),
                        ],
                        [
                            'attribute' => 'status',
                            'format' => 'raw',
                            'value' => $model->status !== $model::STATUS_DELETED ? '<div style="width: 200px;">'
                                . Select2::widget([
                                    'name' => 'status',
                                    'hideSearch' => true,
                                    'id' => $model->id,
                                    'value' => $model->status,
                                    'data' => $model->getStatusArray(),
                                    'theme' => Select2::THEME_DEFAULT,
                                    'pluginEvents' => [
                                        'change' => 'function(e) {
                                            $("#overlay").show();
                                            $.ajax({
                                                url: "set-status",
                                                type: "post",
                                                data: {
                                                    id: parseInt(e.currentTarget.id),
                                                    status: parseInt(e.currentTarget.value)
                                                },
                                                success: function (data) {
                                                    $("#overlay").hide();
                                                }
                                            });
                                        }',
                                    ],
                                ])
                                . '</div>' : '<span class="text-bold text-red">Deleted</span>',
                        ],
                        'created_at:datetime',
                        'updated_at:datetime',
                    ],
                ]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8">
                <?php
                if (!empty($model->orderItems)) {
                    echo GridView::widget([
                        'layout' => '{items}',
                        'showFooter' => true,
                        'footerRowOptions' => ['class' => 'text-bold'],
                        'dataProvider' => $model->getOrderItemsDataProvider(),
                        'columns' => [
                            [
                                'attribute' => 'product_id',
                                'format' => 'raw',
                                'value' => function (OrderItems $data) {
                                    return Html::a(Html::img($data->product->getDefaultImageUrl(), ['alt' => 'product_image', 'width' => '120px'])
                                        . '<br>' . $data->product->name,
                                        ['product/view', 'id' => $data->product_id],
                                        ['target' => '_blank', 'class' => 'text-bold']);
                                },
                                'footer' => 'Общий'
                            ],
                            [
                                'attribute' => 'price',
                                'format' => 'raw',
                                'value' => function (OrderItems $data) {
                                    return Yii::$app->formatter->asDecimal($data->price, 0)
                                        . Yii::t('model', '&nbsp;<i>сум</i>');
                                }
                            ],
                            [
                                'attribute' => 'quantity',
                                'footer' => array_sum(array_map(function ($item) {
                                    return $item['quantity'];
                                }, $model->orderItems))

                            ],
                            [
                                'attribute' => 'total_price',
                                'format' => 'raw',
                                'value' => function (OrderItems $data) {
                                    return Yii::$app->formatter->asDecimal($data->total_price, 0)
                                        . Yii::t('model', '&nbsp;<i>сум</i>');
                                },
                                'footer' => Yii::$app->formatter->asDecimal(array_sum(array_map(function ($item) {
                                        return $item['total_price'];
                                    }, $model->orderItems)), 0)
                                    . Yii::t('model', '&nbsp;<i>сум</i>')
                            ],
                        ]
                    ]);
                } ?>
            </div>
        </div>

    </div>
</div>
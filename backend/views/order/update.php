<?php

/* @var $this yii\web\View */
/* @var $model backend\models\order\Orders */

$this->title = Yii::t('views', 'Update Order № ') . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('views', 'Orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('views', 'Order № ') . $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('views', 'Update');
?>
<div class="orders-update box box-primary">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;
use backend\models\order\OrderItems;

/* @var $this yii\web\View */
/* @var $model backend\models\order\Orders */
/* @var $logo string */
/* @var $addressStore string */

$this->title = Yii::t('views', 'Order № ') . $model->id;

?>

<p align="center"><?= Html::img($logo, ['alt' => 'logo']) ?></p>
<h3 align="center">ПОДРОБНОСТИ ЗАКАЗА</h3>
<h6 align="right">Дата создания:
    <?= Yii::$app->formatter->asDate($model->created_at, 'dd.M.yyyy') ?> | Заказ №
    <?= $model->id ?></h6>

<?php
if (!empty($model->orderItems)) {
    echo GridView::widget([
        'layout' => '{items}',
        'showFooter' => true,
        'footerRowOptions' => ['class' => 'text-bold'],
        'dataProvider' => $model->getOrderItemsDataProvider(),
        'columns' => [
            [
                'attribute' => 'product_id',
                'format' => 'raw',
                'value' => function (OrderItems $data) {
                    return $data->product->name;
                },
                'footer' => '<b>Общий</p>'
            ],
            [
                'attribute' => 'price',
                'format' => 'raw',
                'value' => function (OrderItems $data) {
                    return Yii::$app->formatter->asDecimal($data->price, 0)
                        . Yii::t('model', '&nbsp;<i>сум</i>');
                }
            ],
            [
                'attribute' => 'quantity',
                'footer' => array_sum(array_map(function ($item) {
                    return $item['quantity'];
                }, $model->orderItems))

            ],
            [
                'attribute' => 'total_price',
                'format' => 'raw',
                'value' => function (OrderItems $data) {
                    return Yii::$app->formatter->asDecimal($data->total_price, 0)
                        . Yii::t('model', '&nbsp;<i>сум</i>');
                },
                'footer' => Yii::$app->formatter->asDecimal(array_sum(array_map(function ($item) {
                        return $item['total_price'];
                    }, $model->orderItems)), 0)
                    . Yii::t('model', '&nbsp;<i>сум</i>')
            ],
        ]
    ]);
} ?>

<div class="address-customer">
    <p align="center"><b>Адрес заказчика</b></p>
    <?php
    echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'user_id',
                'value' => $model->user_id !== 0 ? $model->customer->username : 'Гость'
            ],
            'first_name',
            'last_name',
            'email',
            'phone',
            [
                'attribute' => 'location_id',
                'value' => $model->location->name
            ],
            'address',
            [
                'attribute' => 'payment',
                'format' => 'raw',
                'value' => $model->getPaymentName(),
            ],
        ],
    ]); ?>

</div>

<div class="address-store">
    <p align="center"><b>Адрес магазина</b></p>
    <?= $addressStore ?>

</div>
<div class="signature">
    <p>Подпись заказчика: ____________________</p>
    <p>Подпись продавца: ____________________</p>
</div>

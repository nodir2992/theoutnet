<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */
/* @var $exception ->statusCode integer */

use yii\helpers\Html;

$this->title = $name;

?>

<div class="error-page">
    <h2 class="headline text-red"> <?= $exception->statusCode ?></h2>

    <div class="error-content">
        <h3><i class="fa fa-warning text-red"></i> Oops! <?= Html::encode($this->title) ?></h3>

        <p><?= nl2br(Html::encode($message)) ?></p>
        <p><?= Html::a(Yii::t('yii', 'Return to Home'), Yii::$app->homeUrl) ?></p>

    </div>
</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $title string */
/* @var $newOrders integer */
/* @var $completedOrders integer */
/* @var $allOrders integer */
/* @var $brands integer */
/* @var $categories integer */
/* @var $products integer */
/* @var $menus integer */
/* @var $pages integer */
/* @var $blocks integer */

$this->title = $title;

?>

<div class="site-index box box-default">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Yii::t('views', 'Orders') ?></h3>
    </div>
    <div class="box-body">

        <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="small-box bg-red-active">
                <div class="inner">
                    <h3><?= $newOrders ?></h3>
                    <h4><?= Yii::t('views', 'New orders') ?></h4>
                </div>
                <div class="icon"><i class="fa fa-cart-plus"></i></div>
                <?= Html::a(Yii::t('views', 'More info') . '&nbsp;<i class="fa fa-arrow-circle-right"></i>',
                    ['order/index?OrdersSearch%5Bstatus%5D=0'], ['class' => 'small-box-footer']) ?>
            </div>
        </div>
        <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="small-box bg-green-active">
                <div class="inner">
                    <h3><?= $completedOrders ?></h3>
                    <h4><?= Yii::t('views', 'Completed orders') ?></h4>
                </div>
                <div class="icon"><i class="fa fa-cart-arrow-down"></i></div>
                <?= Html::a(Yii::t('views', 'More info') . '&nbsp;<i class="fa fa-arrow-circle-right"></i>',
                    ['order/index?OrdersSearch%5Bstatus%5D=3'], ['class' => 'small-box-footer']) ?>
            </div>
        </div>
        <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="small-box bg-blue-active">
                <div class="inner">
                    <h3><?= $allOrders ?></h3>
                    <h4><?= Yii::t('views', 'Orders') ?></h4>
                </div>
                <div class="icon"><i class="fa fa-shopping-cart"></i></div>
                <?= Html::a(Yii::t('views', 'More info') . '&nbsp;<i class="fa fa-arrow-circle-right"></i>',
                    ['order/index'], ['class' => 'small-box-footer']) ?>
            </div>
        </div>

    </div>
</div>

<div class="site-index box box-default">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Yii::t('views', 'Products') ?></h3>
    </div>
    <div class="box-body">

        <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3><?= $brands ?></h3>
                    <h4><?= Yii::t('views', 'Brands') ?></h4>
                </div>
                <div class="icon"><i class="fa fa-star"></i></div>
                <?= Html::a(Yii::t('views', 'More info') . '&nbsp;<i class="fa fa-arrow-circle-right"></i>',
                    ['brand/index'], ['class' => 'small-box-footer']) ?>
            </div>
        </div>
        <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="small-box bg-purple">
                <div class="inner">
                    <h3><?= $categories ?></h3>
                    <h4><?= Yii::t('views', 'Categories') ?></h4>
                </div>
                <div class="icon"><i class="fa fa-list"></i></div>
                <?= Html::a(Yii::t('views', 'More info') . '&nbsp;<i class="fa fa-arrow-circle-right"></i>',
                    ['category/index'], ['class' => 'small-box-footer']) ?>
            </div>
        </div>
        <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="small-box bg-aqua-active">
                <div class="inner">
                    <h3><?= $products ?></h3>
                    <h4><?= Yii::t('views', 'Products') ?></h4>
                </div>
                <div class="icon"><i class="fa fa-shopping-basket"></i></div>
                <?= Html::a(Yii::t('views', 'More info') . '&nbsp;<i class="fa fa-arrow-circle-right"></i>',
                    ['product/index'], ['class' => 'small-box-footer']) ?>
            </div>
        </div>

    </div>
</div>

<div class="site-index box box-default">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Yii::t('views', 'Contents') ?></h3>
    </div>
    <div class="box-body">

        <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3><?= $menus ?></h3>
                    <h4><?= Yii::t('views', 'Menus') ?></h4>
                </div>
                <div class="icon"><i class="fa fa-list"></i></div>
                <?= Html::a(Yii::t('views', 'More info') . '&nbsp;<i class="fa fa-arrow-circle-right"></i>',
                    ['menu/index'], ['class' => 'small-box-footer']) ?>
            </div>
        </div>
        <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="small-box bg-olive">
                <div class="inner">
                    <h3><?= $pages ?></h3>
                    <h4><?= Yii::t('views', 'Pages') ?></h4>
                </div>
                <div class="icon"><i class="fa fa-list-alt"></i></div>
                <?= Html::a(Yii::t('views', 'More info') . '&nbsp;<i class="fa fa-arrow-circle-right"></i>',
                    ['pages/index'], ['class' => 'small-box-footer']) ?>
            </div>
        </div>
        <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="small-box bg-fuchsia-active">
                <div class="inner">
                    <h3><?= $blocks ?></h3>
                    <h4><?= Yii::t('views', 'Blocks') ?></h4>
                </div>
                <div class="icon"><i class="fa fa-cubes"></i></div>
                <?= Html::a(Yii::t('views', 'More info') . '&nbsp;<i class="fa fa-arrow-circle-right"></i>',
                    ['html-parts/index'], ['class' => 'small-box-footer']) ?>
            </div>
        </div>

    </div>
</div>

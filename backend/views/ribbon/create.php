<?php

/* @var $this yii\web\View */
/* @var $model backend\models\product\Ribbons */

$this->title = Yii::t('views', 'Create Ribbon');
$this->params['breadcrumbs'][] = ['label' => Yii::t('views', 'Ribbons'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ribbons-create box box-success">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

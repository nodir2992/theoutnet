<?php

/* @var $this yii\web\View */
/* @var $model backend\models\product\Ribbons */

$this->title = Yii::t('views', 'Update Ribbon: ') . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('views', 'Ribbons'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('views', 'Update');
?>
<div class="ribbons-update box box-primary">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

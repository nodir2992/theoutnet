<?php

/* @var $this yii\web\View */
/* @var $model backend\models\post\Posts */

$this->title = Yii::t('views', 'Update Post: ') . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('views', 'Post Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->category->name, 'url' => ['view', 'id' => $model->category_id]];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('views', 'Update');
?>
<div class="posts-update box box-primary">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

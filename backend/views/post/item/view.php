<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\modules\admin\components\Helper;

/* @var $this yii\web\View */
/* @var $model backend\models\post\Posts */
/* @var $products yii\data\ActiveDataProvider */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel backend\models\product\search\ProductsSearch */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('views', 'Post Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->category->name, 'url' => ['view', 'id' => $model->category_id]];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="posts-view box box-info">
    <div class="box-body">

        <p class="btn-group">
            <?php
            if (Helper::checkRoute('update-item'))
                echo Html::a(Yii::t('views', 'Update'), ['update-item', 'id' => $model->id], ['class' => 'btn btn-primary']);

            if (Helper::checkRoute('delete-item'))
                echo Html::a(Yii::t('views', 'Delete'), ['delete-item', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('views', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ]);

            if (Helper::checkRoute('product-to-post'))
                echo '<span class="btn-group">'
                    . $this->render('_products_add', [
                        'model' => $model,
                        'dataProvider' => $dataProvider,
                        'searchModel' => $searchModel,
                    ])
                    . '</span>';
            ?>
        </p>

        <div class="row">
            <div class="col-md-10 col-lg-8">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        'category_id',
                        'slug',
                        'title',
                        [
                            'attribute' => 'image',
                            'format' => 'raw',
                            'value' => Html::img($model->image, ['alt' => 'post']),
                        ],
                        'summary:ntext',
                        'body:ntext',
                        'meta_title',
                        'meta_keywords',
                        'meta_description:ntext',
                        'created_at:datetime',
                        'updated_at:datetime',
                        [
                            'attribute' => 'status',
                            'format' => 'raw',
                            'value' => $model->getStatusName(),
                        ],
                    ],
                ]) ?>
            </div>
        </div>

        <?php
        if (!empty($products->models))
            echo $this->render('_products', [
                'model' => $model,
                'products' => $products,
            ]);
        ?>

    </div>
</div>

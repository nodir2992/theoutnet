<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use kartik\select2\Select2;
use backend\widgets\PageSize;
use backend\models\product\Products;

/* @var $this yii\web\View */
/* @var $model backend\models\post\Posts */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel backend\models\product\search\ProductsSearch */

Modal::begin([
    'header' => '<h4>' . Yii::t('views', 'Products') . '</h4>',
    'toggleButton' => [
        'label' => Yii::t('views', 'Add products'),
        'class' => 'btn btn-warning'
    ],
]);
echo Html::beginForm(['product-to-post', 'id' => $model->id], 'POST')
    . '<div class="form-inline margin-bottom-5">'
    . Html::submitButton(Yii::t('views', 'Add selected items'), ['class' => 'btn btn-success'])
    . PageSize::widget(['template' => '{list}&nbsp;{label}</div>']);
Pjax::begin();
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'filterSelector' => 'select[name="per_page"]',
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        ['class' => 'yii\grid\CheckboxColumn'],

        [
            'attribute' => 'id',
            'options' => ['width' => '60'],
        ],
        [
            'attribute' => 'name',
            'format' => 'raw',
            'value' => function (Products $data) {
                return Html::a($data->name, ['product/view', 'id' => $data->id], ['target' => '_blank', 'data-pjax' => 0]);
            },
        ],
        [
            'attribute' => 'brand_id',
            'format' => 'raw',
            'filter' => Select2::widget([
                'model' => $searchModel,
                'attribute' => 'brand_id',
                'data' => Products::getBrandsList(),
                'theme' => Select2::THEME_DEFAULT,
                'options' => ['placeholder' => ''],
                'pluginOptions' => ['allowClear' => true],
            ]),
            'value' => function (Products $data) {
                return Html::a($data->brand->name, ['brand/view', 'id' => $data->brand->id], ['target' => '_blank', 'data-pjax' => 0]);
            },
        ],
        [
            'attribute' => 'group_id',
            'format' => 'raw',
            'filter' => Select2::widget([
                'model' => $searchModel,
                'attribute' => 'group_id',
                'data' => Products::getGroupsList(),
                'theme' => Select2::THEME_DEFAULT,
                'options' => ['placeholder' => ''],
                'pluginOptions' => ['allowClear' => true],
            ]),
            'value' => function (Products $data) {
                return Html::a($data->group->name, ['group/view', 'id' => $data->brand->id], ['target' => '_blank', 'data-pjax' => 0]);
            },
        ],
        [
            'attribute' => 'category_id',
            'format' => 'raw',
            'filter' => Select2::widget([
                'model' => $searchModel,
                'attribute' => 'category_id',
                'data' => Products::getCategoriesList(),
                'theme' => Select2::THEME_DEFAULT,
                'options' => ['placeholder' => ''],
                'pluginOptions' => ['allowClear' => true],
            ]),
            'value' => function (Products $data) {
                $result = '';
                if (!empty($categories = $data->categories)) {
                    foreach ($categories as $category) {
                        $result .= Html::a($category->name, ['category/view', 'id' => $category->id], ['target' => '_blank', 'data-pjax' => 0]) . '<br>';
                    }
                }
                return $result;
            },
        ],
        [
            'attribute' => 'discount_id',
            'format' => 'raw',
            'filter' => Select2::widget([
                'model' => $searchModel,
                'attribute' => 'discount_id',
                'data' => Products::getDiscountsList(),
                'theme' => Select2::THEME_DEFAULT,
                'options' => ['placeholder' => ''],
                'pluginOptions' => ['allowClear' => true],
            ]),
            'value' => function (Products $data) {
                return $data->discount_id ? Html::a($data->discount->name, ['group/view', 'id' => $data->brand->id], ['target' => '_blank', 'data-pjax' => 0]) : $data->discount_id;
            },
        ],
        [
            'attribute' => 'ribbon_id',
            'format' => 'raw',
            'filter' => Select2::widget([
                'model' => $searchModel,
                'attribute' => 'ribbon_id',
                'data' => Products::getRibbonsList(),
                'theme' => Select2::THEME_DEFAULT,
                'options' => ['placeholder' => ''],
                'pluginOptions' => ['allowClear' => true],
            ]),
            'value' => function (Products $data) {
                $result = '';
                if (!empty($ribbons = $data->ribbons)) {
                    foreach ($ribbons as $ribbon) {
                        $result .= Html::a($ribbon->name, ['ribbon/view', 'id' => $ribbon->id], ['target' => '_blank', 'data-pjax' => 0]) . '<br>';
                    }
                }
                return $result;
            },
        ],
        [
            'attribute' => 'status',
            'format' => 'raw',
            'filter' => Select2::widget([
                'model' => $searchModel,
                'attribute' => 'status',
                'data' => Products::getStatusArray(),
                'theme' => Select2::THEME_DEFAULT,
                'options' => ['placeholder' => ''],
                'hideSearch' => true,
                'pluginOptions' => ['allowClear' => true],
            ]),
            'value' => function (Products $data) {
                return $data->getStatusName();
            },
        ],

    ],
]);

Pjax::end();
echo Html::endForm();
Modal::end();
$this->registerCss('.modal-dialog {width: 98%;}');

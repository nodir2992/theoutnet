<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\grid\GridView;
use backend\models\product\Products;

/* @var $this yii\web\View */
/* @var $model backend\models\post\Posts */
/* @var $products yii\data\ActiveDataProvider */

echo '<h3>' . Yii::t('views', 'Products') . '</h3>';
echo Html::beginForm(['product-to-post-delete', 'id' => $model->id], 'POST')
    . '<div class="btn-group margin-bottom-5">'
    . Html::submitButton(Yii::t('views', 'Delete selected items'), [
        'class' => 'btn btn-warning',
        'data' => [
            'confirm' => Yii::t('views', 'Are you sure you want to delete selected items?'),
            'method' => 'post',
        ],
    ])
    . Html::a(Yii::t('views', 'Delete all items'), ['product-to-post-delete-all', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'data' => [
            'confirm' => Yii::t('views', 'Are you sure you want to delete all items?'),
            'method' => 'post',
        ],
    ])
    . '</div>';
Pjax::begin();
echo GridView::widget([
    'dataProvider' => $products,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        ['class' => 'yii\grid\CheckboxColumn'],

        [
            'attribute' => 'name',
            'format' => 'raw',
            'value' => function (Products $data) {
                return Html::a($data->name, ['product/view', 'id' => $data->id], ['target' => '_blank', 'data-pjax' => 0]);
            },
        ],

        [
            'attribute' => 'brand_id',
            'format' => 'raw',
            'value' => function (Products $data) {
                return Html::a($data->brand->name, ['brand/view', 'id' => $data->brand->id], ['target' => '_blank', 'data-pjax' => 0]);
            },
        ],
        [
            'attribute' => 'group_id',
            'format' => 'raw',
            'value' => function (Products $data) {
                return Html::a($data->group->name, ['group/view', 'id' => $data->brand->id], ['target' => '_blank', 'data-pjax' => 0]);
            },
        ],
        [
            'attribute' => 'category_id',
            'format' => 'raw',
            'value' => function (Products $data) {
                $result = '';
                if (!empty($categories = $data->categories)) {
                    foreach ($categories as $category) {
                        $result .= Html::a($category->name, ['category/view', 'id' => $category->id], ['target' => '_blank', 'data-pjax' => 0]) . '<br>';
                    }
                }
                return $result;
            },
        ],
        [
            'attribute' => 'discount_id',
            'format' => 'raw',
            'value' => function (Products $data) {
                return $data->discount_id ? Html::a($data->discount->name, ['group/view', 'id' => $data->brand->id], ['target' => '_blank', 'data-pjax' => 0]) : $data->discount_id;
            },
        ],
        [
            'attribute' => 'ribbon_id',
            'format' => 'raw',
            'value' => function (Products $data) {
                $result = '';
                if (!empty($ribbons = $data->ribbons)) {
                    foreach ($ribbons as $ribbon) {
                        $result .= Html::a($ribbon->name, ['ribbon/view', 'id' => $ribbon->id], ['target' => '_blank', 'data-pjax' => 0]) . '<br>';
                    }
                }
                return $result;
            },
        ],
        [
            'attribute' => 'status',
            'format' => 'raw',
            'value' => function (Products $data) {
                return $data->getStatusName();
            },
        ],
    ],
]);
Pjax::end();
echo Html::endForm();

<?php

use yii\widgets\Pjax;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\web\JsExpression;
use kartik\select2\Select2;
use backend\widgets\PageSize;
use backend\widgets\ActionsApply;
use backend\models\product\Categories;
use wbraganca\fancytree\FancytreeWidget;
use backend\modules\admin\components\Helper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\product\search\CategoriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $dataTree array */

$this->title = Yii::t('views', 'Categories');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categories-index box box-default">
    <div class="box-body">
        <p>
            <?php if (Helper::checkRoute('create'))
                echo Html::a(Yii::t('views', 'Create'), ['create'], ['class' => 'btn btn-success']); ?>
        </p>
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#tab_1" data-toggle="tab"><i class="fa fa-th"></i>&nbsp;<b> Grid</b></a>
                </li>
                <li>
                    <a href="#tab_2" data-toggle="tab"><i class="fa fa-tree"></i>&nbsp;<b> Tree</b></a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane grid active" id="tab_1">

                    <div class="row">
                        <div class="col-lg-9">
                            <?php ActionsApply::begin(['template' => '<div class="form-inline margin-bottom-5">{list}{button}']);
                            echo PageSize::widget([
                                'template' => Helper::checkRoute('apply-items') ? '{list}&nbsp;{label}</div>' :
                                    '<div class="form-inline margin-bottom-5">{list}&nbsp;{label}</div>'
                            ]); ?>
                            <?php Pjax::begin(['id' => 'data_grid']); ?>
                            <?= GridView::widget([
                                'dataProvider' => $dataProvider,
                                'filterModel' => $searchModel,
                                'filterSelector' => 'select[name="per_page"]',
                                'columns' => [
                                    ['class' => 'yii\grid\SerialColumn'],
                                    ['class' => 'yii\grid\CheckboxColumn'],

                                    [
                                        'attribute' => 'id',
                                        'options' => ['width' => '70'],
                                    ],
                                    [
                                        'attribute' => 'parent_id',
                                        'format' => 'raw',
                                        'filter' => Select2::widget([
                                            'data' => Categories::getCategoriesArray(0),
                                            'model' => $searchModel,
                                            'attribute' => 'parent_id',
                                            'theme' => Select2::THEME_DEFAULT,
                                            'options' => ['placeholder' => ''],
                                            'pluginOptions' => ['allowClear' => true],
                                        ]),
                                        'value' => function (Categories $data) {
                                            return $data->parent_id ? Html::a($data->parent->name, ['view', 'id' => $data->parent_id], ['data-pjax' => 0]) : $data->parent_id;
                                        },
                                        'options' => ['width' => '250'],
                                    ],
                                    [
                                        'attribute' => 'name',
                                        'format' => 'raw',
                                        'value' => function (Categories $data) {
                                            return (Helper::checkRoute('view')) ? Html::a($data->name, ['view', 'id' => $data->id], ['data-pjax' => 0]) : $data->name;
                                        },
                                    ],
                                    [
                                        'attribute' => 'status',
                                        'format' => 'raw',
                                        'filter' => Categories::getStatusArray(),
                                        'value' => function (Categories $data) {
                                            return $data->getStatusName();
                                        },
                                    ],
                                    [
                                        'attribute' => 'created_at',
                                        'format' => 'date',
                                        'filter' => false,
                                    ],
                                    [
                                        'attribute' => 'updated_at',
                                        'format' => 'date',
                                        'filter' => false,
                                    ],

                                    [
                                        'class' => 'yii\grid\ActionColumn',
                                        'template' => Helper::filterActionColumn(['view', 'update', 'delete'])
                                    ],
                                ],
                            ]); ?>
                            <?php Pjax::end(); ?>
                            <?php ActionsApply::end(); ?>
                        </div>
                    </div>
                </div>
                <div class="tab-pane tree" id="tab_2">

                    <?php Pjax::begin(['id' => 'data_tree']); ?>
                    <?= (!empty($dataTree) && Helper::checkRoute('drag-drop')) ? FancytreeWidget::widget([
                        'options' => [
                            'source' => $dataTree,
                            'extensions' => ['dnd'],
                            'dnd' => [
                                'preventVoidMoves' => true,
                                'preventRecursiveMoves' => true,
                                'autoExpandMS' => 400,
                                'dragStart' => new JsExpression('function(node, data) {
                                    return true;
                                }'),
                                'dragEnter' => new JsExpression('function(node, data) {
                                    return true;
                                }'),
                                'dragDrop' => new JsExpression('function(node, data) {
                                    var overlay = $("#overlay");
                                    var hitMode = data.hitMode;
                                    var nodeId = data.otherNode.key;
                                    var otherNodeId = node.key;
                                    overlay.show();
                                    data.otherNode.moveTo(node, hitMode);
                                    $.ajax({
                                        url: "drag-drop",
                                        type: "post",
                                        data: {
                                            hit_mode: hitMode,
                                            id: nodeId,
                                            other_id: otherNodeId
                                        },
                                        success: function (data) {
                                            $.pjax.reload({
                                                container: "#data_grid"
                                            }).done(function () {
                                                $.pjax.reload({
                                                    container: "#data_tree"
                                                }).done(function () {
                                                    overlay.hide();
                                                });
                                            });
                                        }
                                    });
                                    
                                }'),
                            ],
                        ]
                    ]) : Yii::t('yii', 'No results found.'); ?>
                    <?php Pjax::end(); ?>

                </div>
            </div>
        </div>
    </div>
</div>

<?php

/* @var $this yii\web\View */
/* @var $model backend\models\product\Categories */

$this->title = Yii::t('views', 'Create Category');
$this->params['breadcrumbs'][] = ['label' => Yii::t('views', 'Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categories-create box box-success">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

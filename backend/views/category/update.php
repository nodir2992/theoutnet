<?php

/* @var $this yii\web\View */
/* @var $model backend\models\product\Categories */

$this->title = Yii::t('views', 'Update Category: ') . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('views', 'Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('views', 'Update');
?>
<div class="categories-update box box-primary">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

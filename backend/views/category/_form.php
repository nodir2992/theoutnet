<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use rmrevin\yii\fontawesome\FA;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
use mihaildev\elfinder\InputFile;

/* @var $this yii\web\View */
/* @var $model backend\models\product\Categories */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="categories-form box-body">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-lg-6 col-md-8">
            <?= $form->field($model, 'parent_id')->widget(Select2::className(), [
                'data' => $model->getCategoriesArray($model->isNewRecord ? 0 : $model->id),
                'theme' => Select2::THEME_DEFAULT,
                'options' => ['placeholder' => ''],
                'pluginOptions' => ['allowClear' => true],
            ]) ?>

            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'image')->widget(InputFile::className(), [
                'controller' => 'elfinder',
                'path' => '/product/category',
                'filter' => 'image',
                'template' => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
                'options' => ['class' => 'form-control'],
                'buttonOptions' => ['class' => 'btn btn-warning'],
                'buttonName' => FA::i(FA::_PHOTO),
            ]) ?>

            <?= $form->field($model, 'description')->widget(CKEditor::className(), [
                'editorOptions' => ElFinder::ckeditorOptions(
                    ['elfinder', 'path' => 'category'],
                    [
                        'allowedContent' => true,
                        'height' => 400,
                        'toolbarGroups' => [
                            'mode', 'undo', 'selection',
                            ['name' => 'clipboard', 'groups' => ['clipboard', 'doctools', 'cleanup']],
                            ['name' => 'basicstyles', 'groups' => ['basicstyles', 'colors']],
                            ['name' => 'paragraph', 'groups' => ['align', 'templates', 'list', 'indent']],
                            'styles', 'insert', 'blocks', 'links', 'find', 'tools', 'about',
                        ]
                    ]
                ),
            ]) ?>

            <?= $form->field($model, 'meta_title')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'meta_keywords')->textarea(['rows' => 6]) ?>

            <?= $form->field($model, 'meta_description')->textarea(['rows' => 6]) ?>

            <?= $form->field($model, 'weight')->textInput(['type' => 'number']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-2 col-sm-4  col-xs-6">
            <?= $form->field($model, 'status')->dropDownList($model->getStatusArray()) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('views', 'Create') : Yii::t('views', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

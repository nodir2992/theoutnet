<?php

/* @var $this yii\web\View */
/* @var $model backend\models\product\Discounts */

$this->title = Yii::t('views', 'Create Discount');
$this->params['breadcrumbs'][] = ['label' => Yii::t('views', 'Discounts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="discounts-create box box-success">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

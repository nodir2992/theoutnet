<?php

/* @var $this yii\web\View */
/* @var $model backend\models\product\Discounts */

$this->title = Yii::t('views', 'Update Discount: ') . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('views', 'Discounts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('views', 'Update');
?>
<div class="discounts-update box box-primary">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

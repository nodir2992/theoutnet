<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use common\models\Settings;
use common\models\LoginForm;
use backend\models\menu\Menus;
use backend\models\page\Pages;
use backend\models\order\Orders;
use backend\models\product\Brands;
use backend\models\parts\HtmlParts;
use backend\models\product\Products;
use backend\models\comment\Comments;
use backend\models\product\Categories;
use backend\models\comment\CommentsSearch;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index', [
            'title' => Yii::$app->name,
            'newOrders' => count(Orders::findAll(['status' => Orders::STATUS_NEW])),
            'completedOrders' => count(Orders::findAll(['status' => Orders::STATUS_COMPLETED])),
            'allOrders' => count(Orders::find()->where(['!=', 'status', Orders::STATUS_DELETED])->all()),
            'brands' => count(Brands::find()->where(['!=', 'status', Brands::STATUS_DELETED])->all()),
            'categories' => count(Categories::find()->all()),
            'products' => count(Products::find()->where(['!=', 'status', Products::STATUS_DELETED])->all()),
            'menus' => count(Menus::find()->all()),
            'pages' => count(Pages::find()->all()),
            'blocks' => count(HtmlParts::find()->all()),
        ]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            if (Yii::$app->user->can('accessDashboard')) {
                return $this->goBack();
            } else {
                Yii::$app->user->logout();
                $model->addError('password', 'Incorrect username or password.');
            }
        }

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Ajax Set Session SidebarCollapse.
     */
    public function actionSessionSidebar()
    {
        $session = Yii::$app->session;
        $session['sidebar'] = $session['sidebar'] ? false : true;
    }

    /**
     * Displays FileManges page.
     * @return string
     */
    public function actionFiles()
    {
        return $this->render('files');
    }

    /**
     * Displays Site Setting page.
     * @return mixed
     */
    public function actionSetting()
    {
        if ($post = Yii::$app->request->post()) {
            foreach ($post as $key => $value) {
                if (($setting = Settings::findOne(['key' => $key])) !== null && !empty($value)) {
                    $setting->value = $value;
                    $setting->save();
                }
            }
            Yii::$app->session->setFlash('success', 'Settings saved.');
            return $this->refresh();
        }

        return $this->render('setting', [
            'model' => new Settings(),
            'models' => Settings::find()->all()
        ]);
    }

    /**
     * Action Create Site Setting.
     * @return mixed
     */
    public function actionSettingCreate()
    {
        $model = new Settings();
        ($model->load(Yii::$app->request->post()) && $model->save()) ?
            Yii::$app->session->setFlash('success', $model->label . ' field added.') :
            Yii::$app->session->setFlash('error', 'Field not added.');

        return $this->redirect(['setting']);
    }

    /**
     * Deletes an existing Setting model.
     * @param integer $id
     * @return mixed
     */
    public function actionSettingDelete($id)
    {
        Settings::findOne($id)->delete();
        Yii::$app->session->setFlash('warning', 'Field deleted.');

        return $this->redirect(['setting']);
    }

    /**
     * Lists all Comments models.
     * @return mixed
     */
    public function actionComments()
    {
        $searchModel = new CommentsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('comments', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Ajax SetCommentStatus.
     */
    public function actionSetCommentStatus()
    {
        if ($post = Yii::$app->request->post()) {
            if ($model = Comments::findOne($post['id'])) {
                $model->status = $post['status'];
                if ($model->save())
                    return true;
            }
        }

        return false;
    }

}

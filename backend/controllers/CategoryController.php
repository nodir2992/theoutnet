<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use backend\models\product\Categories;
use backend\models\product\search\CategoriesSearch;

/**
 * CategoryController implements the CRUD actions for Categories model.
 */
class CategoryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Categories models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CategoriesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataTree' => $this->getCategoriesTree(),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Categories model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Categories model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Categories();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Categories model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Categories model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Delete or Update an existing Categories model.
     * The browser will be redirected to the 'index' page.
     * @return mixed
     */
    public function actionApply()
    {
        if ($post = Yii::$app->request->post()) {
            if (isset($post['selection'])) {

                $condition = ['id' => array_map(function ($value) {
                    return (int)$value;
                }, $post['selection'])];

                switch ($post['action']) {
                    case 'deactivate':
                        Categories::updateAll(['status' => Categories::STATUS_INACTIVE], $condition);
                        Yii::$app->session->setFlash('warning', Yii::t('yii', 'Selected items <b>DEACTIVATED</b>.'));
                        break;
                    case 'activate':
                        Categories::updateAll(['status' => Categories::STATUS_ACTIVE], $condition);
                        Yii::$app->session->setFlash('success', Yii::t('yii', 'Selected items <b>ACTIVATED</b>.'));
                        break;
                    case 'delete':
                        Categories::deleteAll($condition);
                        Yii::$app->session->setFlash('danger', Yii::t('yii', 'Selected items <b>DELETED</b>.'));
                        break;
                }
            }
        }

        return $this->redirect(['index']);
    }

    /**
     * Ajax action DragDrop.
     * @return bool
     */
    public function actionDragDrop()
    {
        if ($post = Yii::$app->request->post()) {

            $hitMode = (string)$post['hit_mode'];
            $id = (int)$post['id'];
            $otherId = (int)$post['other_id'];

            $model = $this->findModel($id);
            $otherModel = $this->findModel($otherId);
            $weight = $otherModel->weight;
            $parentId = $otherModel->parent_id;

            if ($hitMode != 'over') {
                Categories::updateAllCounters(['weight' => 1], ['and',
                    ['parent_id' => $parentId],
                    ['>', 'weight', $weight],
                    ['!=', 'id', $id],
                ]);
            }

            if ($hitMode == 'over') {
                $model->parent_id = $otherId;

            } elseif ($hitMode == 'after') {
                $model->parent_id = $parentId;

                if ($id < $otherId) {
                    $model->weight = $weight + 1;
                } else {
                    $model->weight = $weight;
                }

                Categories::updateAllCounters(['weight' => 1], ['and',
                    ['parent_id' => $parentId],
                    ['=', 'weight', $weight],
                    ['!=', 'id', $id],
                    ['>', 'id', $otherId],
                ]);

            } elseif ($hitMode == 'before') {
                $model->parent_id = $parentId;
                $model->weight = $weight;

                Categories::updateAllCounters(['weight' => 1], ['and',
                    ['parent_id' => $parentId],
                    ['=', 'weight', $weight],
                    ['!=', 'id', $id],
                    ['>=', 'id', $otherId],
                ]);

            }

            $model->save();

            return true;
        } else {
            return false;
        }
    }

    /**
     * Finds the Categories model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Categories the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Categories::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @return array
     */
    protected function getCategoriesTree()
    {
        $data = [];
        if (!empty($models = Categories::find()->where(['parent_id' => NULL])->orderBy(['weight' => SORT_ASC])->all())) {
            /** @var Categories $model */
            foreach ($models as $model) {
                if (!empty($childs = $this->getCategoriesTreeChilds($model->id))) {
                    array_push($data, [
                        'title' => $model->name,
                        'key' => $model->id,
                        'folder' => true,
                        'children' => $childs
                    ]);
                } else {
                    array_push($data, [
                        'title' => $model->name,
                        'key' => $model->id,
                    ]);
                }
            }
        }

        return $data;
    }

    /**
     * @param integer $id
     * @return array
     */
    protected function getCategoriesTreeChilds($id)
    {
        $data = [];
        if (!empty($models = Categories::find()->where(['parent_id' => $id])->orderBy(['weight' => SORT_ASC])->all())) {
            /** @var Categories $model */
            foreach ($models as $model) {
                if (!empty($childs = $this->getCategoriesTreeChilds($model->id))) {
                    array_push($data, [
                        'title' => $model->name,
                        'key' => $model->id,
                        'folder' => true,
                        'children' => $childs
                    ]);
                } else {
                    array_push($data, [
                        'title' => $model->name,
                        'key' => $model->id,
                    ]);
                }
            }
        }

        return $data;
    }
}

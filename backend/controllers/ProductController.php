<?php

namespace backend\controllers;

use Yii;
use yii\helpers\Json;
use yii\web\Response;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use PHPExcel;
use PHPExcel_IOFactory;
use backend\models\product\Filters;
use backend\models\product\Products;
use backend\models\product\DollarRate;
use backend\models\product\search\ProductsSearch;
use backend\models\comment\Comments;
use backend\models\comment\CommentsSearch;

/**
 * ProductController implements the CRUD actions for Products model.
 */
class ProductController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Products models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $allColumns = [
            'id' => Yii::t('model', 'ID'),
            'name' => Yii::t('model', 'Name'),
            'barcode' => Yii::t('model', 'Barcode'),
            'price' => Yii::t('model', 'Price'),
            'old_price' => Yii::t('model', 'Old Price'),
            'brand' => Yii::t('model', 'Brand'),
            'group' => Yii::t('model', 'Group'),
            'discount' => Yii::t('model', 'Discount'),
            'ribbon' => Yii::t('model', 'Ribbon'),
            'sticker' => Yii::t('model', 'Sticker'),
            'category' => Yii::t('model', 'Category'),
            'status' => Yii::t('model', 'Status'),
            'created_at' => Yii::t('model', 'Created At'),
            'updated_at' => Yii::t('model', 'Updated At'),
        ];
        $queryParams = Yii::$app->request->queryParams;
        $visibleColumns = isset($queryParams['visibleColumns']) ? $queryParams['visibleColumns'] :
            array_diff(array_keys($allColumns), ['id', 'old_price', 'discount', 'ribbon', 'sticker', 'updated_at']);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'allColumns' => $allColumns,
            'visibleColumns' => $visibleColumns,
        ]);
    }

    /**
     * Displays a single Products model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Products model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Products();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Products model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if (!empty($filters = Filters::findAll(['group_id' => $model->group_id, 'status' => Filters::STATUS_ACTIVE]))) {
            foreach ($filters as $filter) {
                $model->dataFilters[$filter->name] = ArrayHelper::map($filter->productFilterItems, 'id', 'name');
            }
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Products model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->setStatusDeleted();

        return $this->redirect(['index']);
    }

    /**
     * Delete or Update an existing Products model.
     * The browser will be redirected to the 'index' page.
     * @return mixed
     */
    public function actionApply()
    {
        if ($post = Yii::$app->request->post()) {
            if (isset($post['selection'])) {

                $condition = ['id' => array_map(function ($value) {
                    return (int)$value;
                }, $post['selection'])];

                switch ($post['action']) {
                    case 'deactivate':
                        Products::updateAll(['status' => Products::STATUS_INACTIVE], $condition);
                        Yii::$app->session->setFlash('info', Yii::t('yii', 'The status of selected items changed to <b>INACTIVE</b>.'));
                        break;
                    case 'available':
                        Products::updateAll(['status' => Products::STATUS_AVAILABLE], $condition);
                        Yii::$app->session->setFlash('info', Yii::t('yii', 'The status of selected items changed to <b>AVAILABLE</b>.'));
                        break;
                    case 'not_available':
                        Products::updateAll(['status' => Products::STATUS_NOT_AVAILABLE], $condition);
                        Yii::$app->session->setFlash('info', Yii::t('yii', 'The status of selected items changed to <b>NOT AVAILABLE</b>.'));
                        break;
                    case 'delete':
                        Products::updateAll(['status' => Products::STATUS_DELETED], $condition);
                        Yii::$app->session->setFlash('danger', Yii::t('yii', 'Selected items <b>DELETED</b>.'));
                        break;
                }
            }
        }

        return $this->redirect(['index']);
    }

    /**
     * Upload Product Image.
     * @return object
     */
    public function actionUploadImage()
    {
        $data = [];

        if ($file_image = UploadedFile::getInstancesByName('image')) {
            foreach ($file_image as $file) {
                $folder = '/product/images/';
                $ext = pathinfo($file->name, PATHINFO_EXTENSION);
                $name = pathinfo($file->name, PATHINFO_FILENAME);
                $generateName = Yii::$app->security->generateRandomString();
                $path = Yii::getAlias('@uploadsPath') . $folder . $generateName . ".{$ext}";
                $file->saveAs($path);
                $data = [
                    'generate_name' => $generateName,
                    'name' => $name,
                    'path' => Yii::getAlias('@uploadsUrl') . $folder . $generateName . ".{$ext}"
                ];
            }
        }

        return json_encode($data);
    }

    /**
     * Delete Product Image.
     * @return object
     */
    public function actionDeleteImage()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ($post = Yii::$app->request->post()) ? $post['key'] : null;
    }

    /**
     * Filters List.
     * @return mixed
     */
    public function actionGetFilterList()
    {
        $out = [];
        if ($post = Yii::$app->request->post()) {
            if (($groupId = (int)$post['depdrop_all_params']['main_group']) > 0) {
                if (!empty($filters = Filters::findAll(['group_id' => $groupId, 'status' => Filters::STATUS_ACTIVE]))) {
                    foreach ($filters as $filter) {
                        $out[$filter->name] = ArrayHelper::toArray($filter->productFilterItems, ['id', 'name']);
                    }

                    return Json::encode(['output' => $out, 'selected' => '']);
                }
            }
        }

        return Json::encode(['output' => $out, 'selected' => '']);
    }

    /**
     * Import In Excel.
     * @return mixed
     */
    public function actionExcelImport()
    {
        if ($post = Yii::$app->request->post()) {
            $excel = Yii::getAlias('@rootDir') . $post['file'];
            try {
                $inputFileType = PHPExcel_IOFactory::identify($excel);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($excel);
            } catch (\Exception $e) {
                die('Error');
            }

            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();

            for ($row = 2; $row <= $highestRow; ++$row) {
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                $barcode = (string)$rowData[0][0];
                $name = (string)$rowData[0][1];
                $price = (int)$rowData[0][2];
                $oldPrice = (int)$rowData[0][3];
                $status = (int)$rowData[0][4];

                if (!empty($barcode) && !empty($price)) {
                    if ($product = Products::findOne(['barcode' => $barcode])) {
                        $product->status = ($status == Products::STATUS_AVAILABLE) ? Products::STATUS_AVAILABLE : Products::STATUS_NOT_AVAILABLE;
                        $product->price = $price;
                        $product->old_price = $oldPrice ?: NULL;
                        $product->save();
                    }
                }
            }
        }

        return $this->redirect(['index']);
    }

    /**
     * Export to Excel
     * @return mixed
     */
    public function actionExcelExport()
    {
        $objPHPExcel = new PHPExcel();

        $sheet = 0;
        $objPHPExcel->setActiveSheetIndex($sheet);

//        $objPHPExcel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);

        $objPHPExcel->getActiveSheet()
            ->setTitle('Sheet')
            ->setCellValue('A1', 'barcode')
            ->setCellValue('B1', 'name')
            ->setCellValue('C1', 'price')
            ->setCellValue('D1', 'old price')
            ->setCellValue('E1', 'status');

        $products = Products::find()->where(['status' => [Products::STATUS_AVAILABLE, Products::STATUS_NOT_AVAILABLE]])->all();

        /**
         * @var $product Products
         */
        if ($products) {
            $row = 2;
            foreach ($products as $product) {
                $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, $product->barcode);
                $objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $product->name);
                $objPHPExcel->getActiveSheet()->setCellValue('C' . $row, $product->price);
                $objPHPExcel->getActiveSheet()->setCellValue('D' . $row, $product->old_price);
                $objPHPExcel->getActiveSheet()->setCellValue('E' . $row, $product->status);
                $row++;
            }
        }

        header('Content-Type: application/vnd.ms-excel');
        $filename = "ExcelReport_" . date("d-m-Y-His") . ".xls";
        header('Content-Disposition: attachment;filename=' . $filename);
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');

        return $this->redirect(['index']);

    }

    /**
     * Finds the Products model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Products the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Products::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /* ********** DollarRate Model ********** */

    /**
     * Lists all DollarRate models.
     * @return mixed
     */
    public function actionDollarRate()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => DollarRate::find(),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $model = new DollarRate();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'DollarRate saved!');
            return $this->refresh();
        }

        return $this->render('dollar-rate', [
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing DollarRate model.
     * @param integer $id
     * @return mixed
     */
    public function actionDollarRateDelete($id)
    {
        if ($model = DollarRate::findOne($id)) {
            $model->delete();
            Yii::$app->session->setFlash('warning', 'DollarRate deleted!');
        }

        return $this->redirect(['dollar-rate']);
    }

    /**
     * Lists all Comments models.
     * @return mixed
     */
    public function actionComments()
    {
        $searchModel = new CommentsSearch([
            'model' => Products::tableName()
        ]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('comments', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Ajax SetCommentStatus.
     */
    public function actionSetCommentStatus()
    {
        if ($post = Yii::$app->request->post()) {
            if ($model = Comments::findOne($post['id'])) {
                $model->status = $post['status'];
                if ($model->save())
                    return true;
            }
        }

        return false;
    }
}

<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use backend\models\product\Groups;
use backend\models\product\Filters;
use backend\models\product\FilterItems;
use backend\models\product\search\FiltersSearch;
use backend\models\product\search\GroupsSearch;

/**
 * GroupController implements the CRUD actions for Groups model.
 */
class GroupController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Groups models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GroupsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Groups model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $searchModel = new FiltersSearch(['group_id' => $id]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Groups model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Groups();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Groups model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Groups model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->setStatusDeleted();

        return $this->redirect(['index']);
    }

    /**
     * Delete or Update an existing Groups model.
     * The browser will be redirected to the 'index' page.
     * @return mixed
     */
    public function actionApply()
    {
        if ($post = Yii::$app->request->post()) {
            if (isset($post['selection'])) {

                $condition = ['id' => array_map(function ($value) {
                    return (int)$value;
                }, $post['selection'])];

                switch ($post['action']) {
                    case 'deactivate':
                        Groups::updateAll(['status' => Groups::STATUS_INACTIVE], $condition);
                        Yii::$app->session->setFlash('warning', Yii::t('yii', 'Selected items <b>DEACTIVATED</b>.'));
                        break;
                    case 'activate':
                        Groups::updateAll(['status' => Groups::STATUS_ACTIVE], $condition);
                        Yii::$app->session->setFlash('success', Yii::t('yii', 'Selected items <b>ACTIVATED</b>.'));
                        break;
                    case 'delete':
                        Groups::updateAll(['status' => Groups::STATUS_DELETED], $condition);
                        Yii::$app->session->setFlash('danger', Yii::t('yii', 'Selected items <b>DELETED</b>.'));
                        break;
                }
            }
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Groups model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Groups the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Groups::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /* ********** Filters Model ********** */

    /**
     * Displays a single Filters model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewFilter($id)
    {
        return $this->render('filter/view', [
            'model' => $this->findModelFilter($id),
        ]);
    }

    /**
     * Creates a new Filters model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param integer $group_id
     * @return mixed
     */
    public function actionCreateFilter($group_id)
    {
        $this->findModel($group_id);
        $model = new Filters(['group_id' => $group_id]);
        $modelItems = [new FilterItems()];

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (!empty($modelItems = Yii::$app->request->post('FilterItems'))) {
                foreach ($modelItems as $item) {
                    $filter = new FilterItems(['filter_id' => $model->id]);
                    $filter->name = $item['name'];
                    $filter->save();
                }
            }

            return $this->redirect(['view-filter', 'id' => $model->id]);
        } else {
            return $this->render('filter/create', [
                'model' => $model,
                'modelItems' => $modelItems,
            ]);
        }
    }

    /**
     * Updates an existing Filters model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdateFilter($id)
    {
        $model = $this->findModelFilter($id);
        $modelItems = empty($model->productFilterItems) ? [new FilterItems()] : $model->productFilterItems;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $oldIds = ArrayHelper::map($modelItems, 'id', 'id');
            if (!empty($modelItems = Yii::$app->request->post('FilterItems'))) {
                if (!empty($deletedIds = array_diff($oldIds, array_filter(ArrayHelper::map($modelItems, 'id', 'id')))))
                    FilterItems::deleteAll(['id' => $deletedIds]);
                foreach ($modelItems as $item) {
                    $filter = !empty($item['id']) ? FilterItems::findOne($item['id']) : new FilterItems(['filter_id' => $model->id]);
                    $filter->name = $item['name'];
                    $filter->save();
                }
            }

            return $this->redirect(['view-filter', 'id' => $model->id]);
        } else {
            return $this->render('filter/update', [
                'model' => $model,
                'modelItems' => $modelItems,
            ]);
        }
    }

    /**
     * Deletes an existing Filters model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteFilter($id)
    {
        $model = $this->findModelFilter($id);
        $group_id = $model->group_id;
        $model->delete();

        return $this->redirect(['view', 'id' => $group_id]);
    }

    /**
     * Finds the Filters model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Filters the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelFilter($id)
    {
        if (($model = Filters::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

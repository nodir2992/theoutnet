<?php

namespace backend\controllers;

use Yii;
use kartik\mpdf\Pdf;
use yii\helpers\Html;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use common\models\Settings;
use backend\models\order\Orders;
use backend\models\parts\HtmlParts;
use backend\models\order\OrderItems;
use backend\models\order\OrdersSearch;

/**
 * OrderController implements the CRUD actions for Orders model.
 */
class OrderController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Orders models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrdersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Orders model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Updates an existing Orders model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $oldIds = ArrayHelper::map($model->orderItems, 'product_id', 'product_id');
            if (!empty($modelItems = Yii::$app->request->post('OrderItems'))) {
                if (!empty($deletedIds = array_diff($oldIds, array_filter(ArrayHelper::map($modelItems, 'product_id', 'product_id')))))
                    OrderItems::deleteAll(['order_id' => $id, 'product_id' => $deletedIds]);

                foreach ($modelItems as $item) {
                    $orderItem = OrderItems::findOne(['order_id' => $id, 'product_id' => $item['product_id']]);
                    $orderItem->price = $item['price'];
                    $orderItem->quantity = $item['quantity'];
                    $orderItem->total_price = $orderItem->price * $orderItem->quantity;
                    $orderItem->save();
                }

                if ($model->sendEmail)
                    $this->sendEmailOrder($id);
            }

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Orders model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->setStatusDeleted();

        return $this->redirect(['index']);
    }

    /**
     * Displays a single Candidates model.
     * @param integer $id
     * @return mixed
     */
    public function actionExportPdf($id)
    {
        $model = $this->findModel($id);
        $addressStore = HtmlParts::findOne(['key' => 'address_store_pdf', 'status' => HtmlParts::STATUS_ACTIVE]);

        $content = $this->renderPartial('pdf', [
            'model' => $model,
            'logo' => Settings::getLogoValue(),
            'addressStore' => ($addressStore !== null) ? $addressStore->body : ''
        ]);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'content' => $content,
            'cssFile' => '@backend/web/css/order_export_pdf.css',
            'options' => ['title' => Yii::t('views', 'Order № ') . $model->id],
        ]);

        return $pdf->render();
    }

    /**
     * Ajax SetOrderStatus.
     */
    public function actionSetStatus()
    {
        if ($post = Yii::$app->request->post()) {
            $id = $post['id'];
            $status = $post['status'];
            $model = $this->findModel($id);
            $model->status = $status;
            if ($model->save())
                return true;
        }

        return false;
    }

    /**
     * Finds the Orders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Orders the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Orders::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Order send email.
     * @param integer $id
     * @return bool
     */
    protected function sendEmailOrder($id)
    {
        $order = $this->findModel($id);

        try {
            $message = Yii::$app->mailer->compose();
            $webroot = Yii::getAlias('@rootDir');
            $logo = $message->embed($webroot . Settings::getLogoValue());
            $orderStr = '';

            if (!empty($order->orderItems)) {
                $totalQuantity = 0;
                $totalPrice = 0;
                $orderItems = '';
                $border = 'border:1px solid #D6D4D4;padding:10px;';

                foreach ($order->orderItems as $item) {
                    $totalQuantity += $item->quantity;
                    $totalPrice += $item->total_price;

                    $orderItems .= '<tr><td style="' . $border . '">'
                        . $item->product->name
                        . '</td><td style="' . $border . '">'
                        . Yii::$app->formatter->asDecimal($item->price, 0)
                        . '</td><td style="white-space: nowrap;' . $border . '">'
                        . $item->quantity
                        . '</td><td style="white-space: nowrap;' . $border . '">'
                        . Yii::$app->formatter->asDecimal($item->total_price, 0)
                        . '&nbsp;<i>сум</i></td></tr>';
                }

                $orderStr = '<table style="text-align:center;font-size:14px;width:100%;border-collapse:collapse">'
                    . '<thead><tr>'
                    . '<th style="width:55%;' . $border . '">Наименование</th>'
                    . '<th style="width:15%;' . $border . '">Цена</th>'
                    . '<th style="width:15%;' . $border . '">Количество</th>'
                    . '<th style="width:15%;' . $border . '">Сумма</th>'
                    . '</tr></thead><tbody>'
                    . $orderItems
                    . '<tr><th colspan="2" style="' . $border . '">Общий</th>'
                    . '<th style="' . $border . '">'
                    . $totalQuantity
                    . '</th><th style="white-space: nowrap;' . $border . '">'
                    . Yii::$app->formatter->asDecimal($totalPrice, 0)
                    . '&nbsp;<i>сум</i></th></tr></tbody></table>';
            }

            $message->setHtmlBody('<p align="center">'
                . Html::a(Html::img($logo, ['alt' => 'logo', 'width' => '200px']), Yii::$app->request->hostInfo, ['target' => '_blank'])
                . '</p><h2>Здравствуйте, '
                . $order->first_name
                . '&nbsp;'
                . $order->last_name
                . '!</h2><p>Благодарим за покупку в интернет магазине THEOUTNET.UZ.</p><p>Ваш заказ обновлен. ID вашего заказа : <b>'
                . $order->id
                . '</b><br></p>'
                . $orderStr
                . '<p>Если вы зарегистрированы в интернет магазине THEOUTNET.UZ вы можете посмотреть ваш заказ, войдя в свой профиль, инажав на кнопку «Мои заказы».</p>')
                ->setFrom([Yii::$app->params['noreplyEmail'] => Yii::$app->name])
                ->setBcc(Yii::$app->params['bccEmail'])
                ->setTo($order->email)
                ->setSubject('[theoutnet.uz] Обновление заказа! Заказ № ' . $order->id);

            return $message->send();
        } catch (\Swift_SwiftException $exception) {
            return false;
        }
    }
}

<?php

namespace backend\controllers;

use backend\models\location\Districts;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use backend\models\location\Locations;
use backend\models\location\LocationsSearch;

/**
 * LocationController implements the CRUD actions for Locations model.
 */
class LocationController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Locations models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LocationsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Locations model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Locations model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Locations();
        $modelItems = [new Districts()];

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (!empty($modelItems = Yii::$app->request->post('Districts'))) {
                foreach ($modelItems as $item) {
                    $district = new Locations(['location_id' => $model->id, 'type' => Locations::TYPE_DISTRICT]);
                    $district->name = $item['name'];
                    $district->status = $item['status'];
                    $district->save();
                }
            }

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'modelItems' => $modelItems,
            ]);
        }
    }

    /**
     * Updates an existing Locations model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelItems = $model->districts ?: [new Districts()];

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $oldIds = ArrayHelper::map($modelItems, 'id', 'id');
            if (!empty($modelItems = Yii::$app->request->post('Districts'))) {
                if (!empty($deletedIds = array_diff($oldIds, array_filter(ArrayHelper::map($modelItems, 'id', 'id')))))
                    Locations::updateAll(['status' => Locations::STATUS_DELETED], ['id' => $deletedIds]);
                foreach ($modelItems as $item) {
                    $district = !empty($item['id']) ? Locations::findOne($item['id'])
                        : new Locations(['location_id' => $model->id, 'type' => Locations::TYPE_DISTRICT]);
                    $district->name = $item['name'];
                    $district->status = $item['status'];
                    $district->save();
                }
            }

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'modelItems' => $modelItems,
            ]);
        }
    }

    /**
     * Deletes an existing Locations model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->setStatusDeleted();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Locations model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Locations the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Locations::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $email;
    public $subject;
    public $body;
    public $verifyCode;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'email', 'subject', 'body'], 'required'],
            [['body'], 'string', 'max' => 500],
            // email has to be a valid email address
            ['email', 'email'],
            // verifyCode needs to be entered correctly
            ['verifyCode', 'captcha'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('model', 'Имя'),
            'email' => Yii::t('model', 'Эл. адрес'),
            'subject' => Yii::t('model', 'Тема'),
            'body' => Yii::t('model', 'Сообщение'),
            'verifyCode' => Yii::t('model', 'Проверочный код'),
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @return bool whether the email was sent
     */
    public function sendEmail()
    {
        try {
            return Yii::$app->mailer->compose()
                ->setHtmlBody('<p><b>Email:</b> <a href="mailto:' .
                    $this->email . '" class="rcmContactAddress" onclick="return rcmail.command(\'compose\',\'' .
                    $this->email . '\',this)" title="' .
                    $this->email . '">' .
                    $this->email . '</a></p><p><b>Subject:</b>' .
                    $this->subject . '</p><p><b>Message:</b><br>' .
                    $this->body . '</p>')
                ->setFrom([Yii::$app->params['noreplyEmail'] => Yii::$app->name])
                ->setTo(Yii::$app->params['infoEmail'])
                ->setSubject('[theoutnet.uz] ' . $this->subject)
                ->send();
        } catch (\Swift_SwiftException $exception) {
            return false;
        }
    }
}

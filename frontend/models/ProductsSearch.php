<?php

namespace frontend\models;

use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use backend\models\product\Brands;
use backend\models\product\Ribbons;
use backend\models\product\Products;
use backend\models\product\Categories;
use backend\models\product\FilterItems;

/**
 * ProductsSearch represents the model behind the search form about `backend\models\product\Products`.
 */
class ProductsSearch extends Products
{
    public $price_min;
    public $price_max;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['brand_id', 'price', 'views', 'votes', 'category_id', 'price_min', 'price_max'], 'integer'],
            ['discount_id', 'each', 'rule' => ['integer']],
            ['ribbon_ids', 'each', 'rule' => ['safe']],
            ['filter_ids', 'each', 'rule' => ['integer']],
            [['name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Products::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC]
            ],
        ]);

        if (isset($params['brand']) && ($brand = Brands::findOne(['slug' => $params['brand']])) != null)
            $params['brand_id'] = $brand->id;

        if (isset($params['discount']))
            $params['discount_id'] = $params['discount'];

        if (isset($params['ribbon']))
            $params['ribbon_ids'] = $params['ribbon'];

        if (isset($params['filter']))
            $params['filter_ids'] = $params['filter'];

        $this->load(['ProductsSearch' => $params]);
//        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'brand_id' => $this->brand_id,
            'discount_id' => $this->discount_id,
            'views' => $this->views,
            'votes' => $this->votes,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        if (!empty($this->price_min) && !empty($this->price_max))
            $query->andFilterWhere(['between', 'price', ($this->price_min / $this->getDollarRate()), ($this->price_max / $this->getDollarRate())]);

        if ($this->ribbon_ids && !empty($ribbons = Ribbons::findAll(['slug' => $this->ribbon_ids]))) {
            $productIds = [];
            foreach ($ribbons as $ribbon) {
                $productIds = ArrayHelper::merge($productIds, ArrayHelper::getColumn($ribbon->productToRibbons, 'product_id'));
            }

            empty($productIds) ? $query->where('0=1') : $query->andFilterWhere(['id' => $productIds]);
        }

        if ($this->filter_ids && !empty($filters = FilterItems::findAll($this->filter_ids))) {
            $productIds = [];
            foreach ($filters as $filter) {
                $productIds = ArrayHelper::merge($productIds, ArrayHelper::getColumn($filter->productToFilters, 'product_id'));
            }

            empty($productIds) ? $query->where('0=1') : $query->andFilterWhere(['id' => $productIds]);
        }

        if ($this->category_id && ($category = Categories::findOne($this->category_id)) !== null) {
            $productIds = ArrayHelper::getColumn($category->productToCategories, 'product_id');
            if ($category->activeChilds)
                $productIds = $this->mergeCategoryChilds($category->activeChilds, $productIds);

            empty($productIds = array_unique($productIds)) ? $query->where('0=1') : $query->andFilterWhere(['id' => $productIds]);
        }

        $query->andFilterWhere(['status' => [self::STATUS_AVAILABLE, self::STATUS_NOT_AVAILABLE]]);

        return $dataProvider;
    }

    /**
     * Search All
     * @return array
     */
    public function searchAll()
    {
        $models = Products::find();

        if ($this->category_id && ($category = Categories::findOne($this->category_id)) !== null) {
            $productIds = ArrayHelper::getColumn($category->productToCategories, 'product_id');
            if ($category->activeChilds)
                $productIds = $this->mergeCategoryChilds($category->activeChilds, $productIds);

            empty($productIds = array_unique($productIds)) ? $models->where('0=1') : $models->andFilterWhere(['id' => $productIds]);
        }

        $models->andFilterWhere(['status' => [self::STATUS_AVAILABLE, self::STATUS_NOT_AVAILABLE]]);

        return $models->all();
    }

    /**
     * Get Price Min
     * @return mixed
     */
    public function getPriceMin()
    {
        return (!empty($price = ArrayHelper::getColumn($this->searchAll(), 'price'))) ? min($price) * $this->getDollarRate() : null;
    }

    /**
     * Get Price Max
     * @return mixed
     */
    public function getPriceMax()
    {
        return (!empty($price = ArrayHelper::getColumn($this->searchAll(), 'price'))) ? max($price) * $this->getDollarRate() : null;
    }


    /**
     * @param array $childs
     * @param array $array
     * @return array
     */
    protected function mergeCategoryChilds($childs, $array)
    {
        foreach ($childs as $child) {
            /** @var Categories $child */
            $array = ArrayHelper::merge($array, ArrayHelper::getColumn($child->productToCategories, 'product_id'));
            if ($child->activeChilds)
                $array = $this->mergeCategoryChilds($child->activeChilds, $array);
        }

        return $array;
    }
}

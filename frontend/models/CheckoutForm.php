<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\helpers\Html;
use common\models\Settings;
use backend\models\order\Orders;
use backend\models\order\OrderItems;
use backend\models\location\Locations;

/**
 * CheckoutForm is the model behind the checkout form.
 *
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $phone
 * @property integer $district
 * @property string $address
 * @property string $comment
 * @property integer $payment
 * @property integer $step
 * @property boolean $offer
 *
 */
class CheckoutForm extends Model
{
    public $first_name;
    public $last_name;
    public $email;
    public $phone;
    public $district;
    public $address;
    public $comment;
    public $payment;
    public $step;
    public $item_count;
    public $total_price;
    public $offer;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'email', 'phone'], 'required', 'on' => 'step_0'],
            [['first_name', 'last_name', 'email', 'phone', 'district', 'address'], 'required', 'on' => 'step_1'],
            [['first_name', 'last_name', 'email', 'phone', 'district', 'address', 'payment'], 'required', 'on' => 'step_2'],
            [['district', 'payment', 'step'], 'integer'],
            [['first_name', 'last_name', 'phone', 'address'], 'string', 'max' => 255],
            ['email', 'email'],
            [['comment'], 'string', 'max' => 500],
            [['phone'], 'match', 'pattern' => '/^\+998(\d{2})-(\d{3})-(\d{4})$/'],
            ['offer', 'boolean'],
            ['offer', 'required', 'requiredValue' => true, 'on' => 'step_2', 'message' => Yii::t('model', 'Это поле обязательно для заполнения.')],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'first_name' => Yii::t('model', 'Имя'),
            'last_name' => Yii::t('model', 'Фамилия'),
            'email' => Yii::t('model', 'Эл. адрес'),
            'phone' => Yii::t('model', 'Телефон'),
            'district' => Yii::t('model', 'Район'),
            'address' => Yii::t('model', 'Адрес'),
            'comment' => Yii::t('model', 'Примечание для доставки (не более 500 символов)'),
            'payment' => Yii::t('model', 'Способ оплаты'),
            'item_count' => Yii::t('model', 'Количество товаров'),
            'total_price' => Yii::t('model', 'Всего к оплате'),
            'offer' => Yii::t('model', 'Я прочитал и принимаю'),
        ];
    }

    /**
     * Locations List
     * @return array
     */
    public function getLocationsList()
    {
        return Locations::getLocationsList();
    }

    /**
     * Payment List
     * @return array
     */
    public function getPaymentList()
    {
        return Orders::getPaymentArray();
    }

    /**
     * Payment Name
     * @return string
     */
    public function getPaymentName()
    {
        $array = [
            Orders::PAYMENT_CASH => '<span class="text-bold text-blue">' . Orders::getPaymentArray(Orders::PAYMENT_CASH) . '</span>',
            Orders::PAYMENT_TERMINAL => '<span class="text-bold text-blue">' . Orders::getPaymentArray(Orders::PAYMENT_TERMINAL) . '</span>',
            Orders::PAYMENT_COMBINED => '<span class="text-bold text-blue">' . Orders::getPaymentArray(Orders::PAYMENT_COMBINED) . '</span>',
        ];

        return isset($array[$this->payment]) ? $array[$this->payment] : '';
    }

    /**
     * DistrictName
     * @return int|string
     */
    public function getDistrictName()
    {
        return (($district = Locations::findOne($this->district)) !== null) ? $district->name : $this->district;
    }

    /**
     * BasketInfo
     * @param string $idx
     * @return string
     */
    public function getBasketInfo($idx)
    {
        $itemCount = 0;
        $totalPrice = 0;

        if (!empty($basketItems = Basket::getUserBasketItems())) {
            /** @var Basket $item */
            foreach ($basketItems as $item) {
                $itemCount += $item->quantity;
                $totalPrice += $item->quantity * $item->product->getPriceBySum();
            }
        }

        $array = [
            'item_count' => $itemCount,
            'total_price' => Yii::$app->formatter->asDecimal($totalPrice, 0) . Yii::t('model', '&nbsp;<i>сум</i>')
        ];

        return isset($array[$idx]) ? $array[$idx] : '';
    }

    /**
     * Checkout.
     * @return bool
     */
    public function checkout()
    {
        if ($this->validate()) {
            $session = Yii::$app->session;
            if (!$session->isActive) $session->open();

            $order = new Orders([
                'user_id' => Yii::$app->user->isGuest ? 0 : Yii::$app->user->id,
                'session_id' => $session->id,
                'first_name' => $this->first_name,
                'last_name' => $this->last_name,
                'email' => $this->email,
                'phone' => $this->phone,
                'location_id' => $this->district,
                'address' => $this->address,
                'comment' => $this->comment,
                'payment' => $this->payment,
            ]);

            if ($order->save()) {
                if (!empty($basketItems = Basket::getUserBasketItems())) {
                    /** @var Basket $item */
                    foreach ($basketItems as $item) {
                        $orderItem = new OrderItems([
                            'order_id' => $order->id,
                            'product_id' => $item->product_id,
                            'quantity' => $item->quantity,
                            'price' => $item->product->getPriceBySum(),
                            'total_price' => $item->quantity * $item->product->getPriceBySum()
                        ]);

                        if ($orderItem->save())
                            $item->delete();
                    }

                    Yii::$app->session->setFlash('success', ($this->sendEmail($order)) ?
                        'Ваш заказ оформлен. На указанный вами электронный адрес отправлено письмо с подробной информацией. ID вашего заказа : ' . $order->id :
                        'Ваш заказ оформлен! ID вашего заказа ' . $order->id);

                    return true;
                } else {
                    $order->delete();
                }
            }
        }

        return false;
    }

    /**
     * Order send email.
     * @param Orders $order
     * @return bool
     */
    public function sendEmail($order)
    {
        try {
            $message = Yii::$app->mailer->compose();
            $webroot = Yii::getAlias('@rootDir');
            $logo = $message->embed($webroot . Settings::getLogoValue());
            $orderStr = '';

            if (!empty($order->orderItems)) {
                $totalQuantity = 0;
                $totalPrice = 0;
                $orderItems = '';
                $border = 'border:1px solid #D6D4D4;padding:10px;';

                foreach ($order->orderItems as $item) {
                    $totalQuantity += $item->quantity;
                    $totalPrice += $item->total_price;

                    $orderItems .= '<tr><td style="' . $border . '">'
                        . $item->product->name
                        . '</td><td style="' . $border . '">'
                        . Yii::$app->formatter->asDecimal($item->price, 0)
                        . '</td><td style="white-space: nowrap;' . $border . '">'
                        . $item->quantity
                        . '</td><td style="white-space: nowrap;' . $border . '">'
                        . Yii::$app->formatter->asDecimal($item->total_price, 0)
                        . '&nbsp;<i>сум</i></td></tr>';
                }

                $orderStr = '<table style="text-align:center;font-size:14px;width:100%;border-collapse:collapse">'
                    . '<thead><tr>'
                    . '<th style="width:55%;' . $border . '">Наименование</th>'
                    . '<th style="width:15%;' . $border . '">Цена</th>'
                    . '<th style="width:15%;' . $border . '">Количество</th>'
                    . '<th style="width:15%;' . $border . '">Сумма</th>'
                    . '</tr></thead><tbody>'
                    . $orderItems
                    . '<tr><th colspan="2" style="' . $border . '">Общий</th>'
                    . '<th style="' . $border . '">'
                    . $totalQuantity
                    . '</th><th style="white-space: nowrap;' . $border . '">'
                    . Yii::$app->formatter->asDecimal($totalPrice, 0)
                    . '&nbsp;<i>сум</i></th></tr></tbody></table>';
            }

            $message->setHtmlBody('<p align="center">'
                . Html::a(Html::img($logo, ['alt' => 'logo', 'width' => '200px']), Yii::$app->request->hostInfo, ['target' => '_blank'])
                . '</p><h2>Здравствуйте, '
                . $order->first_name
                . '&nbsp;'
                . $order->last_name
                . '!</h2><p>Благодарим за покупку в интернет магазине THEOUTNET.UZ.</p><p>Ваш заказ принят и ожидает подтверждения. ID вашего заказа : <b>'
                . $order->id
                . '</b><br></p>'
                . $orderStr
                . '<p>Если вы зарегистрированы в интернет магазине THEOUTNET.UZ вы можете посмотреть ваш заказ, войдя в свой профиль, инажав на кнопку «Мои заказы».</p>')
                ->setFrom([Yii::$app->params['noreplyEmail'] => Yii::$app->name])
                ->setBcc(Yii::$app->params['bccEmail'])
                ->setTo($order->email)
                ->setSubject('[theoutnet.uz] Подтверждение заказа! Заказ № ' . $order->id);

            return $message->send();
        } catch (\Swift_SwiftException $exception) {
            return false;
        }
    }
}

<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use backend\models\post\Posts;
use backend\models\post\PostCategories;

class PostController extends Controller
{
    /**
     * Post Category View
     * @param $slug
     * @return mixed
     */
    public function actionCategory($slug)
    {
        return $this->render('category', [
            'model' => $this->findPostCategory($slug)
        ]);
    }

    /**
     * Post View
     * @param $slug
     * @return mixed
     */
    public function actionView($slug)
    {
        return $this->render('view', [
            'model' => $this->findPost($slug)
        ]);
    }

    /**
     * Find PostCategories model.
     * @param string $slug
     * @return PostCategories the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findPostCategory($slug)
    {
        if (($model = PostCategories::findOne(['slug' => $slug, 'status' => PostCategories::STATUS_ACTIVE])) !== null)
            return $model;
        else
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
    }

    /**
     * Find Posts model.
     * @param string $slug
     * @return Posts the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findPost($slug)
    {
        if (($model = Posts::findOne(['slug' => $slug, 'status' => Posts::STATUS_ACTIVE])) !== null) {
            $model->views += 1;
            $model->save();

            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

}

<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use common\models\Settings;
use backend\models\page\Pages;
use frontend\models\ContactForm;
use yii\web\NotFoundHttpException;

class PageController extends Controller
{
    /**
     * Displays about page.
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about', [
            'model' => $this->findPage('about'),
            'logo' => Yii::$app->request->hostInfo . Settings::getLogoValue(),
        ]);
    }

    /**
     * Displays contact page.
     * @return mixed
     */
    public function actionContact()
    {
        $modelForm = new ContactForm();
        if ($modelForm->load(Yii::$app->request->post()) && $modelForm->validate()) {
            if ($modelForm->sendEmail())
                Yii::$app->session->setFlash('success', Yii::t('frontend', 'Благодарим Вас за обращение к нам. Мы ответим вам как можно скорее.'));
            else
                Yii::$app->session->setFlash('error', Yii::t('frontend', 'При отправке вашего сообщения произошла ошибка.'));

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'modelForm' => $modelForm,
                'model' => $this->findPage('contact'),
                'logo' => Yii::$app->request->hostInfo . Settings::getLogoValue(),
            ]);
        }
    }

    /**
     * Displays Page model.
     * @param string $slug
     * @return mixed
     */
    public function actionView($slug)
    {
        return $this->render('view', [
            'model' => $this->findPage($slug),
            'logo' => Yii::$app->request->hostInfo . Settings::getLogoValue(),
        ]);
    }

    /**
     * Find Pages model.
     * @param string $url
     * @return Pages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findPage($url)
    {
        if (($model = Pages::findOne(['url' => $url, 'status' => Pages::STATUS_ACTIVE])) !== null)
            return $model;
        else
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
    }
}

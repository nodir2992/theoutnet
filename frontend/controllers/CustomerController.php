<?php

namespace frontend\controllers;

use common\models\Settings;
use Yii;
use yii\web\Response;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use common\models\User;
use frontend\models\Basket;
use frontend\models\Account;
use common\models\LoginForm;
use frontend\models\SignupForm;
use backend\models\order\Orders;
use frontend\models\CheckoutForm;
use backend\models\product\Products;
use backend\models\location\Locations;
use frontend\models\ResetPasswordForm;
use frontend\models\AccountUpdateForm;
use frontend\models\PasswordResetRequestForm;

class CustomerController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup', 'account', 'account-update', 'orders', 'order-view'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout', 'account', 'account-update', 'orders', 'order-view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Logs in a user.
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest)
            return $this->goHome();

        $session = Yii::$app->session;
        if (!$session->isActive) $session->open();
        $sessionId = $session->id;
        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $model->setUserIdBasketItems($sessionId);
            return $this->goBack('account');
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }

    /**
     * Signs user up.
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                $session = Yii::$app->session;
                if (!$session->isActive) $session->open();
                Basket::updateAll(['user_id' => $user->id], ['session_id' => $session->id]);

                if ($model->sendEmail($user))
                    Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                else
                    Yii::$app->session->setFlash('error', 'Sorry, we are unable to confirm email for the provided email address.');

                return $this->goHome();
            }
        }

        return $this->render('signup', [
            'model' => $model,
            'offerText' => Settings::getSettingValue('offer_text'),
        ]);
    }

    /**
     * Signup with Email Confirmation.
     * @param integer $id
     * @param string $key
     * @return mixed
     */
    public function actionConfirmEmail($id, $key)
    {
        if (($user = User::findOne(['id' => $id, 'auth_key' => $key, 'status' => User::STATUS_INACTIVE])) !== null) {
            $user->status = User::STATUS_ACTIVE;
            $user->generateAuthKey();

            if ($user->save()) {
                Yii::$app->getUser()->login($user);
                Yii::$app->getSession()->setFlash('success', 'Congratulations! You have successfully activated your account.');
            }
        } else {
            Yii::$app->getSession()->setFlash('error', 'Sorry, we cannot activate your account.');
        }

        return $this->goHome();
    }

    /**
     * Requests password reset.
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');
            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * Display basket page
     * @return mixed
     */
    public function actionBasket()
    {
        if ($post = Yii::$app->request->post()) {
            if (!empty($post['product'])) {
                foreach ($post['product'] as $key => $value) {
                    /** @var $model Basket */
                    if (($model = $this->getBasketItem($key)) !== null) {
                        $model->quantity = $value;
                        $model->save();
                    }
                }
            }

            $this->refresh();
        }
        return $this->render('basket', [
            'models' => Basket::getUserBasketItems()
        ]);
    }

    /**
     * Display checkout page
     * @return mixed
     */
    public function actionCheckout()
    {
        if (empty(Basket::getUserBasketItems()))
            return $this->redirect('basket');

        if (Yii::$app->user->isGuest) {
            $checkoutForm = new CheckoutForm(['scenario' => 'step_0', 'step' => 0]);
            $loginForm = new LoginForm();
            $session = Yii::$app->session;
            $sessionId = $session->id;

            if ($loginForm->load(Yii::$app->request->post()) && $loginForm->login()) {
                $loginForm->setUserIdBasketItems($sessionId);
                return $this->refresh();
            }
        } else {
            $loginForm = null;
            $user = User::findOne(Yii::$app->user->id);
            $checkoutForm = new CheckoutForm([
                'scenario' => 'step_0',
                'step' => 0,
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'email' => $user->email,
                'phone' => $user->phone,
                'district' => $user->location_id,
                'address' => $user->address,
            ]);
        }

        if ($checkoutForm->load(Yii::$app->request->post())) {
            switch ($checkoutForm->step) {
                case 0:
                    $checkoutForm->step = 1;
                    $checkoutForm->scenario = 'step_1';
                    break;
                case 1:
                    $checkoutForm->step = 2;
                    $checkoutForm->scenario = 'step_2';
                    break;
                case 2:
                    $checkoutForm->step = 3;
                    $checkoutForm->scenario = 'step_2';
                    break;
                case 3:
                    if ($checkoutForm->checkout())
                        return $this->goHome();
                    break;
            }
        }

        return $this->render('checkout', [
            'checkoutForm' => $checkoutForm,
            'loginForm' => $loginForm,
            'offerText' => Settings::getSettingValue('offer_text'),
        ]);
    }

    /**
     * Add To Cart.
     * @return mixed
     */
    public function actionAddToCart()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if ($post = Yii::$app->request->post()) {
            if (($product = Products::findOne(['id' => $post['product'], 'status' => Products::STATUS_AVAILABLE])) !== null) {
                $session = Yii::$app->session;
                if (!$session->isActive) $session->open();
                $sessionId = $session->id;
                $condition = ['product_id' => $product->id];

                if (Yii::$app->user->isGuest) {
                    $userId = 0;
                    $condition = ArrayHelper::merge($condition, ['session_id' => $sessionId]);
                } else {
                    $userId = Yii::$app->user->id;
                    $condition = ArrayHelper::merge($condition, ['user_id' => $userId]);
                }

                $model = Basket::findOne($condition) ?:
                    new Basket([
                        'user_id' => $userId,
                        'session_id' => $sessionId,
                        'product_id' => $product->id,
                    ]);

                $model->quantity += 1;
                if ($model->save())
                    return true;
            }
        }

        return false;
    }

    /**
     * Delete Basket Item
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteItem($id)
    {
        if (($model = $this->getBasketItem($id)) !== null)
            $model->delete();

        return $this->redirect('basket');
    }

    /**
     * Customer Account
     * @return mixed
     */
    public function actionAccount()
    {
        $user = User::findOne(Yii::$app->user->id);
        $model = new Account([
            'firstName' => $user->first_name,
            'lastName' => $user->last_name,
            'email' => $user->email,
            'phone' => $user->phone,
            'image' => $user->image,
            'district' => (($location = Locations::findOne($user->location_id)) !== null) ? $location->name : '',
            'address' => $user->address,
        ]);

        return $this->render('account', [
            'model' => $model,
        ]);
    }

    /**
     * Customer Account Update
     * @return mixed
     */
    public function actionAccountUpdate()
    {
        $user = User::findOne(Yii::$app->user->id);
        $model = new AccountUpdateForm([
            'email' => $user->email,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'image' => $user->image,
            'phone' => $user->phone,
            'location_id' => $user->location_id,
            'address' => $user->address,
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->update($user->id)) {
            return $this->redirect('account');
        }

        return $this->render('accountUpdate', [
            'model' => $model,
        ]);
    }


    /**
     * Customer Orders
     * @return mixed
     */
    public function actionOrders()
    {
        $orders = new ActiveDataProvider([
            'query' => Orders::find()->where(['user_id' => Yii::$app->user->id])
                ->andFilterWhere(['!=', 'status', Orders::STATUS_DELETED])
                ->andFilterWhere(['!=', 'status', Orders::STATUS_FALSE]),
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC]
            ]
        ]);

        return $this->render('orders', [
            'orders' => $orders,
        ]);
    }

    /**
     * Order View
     * @param $id
     * @return mixed
     */
    public function actionOrderView($id)
    {
        $model = $this->findModelOrder($id);

        return (Yii::$app->request->isAjax) ?
            $this->renderAjax('orderView', [
                'model' => $model,
                'isAjax' => true
            ]) :
            $this->render('orderView', [
                'model' => $model,
                'isAjax' => false
            ]);
    }

    /**
     * Finds the Orders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Orders the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelOrder($id)
    {
        if (($model = Orders::findOne(['id' => $id, 'user_id' => Yii::$app->user->id])) !== null)
            return $model;
        else
            throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Get Basket Item
     * @param integer $id
     * @return Basket | null
     */
    protected function getBasketItem($id)
    {
        $session = Yii::$app->session;
        if (!$session->isActive) $session->open();
        $condition = ArrayHelper::merge(['product_id' => $id], Yii::$app->user->isGuest ?
            ['session_id' => $session->id] :
            ['user_id' => Yii::$app->user->id]);

        return Basket::findOne($condition);
    }
}

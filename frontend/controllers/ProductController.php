<?php

namespace frontend\controllers;

use Yii;
use yii\web\Response;
use yii\helpers\Json;
use yii\web\Controller;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use common\models\LoginForm;
use frontend\models\CommentForm;
use frontend\models\ProductsSearch;
use backend\models\comment\Comments;
use backend\models\product\Brands;
use backend\models\product\Ribbons;
use backend\models\product\Products;
use backend\models\product\Discounts;
use backend\models\product\Categories;
use backend\models\product\FilterItems;
use backend\models\product\ProductToFilter;
use backend\models\product\ProductToRibbon;

class ProductController extends Controller
{
    /**
     * Product Category Page.
     * @param string $slug
     * @return mixed
     */
    public function actionCategory($slug)
    {
        $category = ($slug == 'all') ? null : $this->findModelCategory($slug);

        $searchModel = new ProductsSearch();
        if ($category != null)
            $searchModel->category_id = $category->id;

        $queryParams = Yii::$app->request->queryParams;

        $dataProvider = $searchModel->search($queryParams);
        $models = $searchModel->searchAll();

        $brandName = '';
        if (isset($queryParams['brand']) && ($brand = Brands::findOne(['slug' => $queryParams['brand']])) != null)
            $brandName = ' | ' . $brand->name;

        $categories = Categories::find()
            ->where([
                'status' => Categories::STATUS_ACTIVE,
                'parent_id' => ($category == null) ? null : $category->parent_id
            ])
            ->orderBy(['weight' => SORT_ASC])
            ->all();

        $brands = Brands::find()
            ->where([
                'status' => Brands::STATUS_ACTIVE,
                'id' => array_unique(ArrayHelper::getColumn($models, 'brand_id')),
            ])
            ->orderBy(['weight' => SORT_ASC])
            ->limit(10)
            ->all();

        $discounts = ArrayHelper::map(Discounts::find()
            ->where([
                'status' => Discounts::STATUS_ACTIVE,
                'id' => array_unique(ArrayHelper::getColumn($models, 'discount_id'))
            ])
            ->orderBy(['percentage' => SORT_ASC])
            ->all(), 'id', 'name');

        $priceRange = [
            'min' => $searchModel->getPriceMin(),
            'max' => $searchModel->getPriceMax()
        ];

        $ribbons = ArrayHelper::map(Ribbons::findAll([
            'status' => Ribbons::STATUS_ACTIVE,
            'id' => array_unique(ArrayHelper::getColumn(ProductToRibbon::findAll([
                'product_id' => array_unique(ArrayHelper::getColumn($models, 'id'))
            ]), 'ribbon_id'))
        ]), 'slug', 'name');

        $filters = [];

        if ($category != null && empty($category->activeChilds)) {
            $filterItems = FilterItems::findAll([
                'id' => array_unique(ArrayHelper::getColumn(ProductToFilter::findAll([
                    'product_id' => array_unique(ArrayHelper::getColumn($models, 'id'))
                ]), 'filter_item_id'))
            ]);
            if (!empty($filterItems)) {
                foreach ($filterItems as $item) {
                    /** @var FilterItems $item */
                    $filters[$item->filter->name] = isset($filters[$item->filter->name]) ?
                        ArrayHelper::merge($filters[$item->filter->name], [$item->id => $item->name]) :
                        [$item->id => $item->name];
                }
            }
        }

        return $this->render('category', [
            'category' => $category,
            'dataProvider' => $dataProvider,
            'categories' => $categories,
            'brands' => $brands,
            'discounts' => $discounts,
            'pagination' => $dataProvider->pagination,
            'priceRange' => $priceRange,
            'ribbons' => $ribbons,
            'filters' => $filters,
            'brandName' => $brandName,
            'quantityText' => $this->renderQuantityText($dataProvider),
        ]);
    }

    /**
     * Product View Page.
     * @param string $slug
     * @return mixed
     */
    public function actionView($slug)
    {
        $model = $this->findModelProduct($slug);
        $products = Products::find()->where([
            'group_id' => $model->group_id,
            'status' => [Products::STATUS_AVAILABLE, Products::STATUS_NOT_AVAILABLE]
        ])->limit(10)->all();

        $loginForm = new LoginForm();
        $commentForm = new CommentForm([
            'model' => $model::tableName(),
            'model_id' => $model->id,
        ]);

        if (Yii::$app->user->isGuest) {
            $session = Yii::$app->session;
            $sessionId = $session->id;
            if ($loginForm->load(Yii::$app->request->post()) && $loginForm->login()) {
                $loginForm->setUserIdBasketItems($sessionId);
                return $this->refresh();
            }
        } else {
            if ($commentForm->load(Yii::$app->request->post()) && $commentForm->commentSave())
                return $this->refresh();
        }

        return $this->render('view', [
            'model' => $model,
            'products' => $products,
            'loginForm' => $loginForm,
            'commentForm' => $commentForm,
            'dataProvider' => $model->getCommentsDataProvider(),
        ]);
    }

    /**
     * Find Categories model.
     * @param string $slug
     * @return Categories the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelCategory($slug)
    {
        if (($model = Categories::findOne(['slug' => $slug, 'status' => Categories::STATUS_ACTIVE])) !== null)
            return $model;
        else
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
    }

    /**
     * Find Products model.
     * @param string $slug
     * @return Products the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelProduct($slug)
    {
        if (($model = Products::findOne(['slug' => $slug, 'status' => [Products::STATUS_AVAILABLE, Products::STATUS_NOT_AVAILABLE]])) !== null) {
            $model->views += 1;
            $model->save();

            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

    /**
     * Displays Search Page.
     * @return mixed
     */
    public function actionSearch()
    {
        $searchModel = new ProductsSearch();
        $text = '';

        if ($get = Yii::$app->request->get()) {
            if (strlen($text = strip_tags(trim($get['text']))) >= 2)
                $searchModel->name = $text;
            else
                $searchModel->brand_id = 'brand';
        }
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort->defaultOrder = ['name' => SORT_ASC];

        return $this->render('search', [
            'dataProvider' => $dataProvider,
            'text' => $text,
            'pagination' => $dataProvider->pagination,
        ]);
    }

    /**
     * Ajax Product List.
     * @param string $q
     */
    public function actionProductList($q = null)
    {
        $out = [];

        $models = Products::find()
            ->where(['status' => [Products::STATUS_AVAILABLE, Products::STATUS_NOT_AVAILABLE]])
            ->andWhere(['LIKE', 'name', $q])
            ->orderBy('name')
            ->limit(1000)
            ->all();

        /** @var $model Products */
        foreach ($models as $model) {
            $out[] = [
                'key' => $model->slug,
                'value' => $model->name
            ];
        }

        echo Json::encode($out);
    }

    /**
     * @return bool
     */
    public function actionCommentLike()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if ($post = Yii::$app->request->post()) {
            if ($model = Comments::findOne((int)$post['item'])) {
                $session = Yii::$app->session;
                if (!$session->isActive) $session->open();
                $sessionId = Yii::$app->user->isGuest ? $session->id : Yii::$app->user->id;
                $sessionArray = empty($model->sessions) ? [] : explode(' ', $model->sessions);
                array_push($sessionArray, $sessionId);
                $model->sessions = implode(' ', $sessionArray);
                (int)$post['value'] ? $model->likes += 1 : $model->unlikes += 1;

                if ($model->save())
                    return true;
            }
        }
        return false;
    }

    /**
     * Render Quantity Text.
     * @param ActiveDataProvider $dataProvider
     * @return string
     */
    protected
    function renderQuantityText($dataProvider)
    {
        if ((($count = $dataProvider->getCount()) > 1) && ($pagination = $dataProvider->getPagination()) !== false) {
            $totalCount = $dataProvider->getTotalCount();
            $begin = $pagination->getPage() * $pagination->pageSize + 1;
            $end = $begin + $count - 1;
            if ($begin > $end) $begin = $end;
            $page = $pagination->getPage() + 1;
            $pageCount = $pagination->pageCount;

            return Yii::t('frontend', 'Товары <b>{begin, number}—{end, number}</b> из <b>{totalCount, number}</b>', [
                'begin' => $begin,
                'end' => $end,
                'count' => $count,
                'totalCount' => $totalCount,
                'page' => $page,
                'pageCount' => $pageCount,
            ]);
        }

        return '';
    }
}

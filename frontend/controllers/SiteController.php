<?php

namespace frontend\controllers;

use backend\models\sliders\Sliders;
use Yii;
use yii\web\Controller;
use common\models\Settings;
use backend\models\post\Posts;
use backend\models\product\Brands;
use backend\models\product\Ribbons;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index', [
            'title' => Settings::getSiteNameValue(),
            'logo' => Yii::$app->request->hostInfo . Settings::getLogoValue(),
            'slider' => $slider = Sliders::findOne(['key' => 'home_slider', 'status' => Sliders::STATUS_ACTIVE]),
            'hurry' => Ribbons::findOne(['key' => 'hurry', 'status' => Ribbons::STATUS_ACTIVE]),
            'forFamily' => Ribbons::findOne(['key' => 'for_family', 'status' => Ribbons::STATUS_ACTIVE]),
            'manChoice' => Ribbons::findOne(['key' => 'man_choice', 'status' => Ribbons::STATUS_ACTIVE]),
            'womenThings' => Ribbons::findOne(['key' => 'women_things', 'status' => Ribbons::STATUS_ACTIVE]),
            'metaKeywords' => $this->getSettingValue('home_page_meta_keywords'),
            'metaDescription' => $this->getSettingValue('home_page_meta_description'),
            'footerText' => $this->getSettingValue('home_page_footer_text'),
            'posts' => Posts::find()->where(['status' => Posts::STATUS_ACTIVE])->orderBy(['id' => SORT_DESC])->limit(2)->all(),
            'brands' => Brands::findAll(['status' => Brands::STATUS_ACTIVE]),
        ]);
    }

    /**
     * Find Settings model.
     * @param string $key
     * @return string|null
     */
    protected function getSettingValue($key)
    {
        return (($model = Settings::findOne(['key' => $key])) !== null) ? $model->value : null;
    }
}

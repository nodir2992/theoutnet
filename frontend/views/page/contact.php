<?php

use yii\helpers\Html;
use yii\captcha\Captcha;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $modelForm \frontend\models\ContactForm */
/* @var $model \backend\models\page\Pages */
/* @var $logo string */

$this->title = $model->name;
$this->params['breadcrumbs'][] = $this->title;

$this->params['meta_type'] = 'page';
$this->params['meta_url'] = Yii::$app->request->hostInfo . '/page/' . $model->url;
$this->params['meta_image'] = $logo;
if ($model->meta_keywords)
    $this->params['meta_keywords'] = $model->meta_keywords;
if ($model->meta_description)
    $this->params['meta_description'] = $model->meta_description;
?>
<div class="deals page-contact">
    <h3 class="w3ls-title title-center"><?= Html::encode($model->name) ?></h3>

    <div class="row">
        <div class="col-md-6">
            <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

            <?= $form->field($modelForm, 'name') ?>

            <?= $form->field($modelForm, 'email') ?>

            <?= $form->field($modelForm, 'subject') ?>

            <?= $form->field($modelForm, 'body')->textarea(['rows' => 6]) ?>

            <?= $form->field($modelForm, 'verifyCode')->widget(Captcha::className(), [
                'template' => '<div class="row"><div class="col-md-6">{input}</div></div>{image}<button type="button" id="captcha_refresh" class="btn btn-default"><i class="glyphicon glyphicon-refresh"></i></button>',
                'imageOptions' => ['id' => 'captcha_image']
            ]) ?>
            <?php $this->registerJs("jQuery('#captcha_refresh').on('click', function(e){
                    e.preventDefault(); jQuery('#captcha_image').yiiCaptcha('refresh'); })") ?>
            <div class="form-group">
                <?= Html::submitButton(Yii::t('frontend', 'Отправить'), ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
        <div class="col-md-6">
            <?= $model->body ?>
        </div>
    </div>
</div>

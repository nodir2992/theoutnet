<?php

use kartik\rating\StarRating;
use rmrevin\yii\fontawesome\FA;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \backend\models\comment\Comments */

?>

<div class="well well-sm">
    <h4><?= $model->author->first_name . '&nbsp;' . $model->author->last_name ?></h4>
    <p><?= $model->text ?></p>
    <ul class="list-inline">
        <li>
            <?= Fa::i(FA::_CALENDAR) . '&nbsp;' . Yii::$app->formatter->asDate($model->created_at, 'dd-MM-Y') ?>
        </li>
        <li>|</li>
        <li>
            <?= StarRating::widget([
                'name' => 'rating_' . $model->id,
                'value' => $model->rating,
                'pluginOptions' => ['displayOnly' => true]
            ]) ?>
        </li>
        <li>|</li>
        <li>
            <?= Yii::t('frontend', 'Отзыв полезен?')
            . '&nbsp;'
            . Html::button(FA::icon('thumbs-o-up') . '&nbsp;' . $model->likes, [
                'class' => 'like btn btn-xs btn-success',
                'disabled' => $model->getBtnDisabled(),
                'data-item' => $model->id,
                'data-like' => 1
            ])
            . '&nbsp;'
            . Html::button(FA::icon('thumbs-o-down') . '&nbsp;' . $model->unlikes, [
                'class' => 'like btn btn-xs btn-danger',
                'disabled' => $model->getBtnDisabled(),
                'data-item' => $model->id,
                'data-like' => 0
            ]) ?>
        </li>
    </ul>
</div>

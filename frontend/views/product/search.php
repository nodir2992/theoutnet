<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\LinkPager;
use kartik\typeahead\Typeahead;

/** @var $this yii\web\View */
/** @var $dataProvider \yii\data\ActiveDataProvider */
/** @var $model \backend\models\product\Products */
/** @var $pagination \yii\data\Pagination */
/** @var $text string */

$this->title = Yii::t('frontend', 'Результат поиска');

$this->params['breadcrumbs'][] = ['label' => 'Все товары', 'url' => ['category', 'slug' => 'all']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="products-search">

    <div class="row">
        <div class="col-md-8 col-lg-6">
            <?= Html::beginForm(['product/search'], 'get', ['class' => 'search-form']) ?>
            <div class="input-group">
                <?= Typeahead::widget([
                    'name' => 'text',
                    'options' => [
                        'placeholder' => 'Поиск',
                        'required' => true,
                        'type' => 'search',
                        'style' => 'border-radius: 0;',
                    ],
                    'value' => !empty($text) ? $text : false,
                    'scrollable' => true,
                    'pluginOptions' => [
                        'highlight' => true,
                        'minLength' => 2,
                    ],
                    'dataset' => [
                        [
                            'display' => 'value',
                            'limit' => 1000,
                            'remote' => [
                                'url' => Url::to(['product/product-list']) . '?q=%QUERY',
                                'wildcard' => '%QUERY'
                            ]
                        ]
                    ],
                    'pluginEvents' => [
                        'typeahead:select' => 'function(event, data) {
                            location.href = "/product/view/" + data["key"];
                        }',
                    ]
                ]) ?>
                <div class="input-group-btn">
                    <?= Html::submitButton(' <i class="fa fa-search"></i>', ['class' => 'btn btn-default form-btn']) ?>
                </div>
            </div>
            <?= Html::endForm() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <hr>
            <h3 class="w3ls-title"><?= $this->title ?></h3>
            <p>
                <?= ($dataProvider->totalCount > 0) ? '<strong>Найдено:</strong> ' . $dataProvider->totalCount : 'Результатов не найдено' ?>
            </p>
            <br>
        </div>
    </div>

    <?php if (!empty($dataProvider->models)) { ?>
        <ul class="list-group">
            <?php foreach ($dataProvider->models as $model) { ?>
                <li class="list-group-item">
                    <?= Html::a(Html::img($model->getDefaultImageUrl(), ['alt' => 'product'])
                        . '<span>' . str_ireplace(mb_strtolower($text), '<b>' . $text . '</b>', mb_strtolower($model->name)) . '</span>',
                        ['view', 'slug' => $model->slug]) ?>
                </li>
            <?php } ?>
        </ul>
    <?php } ?>

    <div class="clearfix"></div>
    <div class="products-pager">
        <?= LinkPager::widget(['pagination' => $pagination]) ?>
    </div>

    <div class="clearfix"></div>
</div>

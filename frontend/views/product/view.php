<?php

use yii\widgets\Pjax;
use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\LinkPager;
use yii\bootstrap\ActiveForm;
use russ666\widgets\Countdown;
use kartik\rating\StarRating;

/* @var $this yii\web\View */
/* @var $model \backend\models\product\Products */
/* @var $products array */
/* @var $commentForm \backend\models\comment\Comments */
/* @var $loginForm \common\models\LoginForm */
/* @var $dataProvider \yii\data\ActiveDataProvider */

\frontend\assets\SmoothAsset::register($this);
$this->title = $model->meta_title;

$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Все товары'), 'url' => ['category', 'slug' => 'all']];
if (isset($model->categories[0])) {
    if (($parent = $model->categories[0]->parent) != null) {
        if ($parent->parent != null)
            $this->params['breadcrumbs'][] = ['label' => $parent->parent->name, 'url' => ['category', 'slug' => $parent->parent->slug]];
        $this->params['breadcrumbs'][] = ['label' => $parent->name, 'url' => ['category', 'slug' => $parent->slug]];
    }
    $this->params['breadcrumbs'][] = ['label' => $model->categories[0]->name, 'url' => ['category', 'slug' => $model->categories[0]->slug]];
}
$this->params['breadcrumbs'][] = $model->name;

$this->params['meta_type'] = 'product';
$this->params['meta_url'] = Yii::$app->request->hostInfo . '/product/view/' . $model->slug;
if ($model->meta_keywords)
    $this->params['meta_keywords'] = $model->meta_keywords;
if ($model->meta_description)
    $this->params['meta_description'] = $model->meta_description;
if (!empty($model->getDefaultImageUrl()))
    $this->params['meta_image'] = Yii::$app->request->hostInfo . $model->getDefaultImageUrl();
?>

<!-- products -->
<div class="products">
    <div class="container">
        <div class="single-page">
            <div class="single-page-row">
                <div class="col-sm-6 col-md-5 col-lg-4">
                    <div class="sp-loading">
                        <div class="img"></div>
                        <br><?= Yii::t('frontend', 'Загрузка ...') ?>
                    </div>
                    <div class="sp-wrap">
                        <?php if (!empty($images = $model->productImages)) {
                            foreach ($images as $image) {
                                echo "<a href='$image->path'><img src='$image->path' alt='$model->name'></a>";
                            }
                        } ?>
                    </div>
                    <div class="clearfix"></div>
                    <?php $this->registerJs('$(".sp-wrap").smoothproducts();'); ?>
                </div>
                <div class="col-sm-6 single-top-right">
                    <div class="row">
                        <div class="col-sm-6">
                            <p>
                                <br>
                                <b><?= Yii::t('frontend', 'Код № ') ?></b><?= $model->barcode ?>
                            </p>
                        </div>
                        <div class="col-sm-6" align="center">
                            <?= Html::a(Html::img($model->brand->image, ['alt' => $model->brand->name, 'class' => 'brand-logo']),
                                $model->brand->link, ['target' => '_blank']) ?>
                        </div>
                    </div>
                    <hr>
                    <h3 class="item_name"><?= $model->name ?></h3>
                    <hr>
                    <div class="clearfix"></div>
                    <div class="single-price">
                        <ul>
                            <li><?= $model->getPriceAsCurrency() ?></li>
                            <li>
                                <del><?= $model->getOldPriceAsCurrency() ?></del>
                            </li>
                            <li>
                                <span class="w3off"><?= $model->discount_id ? $model->discount->name : '' ?></span>
                            </li>
                        </ul>
                    </div>
                    <hr>
                    <?php
                    if (!empty($model->activeStickers)) {
                        echo '<p align="center">';
                        foreach ($model->activeStickers as $sticker) {
                            echo Html::a(Html::img($sticker->image, ['alt' => 'sticker', 'class' => 'img-responsive']),
                                [$sticker->link], ['title' => $sticker->name, 'target' => '_blank']);
                        }
                        echo '</p><hr>';
                    }

                    if ($model->timer_end && $model->timer_start && ($model->timer_start < time()) && ($model->timer_end > $model->timer_start)) {
                        echo '<div class="hurry-timer"><span class="icon"><i class="fa fa-clock-o"></i></span>'
                            . Countdown::widget([
                                'datetime' => date('Y-m-d H:i:s', $model->timer_end),
                                'format' => '<span><b>%-D</b><small>'
                                    . Yii::t('mp_app', ' дней')
                                    . '</small></span><span><b>| %H</b><small>'
                                    . Yii::t('mp_app', ' часов')
                                    . '</small></span><span><b>: %M</b><small>'
                                    . Yii::t('mp_app', ' минут')
                                    . '</small></span><span><b>: %S</b><small>'
                                    . Yii::t('mp_app', ' секунд')
                                    . '</small></span>',
                                'options' => [
                                    'class' => 'count-down'
                                ],
                                'events' => [
                                    'finish' => 'function(){location.reload()}',
                                ],
                            ]) . '<span class="count"><b>'
                            . $model->quantity
                            . '</b><small>'
                            . Yii::t('mp_app', 'шт.')
                            . '</small></span></div><hr>';
                    } ?>

                    <p class="single-price-text"><?= $model->detail ?></p>
                    <br>
                    <div class="clearfix"></div>
                    <?= $model->isAvailable() ? Html::button('<i class="fa fa-cart-plus"></i>&nbsp;'
                        . Yii::t('frontend', 'В корзину'),
                        ['class' => 'add-to-cart w3ls-cart', 'data-product' => $model->id]) :
                        '<p class="product-not-available">' . Yii::t('frontend', 'Нет в наличии') . '</p>' ?>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <!-- collapse-tabs -->
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="collpse tabs">
                    <h3 class="w3ls-title"><?= Yii::t('frontend', 'Об этом товаре') ?></h3>
                    <div class="panel-group collpse" id="accordion" role="tablist" aria-multiselectable="true">
                        <?php if ($model->description) { ?>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title">
                                        <a class="pa_italic" role="button" data-toggle="collapse"
                                           data-parent="#accordion"
                                           href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            <i class="fa fa-file-text-o fa-icon" aria-hidden="true"></i>
                                            <?= Yii::t('frontend', 'Описание') ?>
                                            <span class="fa fa-angle-down fa-arrow" aria-hidden="true"></span>
                                            <i class="fa fa-angle-up fa-arrow" aria-hidden="true"></i>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel"
                                     aria-labelledby="headingOne">
                                    <div class="panel-body">
                                        <?= $model->description ?>
                                    </div>
                                </div>
                            </div>
                        <?php }
                        if ($model->information) { ?>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingTwo">
                                    <h4 class="panel-title">
                                        <a class="collapsed pa_italic" role="button" data-toggle="collapse"
                                           data-parent="#accordion"
                                           href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            <i class="fa fa-info-circle fa-icon" aria-hidden="true"></i>
                                            <?= Yii::t('frontend', 'Дополнительная информация') ?>
                                            <span class="fa fa-angle-down fa-arrow" aria-hidden="true"></span> <i
                                                    class="fa fa-angle-up fa-arrow" aria-hidden="true"></i>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel"
                                     aria-labelledby="headingTwo">
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <?= $model->information ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <?php //if ($model->information) { ?>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed pa_italic" role="button" data-toggle="collapse"
                                       data-parent="#accordion"
                                       href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        <i class="fa fa-star fa-icon" aria-hidden="true"></i>
                                        <?= Yii::t('frontend', 'Отзывы ({count})', ['count' => $dataProvider->totalCount]) ?>
                                        <span class="fa fa-angle-down fa-arrow" aria-hidden="true"></span>
                                        <i class="fa fa-angle-up fa-arrow" aria-hidden="true"></i>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="headingThree">
                                <div class="panel-body comments">

                                    <div class="col-md-12">
                                        <?php
                                        if (!empty($dataProvider->models)) {
                                            Pjax::begin(['id' => 'pjax_comments_list']);
                                            echo ListView::widget([
                                                'dataProvider' => $dataProvider,
                                                'layout' => "{items}\n<div align='center'>{pager}</div>",
                                                'itemView' => '_comment',
                                                'itemOptions' => ['class' => 'row item'],
                                                'pager' => ['class' => LinkPager::class]
                                            ]);

                                            $this->registerJs("jQuery('.like').click(function () {
                                            jQuery('.overlay').show();
                                            jQuery.ajax({
                                                type : 'POST',
                                                url: '/product/comment-like',
                                                data: {
                                                    item: jQuery(this).data('item'),
                                                    value: jQuery(this).data('like'),
                                                },
                                                success: function(response) {
                                                    if (response === true) {
                                                        jQuery.pjax.reload({
                                                            container: '#pjax_comments_list'
                                                        }).done(function () {
                                                            jQuery('.overlay').hide();
                                                        });
                                                    }
                                                    
                                                }
                                            });
                                        });");
                                            Pjax::end();
                                        } ?>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="row">
                                        <div class="col-md-8">
                                            <?php if (Yii::$app->user->isGuest) {
                                                Pjax::begin();
                                                echo '<br><p>'
                                                    . Yii::t('frontend', 'Для того, чтобы оставить свой отзыв выполните {btnLogin} или {btnSignup} на сайте.', [
                                                        'btnLogin' => Html::tag('b', Yii::t('frontend', 'вход')),
                                                        'btnSignup' => Html::a('<b><i>' . Yii::t('frontend', 'зарегистрируйтесь') . '</i></b>',
                                                            ['customer/signup'], ['data-pjax' => 0])
                                                    ])
                                                    . '</p><p><b>' . Yii::t('frontend', 'Пожалуйста, заполните следующие поля для входа:')
                                                    . '</b></p>';
                                                echo '<div class="row"><div class="col-md-6 col-sm-8">';
                                                $form = ActiveForm::begin(['id' => 'login-form', 'options' => ['data-pjax' => true]]);
                                                echo $form->field($loginForm, 'username')
                                                    . $form->field($loginForm, 'password')->passwordInput()
                                                    . $form->field($loginForm, 'rememberMe')->checkbox(['template' => '<label class="checkbox">{input}<i></i>{labelTitle}</label>'])
                                                    . '<p>'
                                                    . Html::a(Yii::t('frontend', 'Забыли пароль?'), ['customer/request-password-reset'], ['data-pjax' => 0])
                                                    . Html::submitButton(Yii::t('frontend', 'Войти'), ['class' => 'btn btn-primary pull-right', 'name' => 'login-button'])
                                                    . '</p>';
                                                ActiveForm::end();
                                                echo '</div></div>';
                                                Pjax::end();
                                            } else {
                                                echo '<br><h4 align="center">' . Yii::t('frontend', 'Написать отзыв') . '</h4><br>';
                                                $form = ActiveForm::begin(['id' => 'comment-form']);

                                                echo $form->field($commentForm, 'rating')->widget(StarRating::className(), [
                                                        'pluginOptions' => [
                                                            'size' => 'xs',
                                                            'showCaption' => false,
                                                            'showClear' => false,
                                                        ]
                                                    ])
                                                    . $form->field($commentForm, 'text')->textarea(['rows' => 6])
                                                    . Html::submitButton(Yii::t('frontend', 'Отправить'),
                                                        ['class' => 'btn btn-primary', 'name' => 'comment-button']);
                                                ActiveForm::end();
                                            } ?>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <?php //} ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- //collapse -->

        <?php if (count($products) > 4) { ?>
            <div class="row">
                <div class="col-md-12 welcome-item">
                    <h3 class="title"><?= Yii::t('frontend', 'Похожие товары') ?></h3>
                    <div id="owl-block" class="owl-carousel">
                        <?php /** @var $product \backend\models\product\Products */
                        foreach ($products as $product) { ?>
                            <div class="item">
                                <div class="glry-w3agile-grids agileits">
                                    <?= Html::a(Html::img($product->getDefaultImageUrl(), ['alt' => 'product']), ['view', 'slug' => $product->slug]) ?>
                                    <div class="view-caption agileits-w3layouts">
                                        <h4><?= Html::a($product->name, ['view', 'slug' => $product->slug]) ?></h4>
                                        <h5><?= $product->getPriceAsCurrency() ?>
                                            <?= $product->getOldPriceAsCurrency() ? '<br><del>' . $product->getOldPriceAsCurrency() . '</del>' : '' ?></h5>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <?php $this->registerJs('jQuery("#owl-block").owlCarousel({
                        autoPlay: 3000,
                        stopOnHover: true,
                        items: 4,
                        itemsDesktop: [992, 3],
                        itemsDesktopSmall: [767, 2],
                        navigation: true
                    });'); ?>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
<!--//products-->

<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\jui\SliderInput;
use yii\widgets\LinkPager;
use rmrevin\yii\fontawesome\FA;

/** @var $this yii\web\View */
/** @var $model \backend\models\product\Products */
/** @var $category \backend\models\product\Categories */
/** @var $dataProvider \yii\data\ActiveDataProvider */
/** @var $pagination \yii\data\Pagination */
/** @var $quantityText string */
/** @var $brandName string */

Pjax::begin(['id' => 'pjax_products', 'timeout' => 2000]);

$queryParams = Yii::$app->request->queryParams;

if ($category != null) {
    $title = $category->name;
    $this->title = $category->meta_title . $brandName;
    $this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Все товары'), 'url' => ['category', 'slug' => 'all']];
    if (($parent = $category->parent) != null) {
        if ($parent->parent != null)
            $this->params['breadcrumbs'][] = ['label' => $parent->parent->name, 'url' => ['category', 'slug' => $parent->parent->slug]];

        $this->params['breadcrumbs'][] = ['label' => $parent->name, 'url' => ['category', 'slug' => $parent->slug]];
    }
    $this->params['breadcrumbs'][] = $title;

    $this->params['meta_type'] = 'product-category';
    $this->params['meta_url'] = Yii::$app->request->hostInfo . '/product/category/' . $category->slug;
    $this->params['meta_image'] = Yii::$app->request->hostInfo . $category->image;
    if ($category->meta_keywords)
        $this->params['meta_keywords'] = $category->meta_keywords;
    if ($category->meta_description)
        $this->params['meta_description'] = $category->meta_description;
    if (!isset($queryParams['page']) || (isset($queryParams['page']) && $queryParams['page'] == 1))
        $this->params['footer_text'] = $category->description;

} else {
    $title = Yii::t('frontend', 'Все товары');
    $this->title = $title . $brandName;
    $this->params['breadcrumbs'][] = $title;
}

?>

<!-- products -->
<div class="products">
    <div class="row">
        <div class="col-sm-3 rsidebar">
            <div class="rsidebar-top">
                <?php if (!empty($categories)) { ?>
                    <div class="sidebar-row">
                        <h4>Категории</h4>
                        <ul class="faq">
                            <?php foreach ($categories as $item) {
                                /** @var $item \backend\models\product\Categories */
                                echo (isset($queryParams['slug']) && $queryParams['slug'] == $item->slug) ?
                                    '<li class="active">' . Html::a($item->name, ['category', 'slug' => $item->slug], ['data-pjax' => 0]) . '</li>' :
                                    '<li>' . Html::a($item->name, ['category', 'slug' => $item->slug], ['data-pjax' => 0]) . '</li>';
                            } ?>
                        </ul>
                    </div>
                    <hr>
                <?php } ?>

                <?php if ($category != null && !empty($category->activeChilds) && $dataProvider->totalCount > 0) { ?>
                    <div class="sidebar-row">
                        <h4><?= $category->name ?></h4>
                        <ul class="faq">
                            <?php foreach ($category->activeChilds as $item) {
                                /** @var $item \backend\models\product\Categories */
                                echo '<li>' . Html::a($item->name, ['category', 'slug' => $item->slug], ['data-pjax' => 0]) . '</li>';
                            } ?>
                        </ul>
                    </div>
                    <hr>
                <?php } ?>

                <?php if (!empty($brands) && (count($brands) > 1)) { ?>
                    <div class="sidebar-row">
                        <h4>Бренды</h4>
                        <ul class="faq">
                            <?php foreach ($brands as $item) {
                                /** @var $item \backend\models\product\Brands */
                                echo (isset($queryParams['brand']) && $queryParams['brand'] == $item->slug) ?
                                    '<li class="active">' . Html::a($item->name, Url::current(['brand' => $item->slug]), ['data-pjax' => 0]) . '</li>' :
                                    '<li>' . Html::a($item->name, Url::current(['brand' => $item->slug]), ['data-pjax' => 0]) . '</li>';
                            } ?>
                            <li><?= Html::a('Все бренды', Url::current(['brand' => null])) ?></li>
                        </ul>
                    </div>
                    <hr>
                <?php } ?>

                <?= Html::beginForm(Url::to(['category', 'slug' => $queryParams['slug']]), 'get', ['data-pjax' => true]) ?>
                <?= isset($queryParams['brand']) ? Html::hiddenInput('brand', $queryParams['brand']) : '' ?>

                <?php if (!empty($priceRange) && $priceRange['min'] != $priceRange['max']) { ?>
                    <div class="sidebar-row">
                        <h4>Ценовой диапазон</h4>
                        <div class="range-price-tooltip">
                            <span id="pricerange_tooltip"></span>
                        </div>
                        <?= SliderInput::widget([
                            'id' => 'pricerange',
                            'clientOptions' => [
                                'range' => true,
                                'min' => $priceRange['min'],
                                'max' => $priceRange['max'],
                                'values' => (isset($queryParams['price_min']) && isset($queryParams['price_max'])) ?
                                    [$queryParams['price_min'], $queryParams['price_max']] :
                                    [$priceRange['min'], $priceRange['max']],
                            ],
                            'clientEvents' => [
                                'slide' => 'function(event, ui) {
                                    $("#pricemin").val(ui.values["0"]);
                                    $("#pricemax").val(ui.values["1"]);
                                    $("#pricerange_tooltip").text(ui.values.join(" – ")).show();
                                }',
                                'stop' => 'function(event, ui) {
                                    $("#pricerange_tooltip").hide();
                                }',
                            ],
                        ]) ?>
                        <?= Html::hiddenInput('price_min', isset($queryParams['price_min']) ? $queryParams['price_min'] : $priceRange['min'], ['id' => 'pricemin']) ?>
                        <?= Html::hiddenInput('price_max', isset($queryParams['price_max']) ? $queryParams['price_max'] : $priceRange['max'], ['id' => 'pricemax']) ?>
                        <p>
                            <?= Yii::$app->formatter->asDecimal($priceRange['min'], 0) . ' – '
                            . Yii::$app->formatter->asDecimal($priceRange['max'], 0) . ' сум' ?>
                        </p>
                    </div>
                    <hr>
                <?php } ?>

                <?php if (!empty($discounts)) { ?>
                    <div class="sidebar-row">
                        <h4>Скидки</h4>
                        <?= Html::checkboxList('discount', isset($queryParams['discount']) ? $queryParams['discount'] : null, $discounts, [
                            'item' => function ($index, $label, $name, $checked, $value) {
                                $checked = $checked ? 'checked' : '';
                                return "<label class='checkbox'><input type='checkbox' {$checked} name='{$name}' value='{$value}'><i></i>{$label}</label>";
                            }
                        ]) ?>
                    </div>
                    <hr>
                <?php } ?>

                <?php if (!empty($ribbons)) { ?>
                    <div class="sidebar-row">
                        <h4>Наши предложения</h4>
                        <?= Html::checkboxList('ribbon', isset($queryParams['ribbon']) ? $queryParams['ribbon'] : null, $ribbons, [
                            'item' => function ($index, $label, $name, $checked, $value) {
                                $checked = $checked ? 'checked' : '';
                                return "<label class='checkbox'><input type='checkbox' {$checked} name='{$name}' value='{$value}'><i></i>{$label}</label>";
                            }
                        ]) ?>
                    </div>
                    <hr>
                <?php } ?>

                <?php if (!empty($filters)) {
                    foreach ($filters as $filter => $items) { ?>
                        <div class="sidebar-row">
                            <h4><?= $filter ?></h4>
                            <?= Html::checkboxList('filter', isset($queryParams['filter']) ? $queryParams['filter'] : null, $items, [
                                'item' => function ($index, $label, $name, $checked, $value) {
                                    $checked = $checked ? 'checked' : '';
                                    return "<label class='checkbox'><input type='checkbox' {$checked} name='{$name}' value='{$value}'><i></i>{$label}</label>";
                                }
                            ]) ?>
                        </div>
                        <hr>
                    <?php }
                } ?>

                <?php if (!empty($priceRange) && $priceRange['min'] != $priceRange['max']) { ?>
                    <div class="form-group btn-group">
                        <?= Html::submitButton('Показать', ['class' => 'btn btn-primary btn-sm product-filter']) .Html::a("Сбросить фильтр", Url::to(['category', 'slug' => $queryParams['slug']]), ['class' => 'btn btn-primary btn-sm']) ?>
                    </div>
                <?php } ?>

                <?= Html::endForm() ?>

            </div>
        </div>
        <div class="col-sm-9">
            <div class="product-top">
                <h4><?= $title . $brandName ?></h4>
                <?php if ($dataProvider->totalCount > 11) { ?>
                    <ul>
                        <li class="dropdown head-dpdn">
                            <button class="dropdown-toggle" data-toggle="dropdown">
                                Показывать товаров<span class="caret"></span></button>
                            <ul class="dropdown-menu products-sort">
                                <?php foreach ([12, 24, 36, 48] as $item) {
                                    echo (isset($queryParams['per-page']) && $queryParams['per-page'] == $item) ?
                                        '<li class="active">' . Html::a($item, Url::current(['per-page' => $item])) . '</li>' :
                                        '<li>' . Html::a($item, Url::current(['per-page' => $item])) . '</li>';
                                } ?>
                            </ul>
                        </li>
                    </ul>
                <?php } ?>
                <ul>
                    <li class="dropdown head-dpdn">
                        <button class="dropdown-toggle" data-toggle="dropdown">
                            Сортировать по<span class="caret"></span></button>
                        <ul class="dropdown-menu products-sort">
                            <li><?= $dataProvider->sort->link('price', ['label' => 'Цена', 'class' => 'sort-ordinal']) ?></li>
                            <li><?= $dataProvider->sort->link('name', ['label' => 'Название']) ?></li>
                            <li><?= $dataProvider->sort->link('views', ['label' => 'Популярности', 'class' => 'sort-ordinal']) ?></li>
                        </ul>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <span class="pull-right">
                <?= $quantityText ?>
            </span>
            <div class="clearfix"></div>
            <div class="products-row">
                <?php
                if (!empty($dataProvider->models)) {
                    foreach ($dataProvider->models as $model) { ?>
                        <div class="col-sm-6 col-md-4 product-grids">
                            <div class="agile-products">
                                <span class="barcode">
                                    <?= Yii::t('frontend', 'Код № ') . $model->barcode ?>
                                </span>
                                <?php if ($model->discount_id)
                                    echo '<div class="new-tag"><h6>' . $model->discount->name . '</h6></div>'; ?>

                                <?= Html::a(Html::img($model->getDefaultImageUrl(), ['alt' => 'product', 'class' => 'img-responsive']),
                                    ['view', 'slug' => $model->slug], ['data-pjax' => 0]) ?>
                                <div class="agile-product-text">
                                    <h5><?= Html::a($model->name, ['view', 'slug' => $model->slug], ['data-pjax' => 0]) ?></h5>

                                    <h6>
                                        <?= $model->getPriceAsCurrency() ?>
                                        <?php if (!empty($model->old_price))
                                            echo '<br><del>' . $model->getOldPriceAsCurrency() . '</del>'; ?>
                                    </h6>
                                    <?php if (isset($model->activeStickersBottom[0])) {
                                        echo '<p class="sticker">'
                                            . Html::a(Html::img($model->activeStickersBottom[0]->image, ['alt' => 'sticker', 'class' => 'img-responsive']),
                                                [$model->activeStickersBottom[0]->link],
                                                ['title' => $model->activeStickersBottom[0]->name, 'target' => '_blank', 'data-pjax' => 0])
                                            . '</p>';
                                    } ?>
                                </div>
                            </div>
                        </div>
                    <?php }
                } else {
                    echo '<br><h4 class="col-md-12">' . Yii::t('frontend', 'Результатов не найдено.') . '</h4>';
                } ?>
                <div class="clearfix"></div>
                <div class="products-pager">
                    <?= LinkPager::widget([
                        'pagination' => $pagination,
                        'firstPageLabel' => FA::icon(FA::_ANGLE_DOUBLE_LEFT),
                        'prevPageLabel' => FA::icon(FA::_ANGLE_LEFT),
                        'nextPageLabel' => FA::icon(FA::_ANGLE_RIGHT),
                        'lastPageLabel' => FA::icon(FA::_ANGLE_DOUBLE_RIGHT),
                        'maxButtonCount' => 7,
                    ]) ?>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<?php Pjax::end() ?>
<!--//products-->

<?php

/* @var $this yii\web\View */

use common\widgets\Alert;
use yii\widgets\Breadcrumbs;

/* @var $content string */

?>
<div class="content">
    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <div class="clearfix"></div>

        <?= Alert::widget() ?>

        <?= $content ?>

    </div>
</div>
<?php

use yii\helpers\Html;
use frontend\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <title><?= Html::encode($this->title) ?></title>
    <?= Html::csrfMetaTags() ?>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="yandex-verification" content="f3d05349c38302ed"/>
    <meta name="google-site-verification" content="TYCGTkzS_O0j7XameJd_owkFl_wE8RqRPPf_wyF6ALw" />
    <?php
    if (isset($this->params['meta_keywords']))
        $this->registerMetaTag(['name' => 'keywords', 'content' => $this->params['meta_keywords']]);
    if (isset($this->params['meta_description']))
        $this->registerMetaTag(['name' => 'description', 'content' => $this->params['meta_description']]);

    $this->registerMetaTag(['property' => 'og:site_name', 'content' => Yii::$app->name]);
    $this->registerMetaTag(['property' => 'og:title', 'content' => $this->title]);
    $this->registerMetaTag(['property' => 'og:locale', 'content' => Yii::$app->language]);
    if (isset($this->params['meta_type']))
        $this->registerMetaTag(['property' => 'og:type', 'content' => $this->params['meta_type']]);
    if (isset($this->params['meta_url']))
        $this->registerMetaTag(['property' => 'og:url', 'content' => $this->params['meta_url']]);
    if (isset($this->params['meta_image']))
        $this->registerMetaTag(['property' => 'og:image', 'content' => $this->params['meta_image']]);
    if (isset($this->params['meta_description']))
        $this->registerMetaTag(['property' => 'og:description', 'content' => $this->params['meta_description']]);

    $this->head(); ?>
</head>
<body>
<?php $this->beginBody() ?>

<!-- header -->
<?= $this->render('header') ?>
<!-- //header -->

<!-- content -->
<?= $this->render('content', ['content' => $content]) ?>
<!-- //content -->

<!-- footer -->
<?= $this->render('footer') ?>
<!-- //footer -->

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function () {
            try {
                w.yaCounter46604937 = new Ya.Metrika({
                    id: 46604937,
                    clickmap: true,
                    trackLinks: true,
                    accurateTrackBounce: true
                });
            } catch (e) {
            }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () {
                n.parentNode.insertBefore(s, n);
            };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else {
            f();
        }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript>
    <div><img src="https://mc.yandex.ru/watch/46604937" style="position:absolute; left:-9999px;" alt=""/></div>
</noscript>
<!-- /Yandex.Metrika counter -->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109515944-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());

    gtag('config', 'UA-109515944-1');
</script>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Pjax;
use common\models\Settings;
use frontend\models\Basket;
use backend\models\menu\Menus;
use kartik\typeahead\Typeahead;
use backend\models\parts\HtmlParts;

/* @var $this yii\web\View */
?>

<noindex>
    <div class="header">
        <div class="w3ls-header">
            <div class="w3ls-header-left">
                <p>Звоните нам:  +998 97 425 05 63</p>
            </div>
            <div class="w3ls-header-right">
                <ul>
                    <?php if (Yii::$app->user->isGuest) {
                        echo '<li class="dropdown head-dpdn">'
                            . Html::a('<i class="fa fa-sign-in"></i>' . Yii::t('frontend', 'Войти'),
                                ['/customer/login'], ['class' => 'dropdown-toggle'])
                            . '</li><li class="dropdown head-dpdn">'
                            . Html::a('<i class="fa fa-user"></i>' . Yii::t('frontend', 'Регистрация'),
                                ['/customer/signup'], ['class' => 'dropdown-toggle'])
                            . '</li>';
                    } else {
                        echo '<li class="dropdown head-dpdn"><span class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i>&nbsp;'
                            . Yii::$app->user->identity['email']
                            . '<span class="caret"></span></span><ul class="dropdown-menu"><li>'
                            . Html::a(Yii::t('frontend', 'Мои заказы'), ['customer/orders'])
                            . '</li><li>'
                            . Html::a(Yii::t('frontend', 'Моя учетная запись'), ['customer/account'])
                            . '</li><li>'
                            . Html::a(Yii::t('frontend', 'Изменить учетную запись'), ['customer/account-update'])
                            . '</li><li>'
                            . Html::beginForm(['/customer/logout'])
                            . Html::submitButton(Yii::t('frontend', 'Выйти'), ['class' => 'btn btn-link'])
                            . Html::endForm()
                            . '</li></ul></li>';
                    } ?>
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="header-two">
            <div class="container">
                <div class="header-logo">
                    <?= Html::a(Html::img(Settings::getLogoValue(), ['alt' => 'logo']), ['/']) ?>
                </div>
                <div class="header-search">
                    <?= Html::beginForm(['product/search'], 'get', ['class' => 'search-form']) ?>
                    <?= Typeahead::widget([
                        'name' => 'text',
                        'options' => [
                            'placeholder' => 'Поиск',
                            'required' => true,
                            'type' => 'search',
                            'style' => 'height: auto;border-radius: 0;',
                        ],
                        'scrollable' => true,
                        'pluginOptions' => [
                            'highlight' => true,
                            'minLength' => 2,
                        ],
                        'dataset' => [
                            [
                                'display' => 'value',
                                'limit' => 1000,
                                'remote' => [
                                    'url' => Url::to(['product/product-list']) . '?q=%QUERY',
                                    'wildcard' => '%QUERY'
                                ]
                            ]
                        ],
                        'pluginEvents' => [
                            'typeahead:select' => 'function(event, data) {
                            location.href = "/product/view/" + data["key"];
                        }',
                        ]
                    ]) ?>
                    <?= Html::submitButton('<i class="fa fa-search" aria-hidden="true"> </i>',
                        ['class' => 'btn btn-default']) ?>
                    <?= Html::endForm() ?>
                </div>
                <?php Pjax::begin(['id' => 'pjax_header_cart']); ?>
                <div class="header-cart">
                    <?php $qty = 0;
                    if (!empty($basketItems = Basket::getUserBasketItems())) { ?>
                        <div class="cart-item">
                            <div>
                                <ul>
                                    <?php $totalPrice = 0;
                                    /** @var Basket $item */
                                    foreach ($basketItems as $item) {
                                        if ($item->product !== null) {
                                            echo '<li class="sbmincart-item"><div class="sbmincart-details-name">'
                                                . Html::a($item->product->name, ['/product/view', 'slug' => $item->product->slug], ['class' => 'sbmincart-name', 'data-pjax' => 0])
                                                . '</div><div class="sbmincart-details-quantity"><strong>x'
                                                . $item->quantity
                                                . '</strong></div><div class="sbmincart-details-price">'
                                                . $item->getItemPriceAsCurrency()
                                                . '</div></li>';

                                            $qty += $item->quantity;
                                            $totalPrice += $item->getItemPrice();
                                        }
                                    } ?>
                                </ul>
                                <div class="sbmincart-footer">
                                    <div class="sbmincart-subtotal">
                                        <?= Yii::t('frontend', 'Всего к оплате: ')
                                        . Yii::$app->formatter->asDecimal($totalPrice, 0)
                                        . Yii::t('model', '&nbsp;<i>сум</i>') ?>
                                    </div>
                                    <p>
                                        <?= Html::a(Yii::t('frontend', 'Корзина'),
                                            ['customer/basket'], ['data-pjax' => 0, 'class' => 'btn btn-default']) ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <?= Html::a('<button class="cart"><i class="fa fa-shopping-cart"></i><span>' . $qty . '</span></button>', ['customer/basket'], ['data-pjax' => 0]) ?>
                    <div class="clearfix"></div>
                </div>
                <?php Pjax::end(); ?>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="header-three">
            <div class="container">
                <?php
                if ((($menu = Menus::findOne(['key' => 'categories', 'status' => Menus::STATUS_ACTIVE])) !== null) && !empty($menu->menuItemsActive)) {
                    $strMenuItems = '';
                    foreach ($menu->menuItemsActive as $menuItems) {
                        if (!empty($menuItems->menuItemsActive)) {
                            $strItems = '';
                            $i = 1;

                            foreach ($menuItems->menuItemsActive as $items) {
                                if (!empty($items->menuItemsActive)) {
                                    $strChilds = '';

                                    foreach ($items->menuItemsActive as $childs) {
                                        $strChilds = $strChilds . '<li class="item-' . $childs->id . '">'
                                            . Html::a($childs->label, Url::to($childs->url))
                                            . '</li>';
                                    }

                                    $strItems .= '<li class="has-children item-' . $items->id . '">'
                                        . Html::a($items->label, Url::to($items->url))
                                        . '<ul class="is-hidden"><li class="go-back"><a href="#">Назад</a></li>'
                                        . $strChilds
                                        . '</ul></li>';
                                } else {
                                    $strItems .= '<li class="item-' . $items->id . '">'
                                        . Html::a($items->label, Url::to($items->url))
                                        . '</li>';
                                }

                                if ($i % 3 == 0)
                                    $strItems .= '<div class="clearfix"></div>';
                                $i++;
                            }

                            $strMenuItems .= '<li class="has-children item-' . $menuItems->id . '">'
                                . Html::a($menuItems->label, Url::to($menuItems->url))
                                . '<ul class="cd-secondary-dropdown is-hidden"><li class="go-back"><a href="#">Меню</a></li><li class="see-all">'
                                . Html::a('Все категории', ['/product/category/all'])
                                . '</li>'
                                . $strItems
                                . '</ul></li>';
                        } else {
                            $strMenuItems .= '<li class="item-' . $menuItems->id . '">'
                                . Html::a($menuItems->label, Url::to($menuItems->url))
                                . '</li>';
                        }
                    }

                    echo '<div class="menu"><div class="cd-dropdown-wrapper"><span class="cd-dropdown-trigger">'
                        . $menu->name
                        . '</span><nav class="cd-dropdown"><span class="cd-close">Закрыть</span><ul class="cd-dropdown-content">'
                        . $strMenuItems
                        . '</ul></nav></div></div>';
                } ?>

            </div>
        </div>
    </div>
</noindex>

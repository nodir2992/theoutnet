<?php

use yii\helpers\Html;
use common\models\Settings;
use backend\models\menu\Menus;
use backend\models\parts\HtmlParts;

/* @var $this yii\web\View */
?>

<?php if (isset($this->params['footer_text'])) { ?>

    <?= (($footerTop = HtmlParts::findOne(['key' => 'footer_top', 'status' => HtmlParts::STATUS_ACTIVE])) !== null) ? $footerTop->body : '' ?>

    <div class="footer-text">
        <div class="container">
            <?= $this->params['footer_text'] ?>
            <div class="clearfix"></div>
        </div>
    </div>
<?php } ?>

<div class="footer">
    <div class="container">
        <div class="footer-info w3-agileits-info">
            <div class="col-md-4 address-left agileinfo">
                <div class="footer-logo header-logo">
                    <?= Html::a(Html::img(Settings::getLogoValue(), ['alt' => 'logo']), ['/']) ?>
                </div>
                <?= (($footerContact = HtmlParts::findOne(['key' => 'footer_contact', 'status' => HtmlParts::STATUS_ACTIVE])) !== null) ?
                    $footerContact->body : '' ?>
            </div>
            <div class="col-md-8 address-right">
                <?php if ((($menuInfo = Menus::findOne(['key' => 'footer_info', 'status' => Menus::STATUS_ACTIVE])) !== null) && !empty($menuInfo->menuItemsActive)) { ?>
                    <div class="col-md-4 footer-grids">
                        <h3><?= $menuInfo->name ?></h3>
                        <ul>
                            <?php foreach ($menuInfo->menuItemsActive as $item) {
                                echo '<li>' . Html::a($item->label, [$item->url]) . '</li>';
                            } ?>
                        </ul>
                    </div>
                <?php } ?>

                <?php if ((($menuService = Menus::findOne(['key' => 'footer_service', 'status' => Menus::STATUS_ACTIVE])) !== null) && !empty($menuService->menuItemsActive)) { ?>
                    <div class="col-md-4 footer-grids">
                        <h3><?= $menuService->name ?></h3>
                        <ul>
                            <?php foreach ($menuService->menuItemsActive as $item) {
                                echo '<li>' . Html::a($item->label, [$item->url]) . '</li>';
                            } ?>
                        </ul>
                    </div>
                <?php } ?>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="copy-right">
    <div class="container">
        <p><?= Yii::t('frontend', '© THEOUTNET.UZ — Все права защищены.') ?></p>
    </div>
</div>

<div class="overlay">
    <div class="spinner"></div>
</div>
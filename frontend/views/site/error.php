<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */
/* @var $statusCode integer */

$this->title = $name;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-error">

    <div class="row">
        <div class="col-sm-6">
            <h1><?= Html::encode(isset($exception->statusCode) ? $exception->statusCode : $exception->getCode()) ?></h1>
        </div>
        <div class="col-sm-6">
            <div class="alert alert-danger">
                <?= nl2br(Html::encode($message)) ?>
            </div>
        </div>
    </div>

</div>

<?php

use yii\helpers\Html;
use russ666\widgets\Countdown;

/* @var $this yii\web\View */
/* @var $slider \backend\models\sliders\Sliders */
/* @var $hurry \backend\models\product\Ribbons */
/* @var $forFamily \backend\models\product\Ribbons */
/* @var $manChoice \backend\models\product\Ribbons */
/* @var $womenThings \backend\models\product\Ribbons */
/* @var $metaKeywords string */
/* @var $metaDescription string */
/* @var $footerText string */
/* @var $brands \backend\models\product\Brands */
/* @var $posts \backend\models\post\Posts */
/* @var $title string */
/* @var $logo string */

$this->title = $title;

$this->params['meta_type'] = 'page';
$this->params['meta_url'] = Yii::$app->request->hostInfo;
$this->params['meta_image'] = $logo;
if ($metaKeywords !== null)
    $this->params['meta_keywords'] = $metaKeywords;
if ($metaDescription !== null)
    $this->params['meta_description'] = $metaDescription;

if ($footerText !== null)
    $this->params['footer_text'] = $footerText;

$hurryBlock = $hurry != null && isset($hurry->products[0]);
$hurryProduct = $hurryBlock ? $hurry->products[0] : null;
?>

<?php if ($slider != null && !empty($slider->sliderActiveItems)) { ?>
    <div class="banner">
        <div class="row">
            <div class="col-sm-12">
                <div id="kb" class="carousel slide" data-ride="carousel" data-interval="4000">
                    <!-- Wrapper-for-Slides -->
                    <div class="carousel-inner" role="listbox">
                        <?php $class = 'item active';
                        foreach ($slider->sliderActiveItems as $item) {
                            echo "<div class='$class'>";
                            echo (empty($item->link) || ($item->link == '#')) ?
                                Html::img($item->image, ['alt' => strip_tags($item->title), 'class' => 'img-responsive']) :
                                Html::a(Html::img($item->image, ['alt' => strip_tags($item->title), 'class' => 'img-responsive']), [$item->link]);
                            echo "</div>";
                            $class = 'item';
                        } ?>
                    </div>
                    <!-- Left-Button -->
                    <button class="left carousel-control kb_control_left" href="#kb" role="button"
                            data-slide="prev">
                        <span class="fa fa-angle-left kb_icons" aria-hidden="true"></span>
                    </button>
                    <!-- Right-Button -->
                    <button class="right carousel-control kb_control_right" href="#kb" role="button"
                            data-slide="next">
                        <span class="fa fa-angle-right kb_icons" aria-hidden="true"></span>
                    </button>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<div class="welcome">

    <?php if (!empty($brands)) { ?>
        <div class="row">
            <div class="col-md-12 welcome-item">
                <div id="owl-block-4" class="owl-carousel brands">
                    <?php foreach ($brands as $brand) {
                        /** @var $brand \backend\models\product\Brands */ ?>
                        <div class="item">
                            <?= Html::a(Html::img($brand->image, ['alt' => $brand->name]), $brand->link) ?>
                        </div>
                    <?php } ?>
                </div>
                <?php $this->registerJs('jQuery("#owl-block-4").owlCarousel({
                    autoPlay: 1500,
                    stopOnHover: true,
                    items: 6,
                    itemsDesktop: [992, 4],
                    itemsDesktopSmall: [767, 3],
                });'); ?>
            </div>
        </div>
    <?php } ?>

    <?php if ($forFamily != null && !empty($forFamily->products)) { ?>
        <div class="row">
            <div class="col-md-12 welcome-item">
                <h3 class="title"><?= $forFamily->name ?></h3>
                <div id="owl-block-1" class="owl-carousel">
                    <?php foreach ($forFamily->products as $item) { ?>
                        <div class="item">
                            <div class="glry-w3agile-grids agileits">
                                <?= Html::a(Html::img($item->getDefaultImageUrl(), ['alt' => 'product']), ['/product/view', 'slug' => $item->slug]) ?>
                                <div class="view-caption agileits-w3layouts">
                                    <h4><?= Html::a($item->name, ['/product/view', 'slug' => $item->slug]) ?></h4>
                                    <h5><?= $item->getPriceAsCurrency() ?>
                                        <?= $item->getOldPriceAsCurrency() ? '<br><del>' . $item->getOldPriceAsCurrency() . '</del>' : '' ?></h5>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <?php $this->registerJs('jQuery("#owl-block-1").owlCarousel({
                    autoPlay: 3000,
                    stopOnHover: true,
                    items: 4,
                    itemsDesktop: [992, 3],
                    itemsDesktopSmall: [767, 2],
                    navigation: true
                });'); ?>
            </div>
        </div>
    <?php } ?>

    <?php if (!empty($posts)) { ?>
        <div class="home-page-posts">
            <div class="row">
                <?php foreach ($posts as $post) {
                    /* @var $post \backend\models\post\Posts */ ?>
                    <div class="col-sm-6">
                        <div class="item">
                            <?= Html::a(Html::img($post->image, ['alt' => 'post']), ['post/view', 'slug' => $post->slug]) ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    <?php } ?>

    <?php if ($manChoice != null && !empty($manChoice->products)) { ?>
        <div class="row">
            <div class="col-md-12 welcome-item">
                <h3 class="title"><?= $manChoice->name ?></h3>
                <div id="owl-block-2" class="owl-carousel">
                    <?php foreach ($manChoice->products as $item) { ?>
                        <div class="item">
                            <div class="glry-w3agile-grids agileits">
                                <?= Html::a(Html::img($item->getDefaultImageUrl(), ['alt' => 'product']), ['/product/view', 'slug' => $item->slug]) ?>
                                <div class="view-caption agileits-w3layouts">
                                    <h4><?= Html::a($item->name, ['/product/view', 'slug' => $item->slug]) ?></h4>
                                    <h5><?= $item->getPriceAsCurrency() ?>
                                        <?= $item->getOldPriceAsCurrency() ? '<br><del>' . $item->getOldPriceAsCurrency() . '</del>' : '' ?></h5>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <?php $this->registerJs('jQuery("#owl-block-2").owlCarousel({
                    autoPlay: 3000,
                    stopOnHover: true,
                    items: 4,
                    itemsDesktop: [992, 3],
                    itemsDesktopSmall: [767, 2],
                    navigation: true
                });'); ?>
            </div>
        </div>
    <?php } ?>

    <?php if ($womenThings != null && !empty($womenThings->products)) { ?>
        <div class="row">
            <div class="col-md-12 welcome-item">
                <h3 class="title"><?= $womenThings->name ?></h3>
                <div id="owl-block-3" class="owl-carousel">
                    <?php foreach ($womenThings->products as $item) { ?>
                        <div class="item">
                            <div class="glry-w3agile-grids agileits">
                                <?= Html::a(Html::img($item->getDefaultImageUrl(), ['alt' => 'product']), ['/product/view', 'slug' => $item->slug]) ?>
                                <div class="view-caption agileits-w3layouts">
                                    <h4><?= Html::a($item->name, ['/product/view', 'slug' => $item->slug]) ?></h4>
                                    <h5><?= $item->getPriceAsCurrency() ?>
                                        <?= $item->getOldPriceAsCurrency() ? '<br><del>' . $item->getOldPriceAsCurrency() . '</del>' : '' ?></h5>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <?php $this->registerJs('jQuery("#owl-block-3").owlCarousel({
                    autoPlay: 3000,
                    stopOnHover: true,
                    items: 4,
                    itemsDesktop: [992, 3],
                    itemsDesktopSmall: [767, 2],
                    navigation: true
                });'); ?>
            </div>
        </div>
    <?php } ?>

</div>

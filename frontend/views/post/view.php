<?php

use yii\helpers\Html;
use backend\models\post\Posts;

/* @var $this yii\web\View */
/* @var $model Posts */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => $model->category->name, 'url' => ['category', 'slug' => $model->category->slug]];
$this->params['breadcrumbs'][] = $this->title;

$this->params['meta_type'] = 'post';
$this->params['meta_url'] = Yii::$app->request->hostInfo . '/post/view/' . $model->slug;
$this->params['meta_image'] = Yii::$app->request->hostInfo . $model->image;
if ($model->meta_keywords)
    $this->params['meta_keywords'] = $model->meta_keywords;
if ($model->meta_description)
    $this->params['meta_description'] = $model->meta_description;
?>

<div class="deals post-view">
    <h3 class="w3ls-title title-center"><?= $this->title ?></h3>

    <?= $model->body ?>
    <?php if (!empty($model->products)) { ?>
        <div class="row">
            <div class="col-md-12 welcome-item">
                <h3 class="title"><?= Yii::t('frontend', 'Товары из этого обзора') ?></h3>
                <div id="owl_post_product" class="owl-carousel">
                    <?php foreach ($model->products as $product) { ?>
                        <div class="item">
                            <div class="glry-w3agile-grids agileits">
                                <?= Html::a(Html::img($product->getDefaultImageUrl(), ['alt' => 'product']), ['product/view', 'slug' => $product->slug]) ?>
                                <div class="view-caption agileits-w3layouts">
                                    <h4><?= Html::a($product->name, ['product/view', 'slug' => $product->slug]) ?></h4>
                                    <h5><?= $product->getPriceAsCurrency() ?>
                                        <?= $product->getOldPriceAsCurrency() ? '<br><del>' . $product->getOldPriceAsCurrency() . '</del>' : '' ?></h5>
                                    <?= $product->isAvailable() ? Html::button('<i class="fa fa-cart-plus"></i> '
                                        . Yii::t('frontend', 'В корзину'),
                                        ['class' => 'add-to-cart w3ls-cart', 'data-product' => $product->id]) :
                                        '<span class="product-not-available">' . Yii::t('frontend', 'Нет в наличии') . '</span>' ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <?php $this->registerJs('jQuery("#owl_post_product").owlCarousel({
                    autoPlay: 3000,
                    items: 4,
                    itemsDesktop: [992, 3],
                    itemsDesktopSmall: [767, 2],
                    navigation: true
                });'); ?>
            </div>
        </div>
    <?php } ?>
</div>

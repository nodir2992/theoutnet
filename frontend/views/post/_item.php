<?php

use yii\helpers\Html;
use rmrevin\yii\fontawesome\FA;

/* @var $this yii\web\View */
/* @var $model \backend\models\post\Posts */

?>
<div class="col-md-12">
    <div class="separator"></div>
</div>

<div class="col-md-2 col-sm-3">
    <?= Html::a(Html::img($model->image, ['alt' => $model->category->name, 'class' => 'img-responsive']),
        ['view', 'slug' => $model->slug], ['class' => 'thumbnail', 'data-pjax' => 0]) ?>
</div>

<div class="col-md-8 col-sm-9">
    <h4 class="title"><?= Html::a($model->title, ['view', 'slug' => $model->slug], ['data-pjax' => 0]) ?></h4>
    <p class="date">
        <?= Fa::i(FA::_CALENDAR) . '&nbsp;'
        . Yii::$app->formatter->asDate($model->created_at, 'dd-MM-Y')
        . ' | '
        . Fa::i(FA::_CLOCK_O) . '&nbsp;'
        . Yii::$app->formatter->asDate($model->created_at, 'H:m')
        . ' | '
        . Fa::i(FA::_EYE) . '&nbsp;'
        . $model->views ?>
    </p>
    <p class="text"><?= $model->summary ?></p>
    <p class="more"><?= Html::a(Yii::t('frontend', 'Подробнее'),
            ['view', 'slug' => $model->slug], ['class' => 'btn btn-info', 'data-pjax' => 0]) ?></p>
</div>

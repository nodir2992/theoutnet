<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use yii\widgets\DetailView;
use yii\widgets\MaskedInput;
use yii\bootstrap\ActiveForm;
use frontend\assets\SmartWizardAsset;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $loginForm \common\models\LoginForm */
/* @var $checkoutForm \frontend\models\CheckoutForm */
/* @var $offerText string */

$this->title = Yii::t('frontend', 'Оформить заказ');
$this->params['breadcrumbs'][] = ['label' => 'Корзина', 'url' => ['basket']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="checkout">
    <?php if ($loginForm !== null) { ?>
        <div class="row">
            <div class=" col-md-4 col-sm-8">
                <h3 class="w3ls-title"><?= Yii::t('frontend', 'Вход') ?></h3>
                <?php $form = ActiveForm::begin(['id' => 'login_form']); ?>

                <?= $form->field($loginForm, 'username')->textInput() ?>
                <?= $form->field($loginForm, 'password')->passwordInput() ?>
                <?= $form->field($loginForm, 'rememberMe')->checkbox(['template' => '<label class="checkbox">{input}<i></i>{labelTitle}</label>']) ?>

                <p>
                    <?= Html::a(Yii::t('frontend', 'Забыли пароль?'), ['request-password-reset']) ?>
                    <?= Html::submitButton(Yii::t('frontend', 'Войти'), ['class' => 'btn btn-primary pull-right', 'name' => 'login-button']) ?>
                </p>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
        <hr>
        <h3 class="w3ls-title"><?= Yii::t('frontend', 'Оформить заказ как Гость') ?></h3>
    <?php } else {
        echo '<h3 class="w3ls-title">' . $this->title . '</h3>';
    } ?>

    <?php
    Pjax::begin(['id' => 'checkout_pjax']);
    SmartWizardAsset::register($this);
    $this->registerJs($this->render('_checkout.js'));
    $form = ActiveForm::begin(['id' => 'checkout_form', 'options' => ['data-pjax' => true]]); ?>
    <div id="checkout_wizard">

        <ul>
            <li><a href="#step-0">
                    <i class="fa fa-user"></i><br/>
                    <?= Yii::t('frontend', 'Информация о покупателе') ?>
                </a></li>
            <li><a href="#step-1">
                    <i class="fa fa-map-marker"></i><br/>
                    <?= Yii::t('frontend', 'Информация о доставке') ?>
                </a></li>
            <li><a href="#step-2">
                    <i class="fa fa-money"></i><br/>
                    <?= Yii::t('frontend', 'Платежная информация') ?>
                </a></li>
            <li><a href="#step-3">
                    <i class="fa fa-check"></i><br/>
                    <?= Yii::t('frontend', 'Оформить заказ') ?>
                </a></li>
        </ul>

        <div>

            <div id="step-0">
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-8">
                        <?= $form->field($checkoutForm, 'first_name')->textInput() ?>
                        <?= $form->field($checkoutForm, 'last_name')->textInput() ?>
                        <?= $form->field($checkoutForm, 'email')->textInput() ?>
                        <?= $form->field($checkoutForm, 'phone')->widget(MaskedInput::className(),
                            ['mask' => '+\\9\\9899-999-9999']) ?>
                    </div>
                </div>
            </div>
            <div id="step-1">
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-8">
                        <?= $form->field($checkoutForm, 'district')->dropDownList($checkoutForm->getLocationsList(),
                            ['prompt' => Yii::t('frontend', 'Выбрать ...')]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-9">
                        <?= $form->field($checkoutForm, 'address') ?>
                        <p>
                            <?= Yii::t('frontend', 'Пожалуйста укажите дату и удобное для вас время и место доставки товара (дата/адрес/время).
                            Доставка осуществляется в течение 48 часов после подтверждения заказа.') ?>
                        </p>
                        <?= $form->field($checkoutForm, 'comment')->textarea(['rows' => 6]) ?>
                    </div>
                </div>
            </div>
            <div id="step-2">
                <?= $form->field($checkoutForm, 'payment')->radioList($checkoutForm->getPaymentList(), [
                    'item' => function ($index, $label, $name, $checked, $value) {
                        $checked = $checked ? 'checked' : '';
                        return "<label class='checkbox'><input type='radio' {$checked} name='{$name}' value='{$value}'><i></i>{$label}</label>";
                    }
                ]) ?>

                <?php if (Yii::$app->user->isGuest) {
                    echo $form->field($checkoutForm, 'offer')->checkbox([
                        'template' => '{beginLabel}{input} {labelTitle}{endLabel} <button type="button" data-toggle="modal" data-target="#offerModal" class="btn btn-link"><i>'
                            . Yii::t('frontend', '«Пользовательского соглашения»')
                            . '</i></button>{error}{hint}'
                    ]);

                    Modal::begin([
                        'id' => 'offerModal',
                        'size' => 'modal-lg',
                        'header' => '<h4>' . Yii::t('frontend', 'Публичная оферта интернет-магазина') . '</h4>',
                    ]);
                    echo $offerText . '<div class="clearfix"></div>';
                    Modal::end();
                } ?>

                <?= $form->field($checkoutForm, 'step')->hiddenInput(['id' => 'form_step'])->label(false) ?>
            </div>
            <div id="step-3">
                <div class="row">
                    <div class="table-responsive col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2">
                        <?= DetailView::widget([
                            'model' => $checkoutForm,
                            'attributes' => [
                                'first_name',
                                'last_name',
                                'email',
                                'phone',
                                [
                                    'attribute' => 'district',
                                    'value' => $checkoutForm->getDistrictName()
                                ],
                                'address',
                                'comment:ntext',
                                [
                                    'attribute' => 'payment',
                                    'format' => 'raw',
                                    'value' => $checkoutForm->getPaymentName(),
                                ],
                                [
                                    'attribute' => 'item_count',
                                    'format' => 'raw',
                                    'value' => $checkoutForm->getBasketInfo('item_count'),
                                ],
                                [
                                    'attribute' => 'total_price',
                                    'format' => 'raw',
                                    'value' => $checkoutForm->getBasketInfo('total_price'),
                                ],
                            ],
                        ]) ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <?php
    ActiveForm::end();
    Pjax::end(); ?>

</div>
<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use backend\models\order\Orders;

/* @var $this yii\web\View */
/* @var $orders Orders */

$this->title = Yii::t('frontend', 'Заказы');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="deals customer-orders">
    <h3 class="w3ls-title title-center"><?= $this->title ?></h3>
    <div class="table-responsive">
        <?php Pjax::begin();
        echo GridView::widget([
            'dataProvider' => $orders,
            'layout' => "{summary}\n{items}\n<div align='center'>{pager}</div>",
            'summary' => '<div class="pull-right"><span>'
                . Yii::t('frontend', 'Заказы <b>{begin}—{end}</b> из <b>{totalCount}</b>')
                . '</span></div><div class="clearfix"></div>',
            'columns' => [

                [
                    'attribute' => 'id',
                    'options' => ['width' => '50'],
                    'contentOptions' => ['class' => 'center'],
                    'label' => Yii::t('model', 'Номер заказа'),
                ],
                [
                    'attribute' => 'status',
                    'format' => 'raw',
                    'value' => function (Orders $data) {
                        return $data->getStatusName();
                    },
                ],
                [
                    'attribute' => 'payment',
                    'format' => 'raw',
                    'value' => function (Orders $model) {
                        return $model->getPaymentName();
                    },
                ],
                [
                    'attribute' => 'created_at',
                    'contentOptions' => ['class' => 'center'],
                    'value' => function (Orders $model) {
                        return Yii::$app->formatter->asDate($model->created_at, 'dd-MM-Y');
                    },
                ],
                [
                    'attribute' => 'total_quantity',
                    'format' => 'raw',
                    'options' => ['width' => '50'],
                    'contentOptions' => ['class' => 'center'],
                    'value' => function (Orders $model) {
                        return Yii::$app->formatter->asDecimal(array_sum(array_map(function ($item) {
                            return $item['quantity'];
                        }, $model->orderItems)), 0);
                    },
                ],
                [
                    'attribute' => 'total_price',
                    'format' => 'raw',
                    'contentOptions' => ['class' => 'center'],
                    'value' => function (Orders $model) {
                        return Yii::$app->formatter->asDecimal(array_sum(array_map(function ($item) {
                                return $item['total_price'];
                            }, $model->orderItems)), 0)
                            . Yii::t('model', '&nbsp;<i>сум</i>');
                    },
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'header' => Yii::t('yii', 'View'),
                    'contentOptions' => ['class' => 'center'],
                    'template' => '{order-view}',
                    'buttons' => [
                        'order-view' => function ($url) {
                            return Html::a('<span class="fa fa-eye"></span>', $url, [
                                'title' => Yii::t('yii', 'View'),
                                'class' => 'order-view btn-sm btn btn-info',
                                'data-pjax' => 0,
                            ]);
                        },
                    ],
                ]
            ],
        ]);
        Pjax::end();

        Modal::begin(['id' => 'order_detail', 'size' => 'modal-lg']);
        Modal::end();
        $this->registerJs("$('.order-view').click(function(e){
            e.preventDefault();
            $('#order_detail').modal('show').find('.modal-content').load($(this).attr('href'));
        });"); ?>
    </div>

</div>

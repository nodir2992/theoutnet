<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use frontend\models\Basket;
use kartik\touchspin\TouchSpin;

/* @var $this yii\web\View */
/* @var $models \frontend\models\Basket */

$this->title = Yii::t('frontend', 'Корзина');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="basket">
    <h3 class="w3ls-title title-center"><?= $this->title ?></h3>

    <?php
    if (!empty($models)) {
        Pjax::begin(['id' => 'pjax_basket', 'timeout' => 2000]);
        echo Html::beginForm('', '', ['id' => 'formBasket', 'data-pjax' => true])
            . '<div class="row"><div class="col-md-8 col-md-offset-2"><ul class="list-group basket-items">';

        $totalPrice = 0;
        /** @var Basket $item */
        foreach ($models as $item) {
            if ($item->product !== null) {
                $minQty = 1;
                $maxQty = $item->product->quantity ?: 100;
                echo '<li class="list-group-item">'
                    . Html::a(Html::img($item->product->getDefaultImageUrl(), ['alt' => 'product-image'])
                        . '<span>' . $item->product->name . '</span>', ['/product/view', 'slug' => $item->product->slug], ['class' => 'item', 'data-pjax' => 0])
                    . Html::a('<i class="fa fa-remove"></i>', ['delete-item', 'id' => $item->product_id], ['class' => 'item-remove', 'data-pjax' => 0])
                    . '<strong>'
                    . $item->getItemPriceAsCurrency()
                    . '</strong>'
                    . TouchSpin::widget([
                        'name' => 'product[' . $item->product_id . ']',
                        'value' => $item->quantity,
                        'options' => ['class' => 'input-sm'],
                        'pluginOptions' => [
                            'min' => $minQty,
                            'max' => $maxQty,
                            'buttonup_txt' => '<i class="glyphicon glyphicon-plus"></i>',
                            'buttondown_txt' => '<i class="glyphicon glyphicon-minus"></i>',
                            'buttonup_class' => $item->quantity == $maxQty ? 'disabled btn btn-default' : 'btn btn-default',
                            'buttondown_class' => $item->quantity == $minQty ? 'disabled btn btn-default' : 'btn btn-default',
                        ],
                        'pluginEvents' => [
                            'touchspin.on.startspin' => 'function(e) {
                                $("#formBasket").submit();
                            }'
                        ]
                    ])
                    . '</li>';
            }
            $totalPrice += $item->getItemPrice();
        }

        echo '</ul><p class="total-price">'
            . Yii::t('frontend', 'Всего к оплате:')
            . '&nbsp;'
            . Yii::$app->formatter->asDecimal($totalPrice, 0)
            . '&nbsp;<i>'
            . Yii::t('frontend', 'сум')
            . '</i></p><div class="btn-group pull-right">'
//            . Html::submitButton(Yii::t('frontend', 'Обновить корзину'), ['class' => 'btn btn-primary'])
            . Html::a(Yii::t('frontend', 'Оформить заказ'), ['checkout'], ['class' => 'btn btn-primary', 'data-pjax' => 0])
            . '</div><div class="clearfix"></div></div></div>'
            . Html::endForm();
        Pjax::end();
    } else {
        echo '<p>' . Yii::t('frontend', 'Ваша корзина для покупок пуста.') . '</p>'
            . Html::a(Yii::t('frontend', 'Вернуться на главную'), ['/']);
    } ?>
</div>
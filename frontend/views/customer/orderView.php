<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;
use backend\models\order\OrderItems;

/* @var $this yii\web\View */
/* @var $model \backend\models\order\Orders */
/* @var $isAjax bool */

$this->title = Yii::t('frontend', 'Номер заказа: ') . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Заказы'), 'url' => ['orders']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php if ($isAjax) { ?>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4><?= $this->title ?></h4>
    </div>
<?php } else { ?>
    <br>
    <h3 class="w3ls-title title-center"><?= $this->title ?></h3>
<?php } ?>
<div class="modal-body">
    <?php if (!empty($model->orderItems)) { ?>
        <div class="table-responsive">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'first_name',
                    'last_name',
                    'email:email',
                    'phone',
                    [
                        'attribute' => 'location_id',
                        'value' => $model->location->name
                    ],
                    'address',
                    'comment:ntext',
                    [
                        'attribute' => 'payment',
                        'format' => 'raw',
                        'value' => $model->getPaymentName(),
                    ],
                    [
                        'attribute' => 'status',
                        'format' => 'raw',
                        'value' => $model->getStatusName(),
                    ],
                    [
                        'attribute' => 'created_at',
                        'format' => 'raw',
                        'value' => Yii::$app->formatter->asDate($model->created_at, 'dd-MM-Y'),
                    ],
                ],
            ]) ?>
        </div>
        <div class="table-responsive">
            <?= GridView::widget([
                'layout' => '{items}',
                'showFooter' => true,
                'tableOptions' => ['class' => 'table table-bordered'],
                'footerRowOptions' => ['class' => 'text-bold'],
                'dataProvider' => $model->getOrderItemsDataProvider(),
                'columns' => [
                    [
                        'attribute' => 'product_id',
                        'format' => 'raw',
                        'value' => function (OrderItems $data) {
                            return Html::img($data->product->getDefaultImageUrl(),
                                    ['alt' => 'product_image', 'width' => '100px'])
                                . '<br>'
                                . $data->product->name;
                        },
                        'footer' => 'Общий'
                    ],
                    [
                        'attribute' => 'price',
                        'contentOptions' => ['class' => 'center'],
                        'format' => 'raw',
                        'value' => function (OrderItems $data) {
                            return Yii::$app->formatter->asDecimal($data->price, 0)
                                . Yii::t('model', '&nbsp;<i>сум</i>');
                        }
                    ],
                    [
                        'attribute' => 'quantity',
                        'contentOptions' => ['class' => 'center'],
                        'footer' => array_sum(array_map(function ($item) {
                            return $item['quantity'];
                        }, $model->orderItems))

                    ],
                    [
                        'attribute' => 'total_price',
                        'format' => 'raw',
                        'contentOptions' => ['class' => 'center'],
                        'value' => function (OrderItems $data) {
                            return Yii::$app->formatter->asDecimal($data->total_price, 0)
                                . Yii::t('model', '&nbsp;<i>сум</i>');
                        },
                        'footer' => Yii::$app->formatter->asDecimal(array_sum(array_map(function ($item) {
                                return $item['total_price'];
                            }, $model->orderItems)), 0)
                            . Yii::t('model', '&nbsp;<i>сум</i>')
                    ],
                ]
            ]);
            ?>
        </div>
    <?php } ?>
</div>

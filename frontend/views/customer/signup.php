<?php

use yii\helpers\Html;
use yii\captcha\Captcha;
use yii\bootstrap\Modal;
use kartik\file\FileInput;
use yii\widgets\MaskedInput;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */
/* @var $offerText string */

$this->title = Yii::t('frontend', 'Зарегистрироваться');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="customer-signup deals">
    <h3 class="w3ls-title title-center"><?= Html::encode($this->title) ?></h3>
    <div class="row">
        <div class="col-md-6 col-sm-8 col-md-offset-3">
            <p><?= Yii::t('frontend', 'Пожалуйста, заполните следующие поля для регистрации:') ?></p>

            <?php $form = ActiveForm::begin(['id' => 'form-signup', 'options' => ['enctype' => 'multipart/form-data']]); ?>

            <?= $form->field($model, 'email') ?>
            <?= $form->field($model, 'password')->passwordInput() ?>
            <?= $form->field($model, 'password_repeat')->passwordInput() ?>

            <?= $form->field($model, 'phone')->widget(MaskedInput::className(),
                ['mask' => '+\\9\\9899-999-9999']) ?>

            <?php
            echo $form->field($model, 'offer')->checkbox([
                'template' => '{beginLabel}{input} {labelTitle}{endLabel} <button type="button" data-toggle="modal" data-target="#offerModal" class="btn btn-link"><i>«Пользовательского соглашения»</i></button>{error}{hint}'
            ]);

            Modal::begin([
                'id' => 'offerModal',
                'size' => 'modal-lg',
                'header' => '<h4>' . Yii::t('frontend', 'Публичная оферта интернет-магазина') . '</h4>',
            ]);
            echo $offerText . '<div class="clearfix"></div>';
            Modal::end(); ?>

            <?= $form->field($model, 'verify_code')->widget(Captcha::className(), [
                'template' => '<div class="row"><div class="col-md-6">{input}</div></div>{image}<button type="button" id="captcha_refresh" class="btn btn-default"><i class="glyphicon glyphicon-refresh"></i></button>',
                'imageOptions' => ['id' => 'captcha_image']
            ]) ?>
            <?php $this->registerJs("jQuery('#captcha_refresh').on('click', function(e){
                    e.preventDefault(); jQuery('#captcha_image').yiiCaptcha('refresh'); })") ?>

            <?= Html::submitButton(Yii::t('frontend', 'Зарегистрироваться'), ['class' => 'btn btn-primary pull-right', 'name' => 'signup-button']) ?>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

jQuery(document).ready(function ($) {

    jQuery('.header-two').scrollToFixed({
        minWidth: 768
    });
    var summaries = $('.summary');
    summaries.each(function (i) {
        var summary = $(summaries[i]);
        var next = summaries[i + 1];
        summary.scrollToFixed({
            marginTop: jQuery('.header-two').outerHeight(true) + 10,
            zIndex: 999
        });
    });

    jQuery(".scroll").click(function (event) {
        event.preventDefault();
        jQuery('html,body').animate({
            scrollTop: jQuery(this.hash).offset().top
        }, 1000);
    });

    jQuery().UItoTop({easingType: 'easeOutQuart'});

    jQuery('.marquee').marquee({pauseOnHover: true});

    jQuery('.flexslider').flexslider({
        animation: "slide",
        controlNav: "thumbnails"
    });

    var overlay = jQuery('.overlay');

    function addToCart() {
        jQuery('.add-to-cart').click(function () {
            overlay.show();
            jQuery.ajax({
                type: "POST",
                url: '/customer/add-to-cart',
                data: {product: parseInt(jQuery(this).data('product'))},
                success: function (response) {
                    if (response === true) {
                        jQuery.pjax.reload({
                            container: "#pjax_header_cart"
                        }).done(function () {
                            overlay.hide();
                        });
                    }
                }
            });
        });
    }

    addToCart();

    jQuery(document).on('pjax:send', '#pjax_products', function () {
        overlay.show();
    });

    jQuery(document).on('pjax:complete', '#pjax_products', function () {
        overlay.hide();
        addToCart();
    });

    jQuery(document).on('pjax:send', '#pjax_basket', function () {
        overlay.show();
    });

    jQuery(document).on('pjax:complete', '#pjax_basket', function () {
        overlay.hide();
        addToCart();
    });
});
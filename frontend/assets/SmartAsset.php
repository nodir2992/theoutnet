<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * SmartAsset bundle for the Theme Smart css and js files.
 */
class SmartAsset extends AssetBundle
{
    public $basePath = '@webroot/smart';
    public $baseUrl = '@web/smart';
//    public $sourcePath = '@webroot/smart';
    public $css = [
        'css/style.css',
        'css/menu.css',
        'css/mega-menu.css',
        'css/ken-burns.css',
        'css/owl.carousel.css',
        'css/flexslider.css',
    ];
    public $js = [
        'js/owl.carousel.js',
        'js/jquery-scrolltofixed-min.js',
        'js/move-top.js',
        'js/easing.js',
        'js/jquery.marquee.min.js',
        'js/custom.js',
        'js/jquery.menu-aim.js',
        'js/jquery.flexslider.js',
        'js/main.js',
        'js/script.js',
    ];
    public $depends = [
        'yii\bootstrap\BootstrapPluginAsset',
        'rmrevin\yii\fontawesome\AssetBundle',
    ];

}

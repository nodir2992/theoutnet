<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * SmoothAsset bundle for SmoothAsset css and js files.
 */
class SmoothAsset extends AssetBundle
{
    public $sourcePath = '@webroot/smooth';
    public $css = [
        'css/smoothproducts.css',
    ];
    public $js = [
        'js/smoothproducts.js',
    ];

    public $depends = [
        AppAsset::class,
    ];
}

<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * SmartWizardAsset bundle for SmartWizard css and js files.
 */
class SmartWizardAsset extends AssetBundle
{
    public $sourcePath = '@webroot/wizard';
    public $css = [
        'css/smart_wizard.css',
    ];
    public $js = [
        'js/jquery.smartWizard.js',
    ];

    public $depends = [
        AppAsset::class,
    ];
}

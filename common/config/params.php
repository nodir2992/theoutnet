<?php
return [
    'adminEmail' => 'nodir.madiyarov@gmail.com',
    'noreplyEmail' => 'noreply@theoutnet.uz',
    'infoEmail' => 'info@theoutnet.uz',
    'bccEmail' => 'bcc@theoutnet.uz',
    'supportEmail' => 'support@theoutnet.uz',
    'user.passwordResetTokenExpire' => 3600,
    'multiLanguages' => false,
];

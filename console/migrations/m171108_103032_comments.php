<?php

use yii\db\Migration;

/**
 * Class m171108_103032_comments
 */
class m171108_103032_comments extends Migration
{
    public function up()
    {
        $tableOptions = ($this->db->driverName === 'mysql') ? 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB' : null;

        $this->createTable('comments', [
            'id' => $this->primaryKey(),
            'model' => $this->string()->notNull(),
            'model_id' => $this->integer()->notNull(),
            'author_id' => $this->integer()->notNull(),
            'parent_id' => $this->integer()->null(),
            'rating' => $this->float()->null(),
            'text' => $this->text()->notNull(),
            'likes' => $this->integer()->notNull()->defaultValue(0),
            'unlikes' => $this->integer()->notNull()->defaultValue(0),
            'sessions' => $this->text()->null(),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx-comments', 'comments', ['model', 'model_id', 'author_id', 'parent_id']);

    }

    public function down()
    {
        $this->dropTable('comments');
    }
}

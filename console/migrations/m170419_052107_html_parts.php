<?php

use yii\db\Migration;
use backend\models\menu\MenuItems;
use backend\models\parts\HtmlParts;

class m170419_052107_html_parts extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(HtmlParts::tableName(), [
            'id' => $this->primaryKey(),
            'key' => $this->string(32)->notNull()->unique(),
            'name' => $this->string(64)->notNull(),
            'body' => $this->text()->null(),
            'status' => $this->smallInteger()->notNull()->defaultValue(HtmlParts::STATUS_INACTIVE),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->insert(MenuItems::tableName(), [
            'id' => 18,
            'menu_id' => 1,
            'parent_id' => 14,
            'label' => 'Blocks',
            'url' => '/html-parts/index',
            'class' => 'fa fa-cubes',
            'icon' => '',
            'description' => '',
            'weight' => 0,
            'status' => MenuItems::STATUS_ACTIVE,
            'created_at' => time(),
            'updated_at' => time()
        ]);
    }

    public function down()
    {
        $this->delete(MenuItems::tableName(), ['id' => 18]);
        $this->dropTable(HtmlParts::tableName());
    }
}

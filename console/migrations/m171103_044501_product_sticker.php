<?php

use yii\db\Migration;

class m171103_044501_product_sticker extends Migration
{
    public function up()
    {
        $tableOptions = ($this->db->driverName === 'mysql') ? 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB' : null;

        $this->createTable('product_stickers', [
            'id' => $this->primaryKey(),
            'slug' => $this->string()->notNull()->unique(),
            'name' => $this->string()->notNull(),
            'image' => $this->string()->notNull(),
            'link' => $this->string()->notNull(),
            'description' => $this->text()->null(),
            'position' => $this->smallInteger()->notNull()->defaultValue(0),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createTable('product_to_sticker', [
            'product_id' => $this->integer()->notNull(),
            'sticker_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addPrimaryKey('pk-product_to_sticker', 'product_to_sticker', ['product_id', 'sticker_id']);
        $this->createIndex('idx-product_to_sticker-product_id', 'product_to_sticker', 'product_id');
        $this->createIndex('idx-product_to_sticker-sticker_id', 'product_to_sticker', 'sticker_id');
        $this->addForeignKey('fk-product_to_sticker-product_id', 'product_to_sticker', 'product_id', 'products', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('fk-product_to_sticker-sticker_id', 'product_to_sticker', 'sticker_id', 'product_stickers', 'id', 'CASCADE', 'RESTRICT');
    }

    public function down()
    {
        $this->dropTable('product_to_sticker');
        $this->dropTable('product_stickers');
    }
}

<?php

use common\models\User;
use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string(32)->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string(64)->notNull()->unique(),
            'first_name' => $this->string(32)->null(),
            'last_name' => $this->string(32)->null(),
            'image' => $this->string()->null(),
            'info' => $this->text()->null(),
            'status' => $this->smallInteger()->notNull()->defaultValue(User::STATUS_ACTIVE),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->insert('{{%user}}', [
            'username' => 'admin',
            'auth_key' => 'pAnQu_jfrTR1JE806g8rDmMv9hHhPwNx',
            'password_hash' => '$2y$13$Ykpd5xLtbBy20UeQYXst9unFYlTuTx4CPmBTLfatxLRH8c3E0LI6i',
            'password_reset_token' => null,
            'email' => 'nodir.madiyarov@gmail.com',
            'first_name' => 'Nodir',
            'last_name' => 'Madiyarov',
            'image' => '/uploads/users/default.png',
            'info' => null,
            'status' => User::STATUS_ACTIVE,
            'created_at' => time(),
            'updated_at' => time(),
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%user}}');
    }
}

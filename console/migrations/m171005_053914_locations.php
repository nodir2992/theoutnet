<?php

use yii\db\Migration;

class m171005_053914_locations extends Migration
{
    public function up()
    {
        $tableOptions = ($this->db->driverName === 'mysql') ? 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB' : null;

        $this->createTable('locations', [
            'id' => $this->primaryKey(),
            'location_id' => $this->integer()->null(),
            'type' => $this->char(16)->notNull(),
            'name' => $this->string()->notNull(),
            'weight' => $this->integer()->notNull()->defaultValue(0),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx-locations', 'locations', 'type');
        $this->addForeignKey('fk-locations-location_id', 'locations', 'location_id', 'locations', 'id', 'CASCADE', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('locations');
    }
}

<?php

use yii\db\Migration;

/**
 * Class m171204_065739_product_timer
 */
class m171204_065739_product_timer extends Migration
{
    public function up()
    {
        $this->addColumn('products', 'timer_start', $this->integer()->null()->after('quantity'));
        $this->addColumn('products', 'timer_end', $this->integer()->null()->after('timer_start'));
    }

    public function down()
    {
        $this->dropColumn('products', 'timer_start');
        $this->dropColumn('products', 'timer_end');
    }
}

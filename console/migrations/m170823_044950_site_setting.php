<?php

use backend\models\menu\MenuItems;
use yii\db\Migration;

class m170823_044950_site_setting extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('settings', [
            'id' => $this->primaryKey(),
            'key' => $this->string(64)->notNull()->unique(),
            'label' => $this->string(64)->notNull(),
            'value' => $this->text()->null(),
            'type' => $this->smallInteger()->notNull()->defaultValue(0),
            'required' => $this->smallInteger()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->insert(MenuItems::tableName(), [
            'id' => 26,
            'menu_id' => 1,
            'parent_id' => 14,
            'label' => 'Settings',
            'url' => '/site/setting',
            'class' => 'fa fa-gears',
            'icon' => '',
            'description' => '',
            'weight' => 0,
            'status' => MenuItems::STATUS_ACTIVE,
            'created_at' => time(),
            'updated_at' => time()
        ]);
    }

    public function down()
    {
        $this->delete(MenuItems::tableName(), ['id' => 26]);
        $this->dropTable('settings');
    }
}
